﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"









extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_DeflateStream_UnmanagedRead_m4002292959(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2);
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_DeflateStream_UnmanagedWrite_m3688808850(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsReady_m2855172723(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsDidError_m3621980989(int64_t ___rawError0, char* ___message1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsDidStart_m971319454(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsDidFinish_m4137038415(char* ___placementId0, int64_t ___rawShowResult1);
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[6] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStream_UnmanagedRead_m4002292959),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStream_UnmanagedWrite_m3688808850),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsReady_m2855172723),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsDidError_m3621980989),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsDidStart_m971319454),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsDidFinish_m4137038415),
};
