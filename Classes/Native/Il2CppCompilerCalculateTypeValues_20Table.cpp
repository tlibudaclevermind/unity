﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// TrippleTriggerMiddleScript
struct TrippleTriggerMiddleScript_t4217752517;
// ButtonControllerScript
struct ButtonControllerScript_t2432915718;
// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t990845000;
// LoseScript
struct LoseScript_t212724369;
// System.Void
struct Void_t1185182177;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CDELAYGAMEU3EC__ITERATOR0_T4237126612_H
#define U3CDELAYGAMEU3EC__ITERATOR0_T4237126612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerMiddleScript/<DelayGame>c__Iterator0
struct  U3CDelayGameU3Ec__Iterator0_t4237126612  : public RuntimeObject
{
public:
	// System.Single TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_0;
	// TrippleTriggerMiddleScript TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::$this
	TrippleTriggerMiddleScript_t4217752517 * ___U24this_1;
	// System.Object TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4237126612, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4237126612, ___U24this_1)); }
	inline TrippleTriggerMiddleScript_t4217752517 * get_U24this_1() const { return ___U24this_1; }
	inline TrippleTriggerMiddleScript_t4217752517 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TrippleTriggerMiddleScript_t4217752517 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4237126612, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4237126612, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4237126612, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYGAMEU3EC__ITERATOR0_T4237126612_H
#ifndef U3CHANDLEANIMATIONU3EC__ITERATOR2_T1056433757_H
#define U3CHANDLEANIMATIONU3EC__ITERATOR2_T1056433757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonControllerScript/<HandleAnimation>c__Iterator2
struct  U3CHandleAnimationU3Ec__Iterator2_t1056433757  : public RuntimeObject
{
public:
	// ButtonControllerScript ButtonControllerScript/<HandleAnimation>c__Iterator2::$this
	ButtonControllerScript_t2432915718 * ___U24this_0;
	// System.Object ButtonControllerScript/<HandleAnimation>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ButtonControllerScript/<HandleAnimation>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 ButtonControllerScript/<HandleAnimation>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t1056433757, ___U24this_0)); }
	inline ButtonControllerScript_t2432915718 * get_U24this_0() const { return ___U24this_0; }
	inline ButtonControllerScript_t2432915718 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ButtonControllerScript_t2432915718 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t1056433757, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t1056433757, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t1056433757, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEANIMATIONU3EC__ITERATOR2_T1056433757_H
#ifndef U3CHANDLEANIMATIONU3EC__ITERATOR2_T2078686267_H
#define U3CHANDLEANIMATIONU3EC__ITERATOR2_T2078686267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2
struct  U3CHandleAnimationU3Ec__Iterator2_t2078686267  : public RuntimeObject
{
public:
	// TrippleTriggerMiddleScript TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::$this
	TrippleTriggerMiddleScript_t4217752517 * ___U24this_0;
	// System.Object TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2078686267, ___U24this_0)); }
	inline TrippleTriggerMiddleScript_t4217752517 * get_U24this_0() const { return ___U24this_0; }
	inline TrippleTriggerMiddleScript_t4217752517 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TrippleTriggerMiddleScript_t4217752517 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2078686267, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2078686267, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2078686267, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEANIMATIONU3EC__ITERATOR2_T2078686267_H
#ifndef U3CSHOWADWHENREADYU3EC__ITERATOR1_T1651327877_H
#define U3CSHOWADWHENREADYU3EC__ITERATOR1_T1651327877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1
struct  U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowOptions TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::<options>__0
	ShowOptions_t990845000 * ___U3CoptionsU3E__0_0;
	// TrippleTriggerMiddleScript TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::$this
	TrippleTriggerMiddleScript_t4217752517 * ___U24this_1;
	// System.Object TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoptionsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877, ___U3CoptionsU3E__0_0)); }
	inline ShowOptions_t990845000 * get_U3CoptionsU3E__0_0() const { return ___U3CoptionsU3E__0_0; }
	inline ShowOptions_t990845000 ** get_address_of_U3CoptionsU3E__0_0() { return &___U3CoptionsU3E__0_0; }
	inline void set_U3CoptionsU3E__0_0(ShowOptions_t990845000 * value)
	{
		___U3CoptionsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoptionsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877, ___U24this_1)); }
	inline TrippleTriggerMiddleScript_t4217752517 * get_U24this_1() const { return ___U24this_1; }
	inline TrippleTriggerMiddleScript_t4217752517 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TrippleTriggerMiddleScript_t4217752517 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWADWHENREADYU3EC__ITERATOR1_T1651327877_H
#ifndef U3CSHOWADWHENREADYU3EC__ITERATOR1_T1035390249_H
#define U3CSHOWADWHENREADYU3EC__ITERATOR1_T1035390249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonControllerScript/<ShowAdWhenReady>c__Iterator1
struct  U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowOptions ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::<options>__0
	ShowOptions_t990845000 * ___U3CoptionsU3E__0_0;
	// ButtonControllerScript ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::$this
	ButtonControllerScript_t2432915718 * ___U24this_1;
	// System.Object ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoptionsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249, ___U3CoptionsU3E__0_0)); }
	inline ShowOptions_t990845000 * get_U3CoptionsU3E__0_0() const { return ___U3CoptionsU3E__0_0; }
	inline ShowOptions_t990845000 ** get_address_of_U3CoptionsU3E__0_0() { return &___U3CoptionsU3E__0_0; }
	inline void set_U3CoptionsU3E__0_0(ShowOptions_t990845000 * value)
	{
		___U3CoptionsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoptionsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249, ___U24this_1)); }
	inline ButtonControllerScript_t2432915718 * get_U24this_1() const { return ___U24this_1; }
	inline ButtonControllerScript_t2432915718 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ButtonControllerScript_t2432915718 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWADWHENREADYU3EC__ITERATOR1_T1035390249_H
#ifndef U3CDELAYGAMEU3EC__ITERATOR0_T4235577792_H
#define U3CDELAYGAMEU3EC__ITERATOR0_T4235577792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseScript/<DelayGame>c__Iterator0
struct  U3CDelayGameU3Ec__Iterator0_t4235577792  : public RuntimeObject
{
public:
	// System.Single LoseScript/<DelayGame>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_0;
	// LoseScript LoseScript/<DelayGame>c__Iterator0::$this
	LoseScript_t212724369 * ___U24this_1;
	// System.Object LoseScript/<DelayGame>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean LoseScript/<DelayGame>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 LoseScript/<DelayGame>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4235577792, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4235577792, ___U24this_1)); }
	inline LoseScript_t212724369 * get_U24this_1() const { return ___U24this_1; }
	inline LoseScript_t212724369 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LoseScript_t212724369 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4235577792, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4235577792, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4235577792, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYGAMEU3EC__ITERATOR0_T4235577792_H
#ifndef U3CDELAYGAMESTARTU3EC__ITERATOR0_T3856269498_H
#define U3CDELAYGAMESTARTU3EC__ITERATOR0_T3856269498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonControllerScript/<DelayGameStart>c__Iterator0
struct  U3CDelayGameStartU3Ec__Iterator0_t3856269498  : public RuntimeObject
{
public:
	// ButtonControllerScript ButtonControllerScript/<DelayGameStart>c__Iterator0::$this
	ButtonControllerScript_t2432915718 * ___U24this_0;
	// System.Object ButtonControllerScript/<DelayGameStart>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ButtonControllerScript/<DelayGameStart>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ButtonControllerScript/<DelayGameStart>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDelayGameStartU3Ec__Iterator0_t3856269498, ___U24this_0)); }
	inline ButtonControllerScript_t2432915718 * get_U24this_0() const { return ___U24this_0; }
	inline ButtonControllerScript_t2432915718 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ButtonControllerScript_t2432915718 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayGameStartU3Ec__Iterator0_t3856269498, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayGameStartU3Ec__Iterator0_t3856269498, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayGameStartU3Ec__Iterator0_t3856269498, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYGAMESTARTU3EC__ITERATOR0_T3856269498_H
#ifndef U3CSHOWADWHENREADYU3EC__ITERATOR1_T4153835618_H
#define U3CSHOWADWHENREADYU3EC__ITERATOR1_T4153835618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseScript/<ShowAdWhenReady>c__Iterator1
struct  U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowOptions LoseScript/<ShowAdWhenReady>c__Iterator1::<options>__0
	ShowOptions_t990845000 * ___U3CoptionsU3E__0_0;
	// LoseScript LoseScript/<ShowAdWhenReady>c__Iterator1::$this
	LoseScript_t212724369 * ___U24this_1;
	// System.Object LoseScript/<ShowAdWhenReady>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean LoseScript/<ShowAdWhenReady>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 LoseScript/<ShowAdWhenReady>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoptionsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618, ___U3CoptionsU3E__0_0)); }
	inline ShowOptions_t990845000 * get_U3CoptionsU3E__0_0() const { return ___U3CoptionsU3E__0_0; }
	inline ShowOptions_t990845000 ** get_address_of_U3CoptionsU3E__0_0() { return &___U3CoptionsU3E__0_0; }
	inline void set_U3CoptionsU3E__0_0(ShowOptions_t990845000 * value)
	{
		___U3CoptionsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoptionsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618, ___U24this_1)); }
	inline LoseScript_t212724369 * get_U24this_1() const { return ___U24this_1; }
	inline LoseScript_t212724369 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LoseScript_t212724369 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWADWHENREADYU3EC__ITERATOR1_T4153835618_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CHANDLEANIMATIONU3EC__ITERATOR2_T2293152264_H
#define U3CHANDLEANIMATIONU3EC__ITERATOR2_T2293152264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseScript/<HandleAnimation>c__Iterator2
struct  U3CHandleAnimationU3Ec__Iterator2_t2293152264  : public RuntimeObject
{
public:
	// System.Object LoseScript/<HandleAnimation>c__Iterator2::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean LoseScript/<HandleAnimation>c__Iterator2::$disposing
	bool ___U24disposing_1;
	// System.Int32 LoseScript/<HandleAnimation>c__Iterator2::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2293152264, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2293152264, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2293152264, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEANIMATIONU3EC__ITERATOR2_T2293152264_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef LOSESCRIPT_T212724369_H
#define LOSESCRIPT_T212724369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseScript
struct  LoseScript_t212724369  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LoseScript::Game
	GameObject_t1113636619 * ___Game_2;
	// UnityEngine.GameObject LoseScript::Menu
	GameObject_t1113636619 * ___Menu_3;
	// UnityEngine.GameObject[] LoseScript::Cubes
	GameObjectU5BU5D_t3328599146* ___Cubes_4;
	// System.Int32 LoseScript::tries
	int32_t ___tries_5;
	// System.String LoseScript::difficulty
	String_t* ___difficulty_6;
	// UnityEngine.AudioSource LoseScript::audioSrc
	AudioSource_t3935305588 * ___audioSrc_7;
	// System.Int32 LoseScript::triesTillAds
	int32_t ___triesTillAds_8;
	// System.Single LoseScript::savedTimeScale
	float ___savedTimeScale_9;
	// UnityEngine.Animator LoseScript::anim
	Animator_t434523843 * ___anim_10;

public:
	inline static int32_t get_offset_of_Game_2() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___Game_2)); }
	inline GameObject_t1113636619 * get_Game_2() const { return ___Game_2; }
	inline GameObject_t1113636619 ** get_address_of_Game_2() { return &___Game_2; }
	inline void set_Game_2(GameObject_t1113636619 * value)
	{
		___Game_2 = value;
		Il2CppCodeGenWriteBarrier((&___Game_2), value);
	}

	inline static int32_t get_offset_of_Menu_3() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___Menu_3)); }
	inline GameObject_t1113636619 * get_Menu_3() const { return ___Menu_3; }
	inline GameObject_t1113636619 ** get_address_of_Menu_3() { return &___Menu_3; }
	inline void set_Menu_3(GameObject_t1113636619 * value)
	{
		___Menu_3 = value;
		Il2CppCodeGenWriteBarrier((&___Menu_3), value);
	}

	inline static int32_t get_offset_of_Cubes_4() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___Cubes_4)); }
	inline GameObjectU5BU5D_t3328599146* get_Cubes_4() const { return ___Cubes_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Cubes_4() { return &___Cubes_4; }
	inline void set_Cubes_4(GameObjectU5BU5D_t3328599146* value)
	{
		___Cubes_4 = value;
		Il2CppCodeGenWriteBarrier((&___Cubes_4), value);
	}

	inline static int32_t get_offset_of_tries_5() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___tries_5)); }
	inline int32_t get_tries_5() const { return ___tries_5; }
	inline int32_t* get_address_of_tries_5() { return &___tries_5; }
	inline void set_tries_5(int32_t value)
	{
		___tries_5 = value;
	}

	inline static int32_t get_offset_of_difficulty_6() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___difficulty_6)); }
	inline String_t* get_difficulty_6() const { return ___difficulty_6; }
	inline String_t** get_address_of_difficulty_6() { return &___difficulty_6; }
	inline void set_difficulty_6(String_t* value)
	{
		___difficulty_6 = value;
		Il2CppCodeGenWriteBarrier((&___difficulty_6), value);
	}

	inline static int32_t get_offset_of_audioSrc_7() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___audioSrc_7)); }
	inline AudioSource_t3935305588 * get_audioSrc_7() const { return ___audioSrc_7; }
	inline AudioSource_t3935305588 ** get_address_of_audioSrc_7() { return &___audioSrc_7; }
	inline void set_audioSrc_7(AudioSource_t3935305588 * value)
	{
		___audioSrc_7 = value;
		Il2CppCodeGenWriteBarrier((&___audioSrc_7), value);
	}

	inline static int32_t get_offset_of_triesTillAds_8() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___triesTillAds_8)); }
	inline int32_t get_triesTillAds_8() const { return ___triesTillAds_8; }
	inline int32_t* get_address_of_triesTillAds_8() { return &___triesTillAds_8; }
	inline void set_triesTillAds_8(int32_t value)
	{
		___triesTillAds_8 = value;
	}

	inline static int32_t get_offset_of_savedTimeScale_9() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___savedTimeScale_9)); }
	inline float get_savedTimeScale_9() const { return ___savedTimeScale_9; }
	inline float* get_address_of_savedTimeScale_9() { return &___savedTimeScale_9; }
	inline void set_savedTimeScale_9(float value)
	{
		___savedTimeScale_9 = value;
	}

	inline static int32_t get_offset_of_anim_10() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___anim_10)); }
	inline Animator_t434523843 * get_anim_10() const { return ___anim_10; }
	inline Animator_t434523843 ** get_address_of_anim_10() { return &___anim_10; }
	inline void set_anim_10(Animator_t434523843 * value)
	{
		___anim_10 = value;
		Il2CppCodeGenWriteBarrier((&___anim_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOSESCRIPT_T212724369_H
#ifndef RANDOMSPAWNSCRIPT_T4065494415_H
#define RANDOMSPAWNSCRIPT_T4065494415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomSpawnScript
struct  RandomSpawnScript_t4065494415  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject RandomSpawnScript::prefab1
	GameObject_t1113636619 * ___prefab1_2;
	// UnityEngine.GameObject RandomSpawnScript::prefab2
	GameObject_t1113636619 * ___prefab2_3;
	// UnityEngine.GameObject RandomSpawnScript::prefab3
	GameObject_t1113636619 * ___prefab3_4;
	// UnityEngine.GameObject RandomSpawnScript::prefab4
	GameObject_t1113636619 * ___prefab4_5;
	// System.Single RandomSpawnScript::spawnRate
	float ___spawnRate_6;
	// System.Single RandomSpawnScript::spawnRateHard
	float ___spawnRateHard_7;
	// System.Single RandomSpawnScript::nextSpawn
	float ___nextSpawn_8;
	// System.Int32 RandomSpawnScript::whatToSpawn
	int32_t ___whatToSpawn_9;
	// System.String RandomSpawnScript::theme
	String_t* ___theme_10;
	// UnityEngine.GameObject[] RandomSpawnScript::obstacles
	GameObjectU5BU5D_t3328599146* ___obstacles_11;
	// UnityEngine.Vector3 RandomSpawnScript::v3_position
	Vector3_t3722313464  ___v3_position_12;

public:
	inline static int32_t get_offset_of_prefab1_2() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___prefab1_2)); }
	inline GameObject_t1113636619 * get_prefab1_2() const { return ___prefab1_2; }
	inline GameObject_t1113636619 ** get_address_of_prefab1_2() { return &___prefab1_2; }
	inline void set_prefab1_2(GameObject_t1113636619 * value)
	{
		___prefab1_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefab1_2), value);
	}

	inline static int32_t get_offset_of_prefab2_3() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___prefab2_3)); }
	inline GameObject_t1113636619 * get_prefab2_3() const { return ___prefab2_3; }
	inline GameObject_t1113636619 ** get_address_of_prefab2_3() { return &___prefab2_3; }
	inline void set_prefab2_3(GameObject_t1113636619 * value)
	{
		___prefab2_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefab2_3), value);
	}

	inline static int32_t get_offset_of_prefab3_4() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___prefab3_4)); }
	inline GameObject_t1113636619 * get_prefab3_4() const { return ___prefab3_4; }
	inline GameObject_t1113636619 ** get_address_of_prefab3_4() { return &___prefab3_4; }
	inline void set_prefab3_4(GameObject_t1113636619 * value)
	{
		___prefab3_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefab3_4), value);
	}

	inline static int32_t get_offset_of_prefab4_5() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___prefab4_5)); }
	inline GameObject_t1113636619 * get_prefab4_5() const { return ___prefab4_5; }
	inline GameObject_t1113636619 ** get_address_of_prefab4_5() { return &___prefab4_5; }
	inline void set_prefab4_5(GameObject_t1113636619 * value)
	{
		___prefab4_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefab4_5), value);
	}

	inline static int32_t get_offset_of_spawnRate_6() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___spawnRate_6)); }
	inline float get_spawnRate_6() const { return ___spawnRate_6; }
	inline float* get_address_of_spawnRate_6() { return &___spawnRate_6; }
	inline void set_spawnRate_6(float value)
	{
		___spawnRate_6 = value;
	}

	inline static int32_t get_offset_of_spawnRateHard_7() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___spawnRateHard_7)); }
	inline float get_spawnRateHard_7() const { return ___spawnRateHard_7; }
	inline float* get_address_of_spawnRateHard_7() { return &___spawnRateHard_7; }
	inline void set_spawnRateHard_7(float value)
	{
		___spawnRateHard_7 = value;
	}

	inline static int32_t get_offset_of_nextSpawn_8() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___nextSpawn_8)); }
	inline float get_nextSpawn_8() const { return ___nextSpawn_8; }
	inline float* get_address_of_nextSpawn_8() { return &___nextSpawn_8; }
	inline void set_nextSpawn_8(float value)
	{
		___nextSpawn_8 = value;
	}

	inline static int32_t get_offset_of_whatToSpawn_9() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___whatToSpawn_9)); }
	inline int32_t get_whatToSpawn_9() const { return ___whatToSpawn_9; }
	inline int32_t* get_address_of_whatToSpawn_9() { return &___whatToSpawn_9; }
	inline void set_whatToSpawn_9(int32_t value)
	{
		___whatToSpawn_9 = value;
	}

	inline static int32_t get_offset_of_theme_10() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___theme_10)); }
	inline String_t* get_theme_10() const { return ___theme_10; }
	inline String_t** get_address_of_theme_10() { return &___theme_10; }
	inline void set_theme_10(String_t* value)
	{
		___theme_10 = value;
		Il2CppCodeGenWriteBarrier((&___theme_10), value);
	}

	inline static int32_t get_offset_of_obstacles_11() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___obstacles_11)); }
	inline GameObjectU5BU5D_t3328599146* get_obstacles_11() const { return ___obstacles_11; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_obstacles_11() { return &___obstacles_11; }
	inline void set_obstacles_11(GameObjectU5BU5D_t3328599146* value)
	{
		___obstacles_11 = value;
		Il2CppCodeGenWriteBarrier((&___obstacles_11), value);
	}

	inline static int32_t get_offset_of_v3_position_12() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___v3_position_12)); }
	inline Vector3_t3722313464  get_v3_position_12() const { return ___v3_position_12; }
	inline Vector3_t3722313464 * get_address_of_v3_position_12() { return &___v3_position_12; }
	inline void set_v3_position_12(Vector3_t3722313464  value)
	{
		___v3_position_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMSPAWNSCRIPT_T4065494415_H
#ifndef TRIPPLETRIGGERLEFTSCRIPT_T4183068999_H
#define TRIPPLETRIGGERLEFTSCRIPT_T4183068999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerLeftScript
struct  TrippleTriggerLeftScript_t4183068999  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIPPLETRIGGERLEFTSCRIPT_T4183068999_H
#ifndef TRIPPLETRIGGERRIGHTSCRIPT_T853323302_H
#define TRIPPLETRIGGERRIGHTSCRIPT_T853323302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerRightScript
struct  TrippleTriggerRightScript_t853323302  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIPPLETRIGGERRIGHTSCRIPT_T853323302_H
#ifndef BUTTONCONTROLLERSCRIPT_T2432915718_H
#define BUTTONCONTROLLERSCRIPT_T2432915718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonControllerScript
struct  ButtonControllerScript_t2432915718  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ButtonControllerScript::audioSource
	GameObject_t1113636619 * ___audioSource_2;
	// UnityEngine.AudioClip ButtonControllerScript::clip_mainSound
	AudioClip_t3680889665 * ___clip_mainSound_3;
	// UnityEngine.AudioClip ButtonControllerScript::clip_dankstormSound
	AudioClip_t3680889665 * ___clip_dankstormSound_4;
	// UnityEngine.AudioClip ButtonControllerScript::clip_skyflightSound
	AudioClip_t3680889665 * ___clip_skyflightSound_5;
	// UnityEngine.AudioClip ButtonControllerScript::clip_egyptSound
	AudioClip_t3680889665 * ___clip_egyptSound_6;
	// UnityEngine.GameObject ButtonControllerScript::backgroundSprite
	GameObject_t1113636619 * ___backgroundSprite_7;
	// UnityEngine.Sprite ButtonControllerScript::mainTheme
	Sprite_t280657092 * ___mainTheme_8;
	// UnityEngine.Sprite ButtonControllerScript::flappyTheme
	Sprite_t280657092 * ___flappyTheme_9;
	// UnityEngine.Sprite ButtonControllerScript::nightTheme
	Sprite_t280657092 * ___nightTheme_10;
	// UnityEngine.Sprite ButtonControllerScript::desertTheme
	Sprite_t280657092 * ___desertTheme_11;
	// UnityEngine.GameObject ButtonControllerScript::btn_play
	GameObject_t1113636619 * ___btn_play_12;
	// UnityEngine.GameObject ButtonControllerScript::btn_video
	GameObject_t1113636619 * ___btn_video_13;
	// UnityEngine.GameObject ButtonControllerScript::btn_setting
	GameObject_t1113636619 * ___btn_setting_14;
	// UnityEngine.GameObject ButtonControllerScript::btn_reset
	GameObject_t1113636619 * ___btn_reset_15;
	// UnityEngine.GameObject ButtonControllerScript::txt_reset
	GameObject_t1113636619 * ___txt_reset_16;
	// UnityEngine.GameObject ButtonControllerScript::btn_answers
	GameObject_t1113636619 * ___btn_answers_17;
	// UnityEngine.GameObject ButtonControllerScript::btn_noSound
	GameObject_t1113636619 * ___btn_noSound_18;
	// UnityEngine.GameObject ButtonControllerScript::btn_mainSound
	GameObject_t1113636619 * ___btn_mainSound_19;
	// UnityEngine.GameObject ButtonControllerScript::btn_skyflightSound
	GameObject_t1113636619 * ___btn_skyflightSound_20;
	// UnityEngine.GameObject ButtonControllerScript::btn_dankstormSound
	GameObject_t1113636619 * ___btn_dankstormSound_21;
	// UnityEngine.GameObject ButtonControllerScript::btn_egyptSound
	GameObject_t1113636619 * ___btn_egyptSound_22;
	// UnityEngine.GameObject ButtonControllerScript::btn_back
	GameObject_t1113636619 * ___btn_back_23;
	// UnityEngine.GameObject ButtonControllerScript::btn_buy
	GameObject_t1113636619 * ___btn_buy_24;
	// System.Int32 ButtonControllerScript::nightThemeCost
	int32_t ___nightThemeCost_25;
	// System.Int32 ButtonControllerScript::flappyThemeCost
	int32_t ___flappyThemeCost_26;
	// System.Int32 ButtonControllerScript::desertThemeCost
	int32_t ___desertThemeCost_27;
	// System.Int32 ButtonControllerScript::skyflightSoundCost
	int32_t ___skyflightSoundCost_28;
	// System.Int32 ButtonControllerScript::dankstormSoundCost
	int32_t ___dankstormSoundCost_29;
	// System.Int32 ButtonControllerScript::egyptSoundCost
	int32_t ___egyptSoundCost_30;
	// UnityEngine.GameObject ButtonControllerScript::cnv_menu
	GameObject_t1113636619 * ___cnv_menu_31;
	// UnityEngine.GameObject ButtonControllerScript::cnv_settings
	GameObject_t1113636619 * ___cnv_settings_32;
	// UnityEngine.GameObject ButtonControllerScript::cnv_game
	GameObject_t1113636619 * ___cnv_game_33;
	// UnityEngine.GameObject ButtonControllerScript::go_permanent
	GameObject_t1113636619 * ___go_permanent_34;
	// UnityEngine.GameObject ButtonControllerScript::text_buy
	GameObject_t1113636619 * ___text_buy_35;
	// UnityEngine.UI.Text ButtonControllerScript::txt_cost
	Text_t1901882714 * ___txt_cost_36;
	// UnityEngine.UI.Text ButtonControllerScript::scoreText
	Text_t1901882714 * ___scoreText_37;
	// UnityEngine.UI.Text ButtonControllerScript::specialPointsText
	Text_t1901882714 * ___specialPointsText_38;
	// UnityEngine.Animator ButtonControllerScript::anim
	Animator_t434523843 * ___anim_39;
	// System.Int32 ButtonControllerScript::triesTillAds
	int32_t ___triesTillAds_40;
	// UnityEngine.GameObject ButtonControllerScript::Cube_plusOne
	GameObject_t1113636619 * ___Cube_plusOne_41;
	// System.Int32 ButtonControllerScript::specialPoints
	int32_t ___specialPoints_42;
	// UnityEngine.GameObject ButtonControllerScript::singleObstacle
	GameObject_t1113636619 * ___singleObstacle_43;
	// UnityEngine.GameObject ButtonControllerScript::doubleObstacle
	GameObject_t1113636619 * ___doubleObstacle_44;
	// UnityEngine.GameObject ButtonControllerScript::trippleObstacle
	GameObject_t1113636619 * ___trippleObstacle_45;
	// UnityEngine.GameObject ButtonControllerScript::quattroObstacle
	GameObject_t1113636619 * ___quattroObstacle_46;
	// System.Single ButtonControllerScript::previewTime
	float ___previewTime_47;
	// UnityEngine.UI.Toggle ButtonControllerScript::btn_difficulty
	Toggle_t2735377061 * ___btn_difficulty_48;
	// System.String ButtonControllerScript::difficulty
	String_t* ___difficulty_49;
	// UnityEngine.UI.Text ButtonControllerScript::textHighscoreCount
	Text_t1901882714 * ___textHighscoreCount_50;
	// System.Int32 ButtonControllerScript::highscore
	int32_t ___highscore_51;
	// UnityEngine.UI.Text ButtonControllerScript::textPlayedCount
	Text_t1901882714 * ___textPlayedCount_52;
	// System.Int32 ButtonControllerScript::tries
	int32_t ___tries_53;
	// UnityEngine.UI.Text ButtonControllerScript::textLastPoints
	Text_t1901882714 * ___textLastPoints_54;
	// System.Int32 ButtonControllerScript::lastPoints
	int32_t ___lastPoints_55;
	// UnityEngine.GameObject ButtonControllerScript::ckm_mainTheme
	GameObject_t1113636619 * ___ckm_mainTheme_56;
	// UnityEngine.GameObject ButtonControllerScript::ckm_nightTheme
	GameObject_t1113636619 * ___ckm_nightTheme_57;
	// UnityEngine.GameObject ButtonControllerScript::ckm_flappyTheme
	GameObject_t1113636619 * ___ckm_flappyTheme_58;
	// UnityEngine.GameObject ButtonControllerScript::ckm_desertTheme
	GameObject_t1113636619 * ___ckm_desertTheme_59;
	// UnityEngine.GameObject ButtonControllerScript::ckm_noSound
	GameObject_t1113636619 * ___ckm_noSound_60;
	// UnityEngine.GameObject ButtonControllerScript::ckm_mainSound
	GameObject_t1113636619 * ___ckm_mainSound_61;
	// UnityEngine.GameObject ButtonControllerScript::ckm_skyflightSound
	GameObject_t1113636619 * ___ckm_skyflightSound_62;
	// UnityEngine.GameObject ButtonControllerScript::ckm_dankstormSound
	GameObject_t1113636619 * ___ckm_dankstormSound_63;
	// UnityEngine.GameObject ButtonControllerScript::ckm_egyptSound
	GameObject_t1113636619 * ___ckm_egyptSound_64;
	// UnityEngine.GameObject ButtonControllerScript::texts
	GameObject_t1113636619 * ___texts_65;
	// UnityEngine.UI.ScrollRect ButtonControllerScript::svSounds
	ScrollRect_t4137855814 * ___svSounds_66;
	// UnityEngine.UI.ScrollRect ButtonControllerScript::svThemes
	ScrollRect_t4137855814 * ___svThemes_67;

public:
	inline static int32_t get_offset_of_audioSource_2() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___audioSource_2)); }
	inline GameObject_t1113636619 * get_audioSource_2() const { return ___audioSource_2; }
	inline GameObject_t1113636619 ** get_address_of_audioSource_2() { return &___audioSource_2; }
	inline void set_audioSource_2(GameObject_t1113636619 * value)
	{
		___audioSource_2 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_2), value);
	}

	inline static int32_t get_offset_of_clip_mainSound_3() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___clip_mainSound_3)); }
	inline AudioClip_t3680889665 * get_clip_mainSound_3() const { return ___clip_mainSound_3; }
	inline AudioClip_t3680889665 ** get_address_of_clip_mainSound_3() { return &___clip_mainSound_3; }
	inline void set_clip_mainSound_3(AudioClip_t3680889665 * value)
	{
		___clip_mainSound_3 = value;
		Il2CppCodeGenWriteBarrier((&___clip_mainSound_3), value);
	}

	inline static int32_t get_offset_of_clip_dankstormSound_4() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___clip_dankstormSound_4)); }
	inline AudioClip_t3680889665 * get_clip_dankstormSound_4() const { return ___clip_dankstormSound_4; }
	inline AudioClip_t3680889665 ** get_address_of_clip_dankstormSound_4() { return &___clip_dankstormSound_4; }
	inline void set_clip_dankstormSound_4(AudioClip_t3680889665 * value)
	{
		___clip_dankstormSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___clip_dankstormSound_4), value);
	}

	inline static int32_t get_offset_of_clip_skyflightSound_5() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___clip_skyflightSound_5)); }
	inline AudioClip_t3680889665 * get_clip_skyflightSound_5() const { return ___clip_skyflightSound_5; }
	inline AudioClip_t3680889665 ** get_address_of_clip_skyflightSound_5() { return &___clip_skyflightSound_5; }
	inline void set_clip_skyflightSound_5(AudioClip_t3680889665 * value)
	{
		___clip_skyflightSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___clip_skyflightSound_5), value);
	}

	inline static int32_t get_offset_of_clip_egyptSound_6() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___clip_egyptSound_6)); }
	inline AudioClip_t3680889665 * get_clip_egyptSound_6() const { return ___clip_egyptSound_6; }
	inline AudioClip_t3680889665 ** get_address_of_clip_egyptSound_6() { return &___clip_egyptSound_6; }
	inline void set_clip_egyptSound_6(AudioClip_t3680889665 * value)
	{
		___clip_egyptSound_6 = value;
		Il2CppCodeGenWriteBarrier((&___clip_egyptSound_6), value);
	}

	inline static int32_t get_offset_of_backgroundSprite_7() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___backgroundSprite_7)); }
	inline GameObject_t1113636619 * get_backgroundSprite_7() const { return ___backgroundSprite_7; }
	inline GameObject_t1113636619 ** get_address_of_backgroundSprite_7() { return &___backgroundSprite_7; }
	inline void set_backgroundSprite_7(GameObject_t1113636619 * value)
	{
		___backgroundSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSprite_7), value);
	}

	inline static int32_t get_offset_of_mainTheme_8() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___mainTheme_8)); }
	inline Sprite_t280657092 * get_mainTheme_8() const { return ___mainTheme_8; }
	inline Sprite_t280657092 ** get_address_of_mainTheme_8() { return &___mainTheme_8; }
	inline void set_mainTheme_8(Sprite_t280657092 * value)
	{
		___mainTheme_8 = value;
		Il2CppCodeGenWriteBarrier((&___mainTheme_8), value);
	}

	inline static int32_t get_offset_of_flappyTheme_9() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___flappyTheme_9)); }
	inline Sprite_t280657092 * get_flappyTheme_9() const { return ___flappyTheme_9; }
	inline Sprite_t280657092 ** get_address_of_flappyTheme_9() { return &___flappyTheme_9; }
	inline void set_flappyTheme_9(Sprite_t280657092 * value)
	{
		___flappyTheme_9 = value;
		Il2CppCodeGenWriteBarrier((&___flappyTheme_9), value);
	}

	inline static int32_t get_offset_of_nightTheme_10() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___nightTheme_10)); }
	inline Sprite_t280657092 * get_nightTheme_10() const { return ___nightTheme_10; }
	inline Sprite_t280657092 ** get_address_of_nightTheme_10() { return &___nightTheme_10; }
	inline void set_nightTheme_10(Sprite_t280657092 * value)
	{
		___nightTheme_10 = value;
		Il2CppCodeGenWriteBarrier((&___nightTheme_10), value);
	}

	inline static int32_t get_offset_of_desertTheme_11() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___desertTheme_11)); }
	inline Sprite_t280657092 * get_desertTheme_11() const { return ___desertTheme_11; }
	inline Sprite_t280657092 ** get_address_of_desertTheme_11() { return &___desertTheme_11; }
	inline void set_desertTheme_11(Sprite_t280657092 * value)
	{
		___desertTheme_11 = value;
		Il2CppCodeGenWriteBarrier((&___desertTheme_11), value);
	}

	inline static int32_t get_offset_of_btn_play_12() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_play_12)); }
	inline GameObject_t1113636619 * get_btn_play_12() const { return ___btn_play_12; }
	inline GameObject_t1113636619 ** get_address_of_btn_play_12() { return &___btn_play_12; }
	inline void set_btn_play_12(GameObject_t1113636619 * value)
	{
		___btn_play_12 = value;
		Il2CppCodeGenWriteBarrier((&___btn_play_12), value);
	}

	inline static int32_t get_offset_of_btn_video_13() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_video_13)); }
	inline GameObject_t1113636619 * get_btn_video_13() const { return ___btn_video_13; }
	inline GameObject_t1113636619 ** get_address_of_btn_video_13() { return &___btn_video_13; }
	inline void set_btn_video_13(GameObject_t1113636619 * value)
	{
		___btn_video_13 = value;
		Il2CppCodeGenWriteBarrier((&___btn_video_13), value);
	}

	inline static int32_t get_offset_of_btn_setting_14() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_setting_14)); }
	inline GameObject_t1113636619 * get_btn_setting_14() const { return ___btn_setting_14; }
	inline GameObject_t1113636619 ** get_address_of_btn_setting_14() { return &___btn_setting_14; }
	inline void set_btn_setting_14(GameObject_t1113636619 * value)
	{
		___btn_setting_14 = value;
		Il2CppCodeGenWriteBarrier((&___btn_setting_14), value);
	}

	inline static int32_t get_offset_of_btn_reset_15() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_reset_15)); }
	inline GameObject_t1113636619 * get_btn_reset_15() const { return ___btn_reset_15; }
	inline GameObject_t1113636619 ** get_address_of_btn_reset_15() { return &___btn_reset_15; }
	inline void set_btn_reset_15(GameObject_t1113636619 * value)
	{
		___btn_reset_15 = value;
		Il2CppCodeGenWriteBarrier((&___btn_reset_15), value);
	}

	inline static int32_t get_offset_of_txt_reset_16() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___txt_reset_16)); }
	inline GameObject_t1113636619 * get_txt_reset_16() const { return ___txt_reset_16; }
	inline GameObject_t1113636619 ** get_address_of_txt_reset_16() { return &___txt_reset_16; }
	inline void set_txt_reset_16(GameObject_t1113636619 * value)
	{
		___txt_reset_16 = value;
		Il2CppCodeGenWriteBarrier((&___txt_reset_16), value);
	}

	inline static int32_t get_offset_of_btn_answers_17() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_answers_17)); }
	inline GameObject_t1113636619 * get_btn_answers_17() const { return ___btn_answers_17; }
	inline GameObject_t1113636619 ** get_address_of_btn_answers_17() { return &___btn_answers_17; }
	inline void set_btn_answers_17(GameObject_t1113636619 * value)
	{
		___btn_answers_17 = value;
		Il2CppCodeGenWriteBarrier((&___btn_answers_17), value);
	}

	inline static int32_t get_offset_of_btn_noSound_18() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_noSound_18)); }
	inline GameObject_t1113636619 * get_btn_noSound_18() const { return ___btn_noSound_18; }
	inline GameObject_t1113636619 ** get_address_of_btn_noSound_18() { return &___btn_noSound_18; }
	inline void set_btn_noSound_18(GameObject_t1113636619 * value)
	{
		___btn_noSound_18 = value;
		Il2CppCodeGenWriteBarrier((&___btn_noSound_18), value);
	}

	inline static int32_t get_offset_of_btn_mainSound_19() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_mainSound_19)); }
	inline GameObject_t1113636619 * get_btn_mainSound_19() const { return ___btn_mainSound_19; }
	inline GameObject_t1113636619 ** get_address_of_btn_mainSound_19() { return &___btn_mainSound_19; }
	inline void set_btn_mainSound_19(GameObject_t1113636619 * value)
	{
		___btn_mainSound_19 = value;
		Il2CppCodeGenWriteBarrier((&___btn_mainSound_19), value);
	}

	inline static int32_t get_offset_of_btn_skyflightSound_20() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_skyflightSound_20)); }
	inline GameObject_t1113636619 * get_btn_skyflightSound_20() const { return ___btn_skyflightSound_20; }
	inline GameObject_t1113636619 ** get_address_of_btn_skyflightSound_20() { return &___btn_skyflightSound_20; }
	inline void set_btn_skyflightSound_20(GameObject_t1113636619 * value)
	{
		___btn_skyflightSound_20 = value;
		Il2CppCodeGenWriteBarrier((&___btn_skyflightSound_20), value);
	}

	inline static int32_t get_offset_of_btn_dankstormSound_21() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_dankstormSound_21)); }
	inline GameObject_t1113636619 * get_btn_dankstormSound_21() const { return ___btn_dankstormSound_21; }
	inline GameObject_t1113636619 ** get_address_of_btn_dankstormSound_21() { return &___btn_dankstormSound_21; }
	inline void set_btn_dankstormSound_21(GameObject_t1113636619 * value)
	{
		___btn_dankstormSound_21 = value;
		Il2CppCodeGenWriteBarrier((&___btn_dankstormSound_21), value);
	}

	inline static int32_t get_offset_of_btn_egyptSound_22() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_egyptSound_22)); }
	inline GameObject_t1113636619 * get_btn_egyptSound_22() const { return ___btn_egyptSound_22; }
	inline GameObject_t1113636619 ** get_address_of_btn_egyptSound_22() { return &___btn_egyptSound_22; }
	inline void set_btn_egyptSound_22(GameObject_t1113636619 * value)
	{
		___btn_egyptSound_22 = value;
		Il2CppCodeGenWriteBarrier((&___btn_egyptSound_22), value);
	}

	inline static int32_t get_offset_of_btn_back_23() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_back_23)); }
	inline GameObject_t1113636619 * get_btn_back_23() const { return ___btn_back_23; }
	inline GameObject_t1113636619 ** get_address_of_btn_back_23() { return &___btn_back_23; }
	inline void set_btn_back_23(GameObject_t1113636619 * value)
	{
		___btn_back_23 = value;
		Il2CppCodeGenWriteBarrier((&___btn_back_23), value);
	}

	inline static int32_t get_offset_of_btn_buy_24() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_buy_24)); }
	inline GameObject_t1113636619 * get_btn_buy_24() const { return ___btn_buy_24; }
	inline GameObject_t1113636619 ** get_address_of_btn_buy_24() { return &___btn_buy_24; }
	inline void set_btn_buy_24(GameObject_t1113636619 * value)
	{
		___btn_buy_24 = value;
		Il2CppCodeGenWriteBarrier((&___btn_buy_24), value);
	}

	inline static int32_t get_offset_of_nightThemeCost_25() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___nightThemeCost_25)); }
	inline int32_t get_nightThemeCost_25() const { return ___nightThemeCost_25; }
	inline int32_t* get_address_of_nightThemeCost_25() { return &___nightThemeCost_25; }
	inline void set_nightThemeCost_25(int32_t value)
	{
		___nightThemeCost_25 = value;
	}

	inline static int32_t get_offset_of_flappyThemeCost_26() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___flappyThemeCost_26)); }
	inline int32_t get_flappyThemeCost_26() const { return ___flappyThemeCost_26; }
	inline int32_t* get_address_of_flappyThemeCost_26() { return &___flappyThemeCost_26; }
	inline void set_flappyThemeCost_26(int32_t value)
	{
		___flappyThemeCost_26 = value;
	}

	inline static int32_t get_offset_of_desertThemeCost_27() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___desertThemeCost_27)); }
	inline int32_t get_desertThemeCost_27() const { return ___desertThemeCost_27; }
	inline int32_t* get_address_of_desertThemeCost_27() { return &___desertThemeCost_27; }
	inline void set_desertThemeCost_27(int32_t value)
	{
		___desertThemeCost_27 = value;
	}

	inline static int32_t get_offset_of_skyflightSoundCost_28() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___skyflightSoundCost_28)); }
	inline int32_t get_skyflightSoundCost_28() const { return ___skyflightSoundCost_28; }
	inline int32_t* get_address_of_skyflightSoundCost_28() { return &___skyflightSoundCost_28; }
	inline void set_skyflightSoundCost_28(int32_t value)
	{
		___skyflightSoundCost_28 = value;
	}

	inline static int32_t get_offset_of_dankstormSoundCost_29() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___dankstormSoundCost_29)); }
	inline int32_t get_dankstormSoundCost_29() const { return ___dankstormSoundCost_29; }
	inline int32_t* get_address_of_dankstormSoundCost_29() { return &___dankstormSoundCost_29; }
	inline void set_dankstormSoundCost_29(int32_t value)
	{
		___dankstormSoundCost_29 = value;
	}

	inline static int32_t get_offset_of_egyptSoundCost_30() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___egyptSoundCost_30)); }
	inline int32_t get_egyptSoundCost_30() const { return ___egyptSoundCost_30; }
	inline int32_t* get_address_of_egyptSoundCost_30() { return &___egyptSoundCost_30; }
	inline void set_egyptSoundCost_30(int32_t value)
	{
		___egyptSoundCost_30 = value;
	}

	inline static int32_t get_offset_of_cnv_menu_31() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___cnv_menu_31)); }
	inline GameObject_t1113636619 * get_cnv_menu_31() const { return ___cnv_menu_31; }
	inline GameObject_t1113636619 ** get_address_of_cnv_menu_31() { return &___cnv_menu_31; }
	inline void set_cnv_menu_31(GameObject_t1113636619 * value)
	{
		___cnv_menu_31 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_menu_31), value);
	}

	inline static int32_t get_offset_of_cnv_settings_32() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___cnv_settings_32)); }
	inline GameObject_t1113636619 * get_cnv_settings_32() const { return ___cnv_settings_32; }
	inline GameObject_t1113636619 ** get_address_of_cnv_settings_32() { return &___cnv_settings_32; }
	inline void set_cnv_settings_32(GameObject_t1113636619 * value)
	{
		___cnv_settings_32 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_settings_32), value);
	}

	inline static int32_t get_offset_of_cnv_game_33() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___cnv_game_33)); }
	inline GameObject_t1113636619 * get_cnv_game_33() const { return ___cnv_game_33; }
	inline GameObject_t1113636619 ** get_address_of_cnv_game_33() { return &___cnv_game_33; }
	inline void set_cnv_game_33(GameObject_t1113636619 * value)
	{
		___cnv_game_33 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_game_33), value);
	}

	inline static int32_t get_offset_of_go_permanent_34() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___go_permanent_34)); }
	inline GameObject_t1113636619 * get_go_permanent_34() const { return ___go_permanent_34; }
	inline GameObject_t1113636619 ** get_address_of_go_permanent_34() { return &___go_permanent_34; }
	inline void set_go_permanent_34(GameObject_t1113636619 * value)
	{
		___go_permanent_34 = value;
		Il2CppCodeGenWriteBarrier((&___go_permanent_34), value);
	}

	inline static int32_t get_offset_of_text_buy_35() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___text_buy_35)); }
	inline GameObject_t1113636619 * get_text_buy_35() const { return ___text_buy_35; }
	inline GameObject_t1113636619 ** get_address_of_text_buy_35() { return &___text_buy_35; }
	inline void set_text_buy_35(GameObject_t1113636619 * value)
	{
		___text_buy_35 = value;
		Il2CppCodeGenWriteBarrier((&___text_buy_35), value);
	}

	inline static int32_t get_offset_of_txt_cost_36() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___txt_cost_36)); }
	inline Text_t1901882714 * get_txt_cost_36() const { return ___txt_cost_36; }
	inline Text_t1901882714 ** get_address_of_txt_cost_36() { return &___txt_cost_36; }
	inline void set_txt_cost_36(Text_t1901882714 * value)
	{
		___txt_cost_36 = value;
		Il2CppCodeGenWriteBarrier((&___txt_cost_36), value);
	}

	inline static int32_t get_offset_of_scoreText_37() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___scoreText_37)); }
	inline Text_t1901882714 * get_scoreText_37() const { return ___scoreText_37; }
	inline Text_t1901882714 ** get_address_of_scoreText_37() { return &___scoreText_37; }
	inline void set_scoreText_37(Text_t1901882714 * value)
	{
		___scoreText_37 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_37), value);
	}

	inline static int32_t get_offset_of_specialPointsText_38() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___specialPointsText_38)); }
	inline Text_t1901882714 * get_specialPointsText_38() const { return ___specialPointsText_38; }
	inline Text_t1901882714 ** get_address_of_specialPointsText_38() { return &___specialPointsText_38; }
	inline void set_specialPointsText_38(Text_t1901882714 * value)
	{
		___specialPointsText_38 = value;
		Il2CppCodeGenWriteBarrier((&___specialPointsText_38), value);
	}

	inline static int32_t get_offset_of_anim_39() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___anim_39)); }
	inline Animator_t434523843 * get_anim_39() const { return ___anim_39; }
	inline Animator_t434523843 ** get_address_of_anim_39() { return &___anim_39; }
	inline void set_anim_39(Animator_t434523843 * value)
	{
		___anim_39 = value;
		Il2CppCodeGenWriteBarrier((&___anim_39), value);
	}

	inline static int32_t get_offset_of_triesTillAds_40() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___triesTillAds_40)); }
	inline int32_t get_triesTillAds_40() const { return ___triesTillAds_40; }
	inline int32_t* get_address_of_triesTillAds_40() { return &___triesTillAds_40; }
	inline void set_triesTillAds_40(int32_t value)
	{
		___triesTillAds_40 = value;
	}

	inline static int32_t get_offset_of_Cube_plusOne_41() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___Cube_plusOne_41)); }
	inline GameObject_t1113636619 * get_Cube_plusOne_41() const { return ___Cube_plusOne_41; }
	inline GameObject_t1113636619 ** get_address_of_Cube_plusOne_41() { return &___Cube_plusOne_41; }
	inline void set_Cube_plusOne_41(GameObject_t1113636619 * value)
	{
		___Cube_plusOne_41 = value;
		Il2CppCodeGenWriteBarrier((&___Cube_plusOne_41), value);
	}

	inline static int32_t get_offset_of_specialPoints_42() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___specialPoints_42)); }
	inline int32_t get_specialPoints_42() const { return ___specialPoints_42; }
	inline int32_t* get_address_of_specialPoints_42() { return &___specialPoints_42; }
	inline void set_specialPoints_42(int32_t value)
	{
		___specialPoints_42 = value;
	}

	inline static int32_t get_offset_of_singleObstacle_43() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___singleObstacle_43)); }
	inline GameObject_t1113636619 * get_singleObstacle_43() const { return ___singleObstacle_43; }
	inline GameObject_t1113636619 ** get_address_of_singleObstacle_43() { return &___singleObstacle_43; }
	inline void set_singleObstacle_43(GameObject_t1113636619 * value)
	{
		___singleObstacle_43 = value;
		Il2CppCodeGenWriteBarrier((&___singleObstacle_43), value);
	}

	inline static int32_t get_offset_of_doubleObstacle_44() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___doubleObstacle_44)); }
	inline GameObject_t1113636619 * get_doubleObstacle_44() const { return ___doubleObstacle_44; }
	inline GameObject_t1113636619 ** get_address_of_doubleObstacle_44() { return &___doubleObstacle_44; }
	inline void set_doubleObstacle_44(GameObject_t1113636619 * value)
	{
		___doubleObstacle_44 = value;
		Il2CppCodeGenWriteBarrier((&___doubleObstacle_44), value);
	}

	inline static int32_t get_offset_of_trippleObstacle_45() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___trippleObstacle_45)); }
	inline GameObject_t1113636619 * get_trippleObstacle_45() const { return ___trippleObstacle_45; }
	inline GameObject_t1113636619 ** get_address_of_trippleObstacle_45() { return &___trippleObstacle_45; }
	inline void set_trippleObstacle_45(GameObject_t1113636619 * value)
	{
		___trippleObstacle_45 = value;
		Il2CppCodeGenWriteBarrier((&___trippleObstacle_45), value);
	}

	inline static int32_t get_offset_of_quattroObstacle_46() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___quattroObstacle_46)); }
	inline GameObject_t1113636619 * get_quattroObstacle_46() const { return ___quattroObstacle_46; }
	inline GameObject_t1113636619 ** get_address_of_quattroObstacle_46() { return &___quattroObstacle_46; }
	inline void set_quattroObstacle_46(GameObject_t1113636619 * value)
	{
		___quattroObstacle_46 = value;
		Il2CppCodeGenWriteBarrier((&___quattroObstacle_46), value);
	}

	inline static int32_t get_offset_of_previewTime_47() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___previewTime_47)); }
	inline float get_previewTime_47() const { return ___previewTime_47; }
	inline float* get_address_of_previewTime_47() { return &___previewTime_47; }
	inline void set_previewTime_47(float value)
	{
		___previewTime_47 = value;
	}

	inline static int32_t get_offset_of_btn_difficulty_48() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_difficulty_48)); }
	inline Toggle_t2735377061 * get_btn_difficulty_48() const { return ___btn_difficulty_48; }
	inline Toggle_t2735377061 ** get_address_of_btn_difficulty_48() { return &___btn_difficulty_48; }
	inline void set_btn_difficulty_48(Toggle_t2735377061 * value)
	{
		___btn_difficulty_48 = value;
		Il2CppCodeGenWriteBarrier((&___btn_difficulty_48), value);
	}

	inline static int32_t get_offset_of_difficulty_49() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___difficulty_49)); }
	inline String_t* get_difficulty_49() const { return ___difficulty_49; }
	inline String_t** get_address_of_difficulty_49() { return &___difficulty_49; }
	inline void set_difficulty_49(String_t* value)
	{
		___difficulty_49 = value;
		Il2CppCodeGenWriteBarrier((&___difficulty_49), value);
	}

	inline static int32_t get_offset_of_textHighscoreCount_50() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___textHighscoreCount_50)); }
	inline Text_t1901882714 * get_textHighscoreCount_50() const { return ___textHighscoreCount_50; }
	inline Text_t1901882714 ** get_address_of_textHighscoreCount_50() { return &___textHighscoreCount_50; }
	inline void set_textHighscoreCount_50(Text_t1901882714 * value)
	{
		___textHighscoreCount_50 = value;
		Il2CppCodeGenWriteBarrier((&___textHighscoreCount_50), value);
	}

	inline static int32_t get_offset_of_highscore_51() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___highscore_51)); }
	inline int32_t get_highscore_51() const { return ___highscore_51; }
	inline int32_t* get_address_of_highscore_51() { return &___highscore_51; }
	inline void set_highscore_51(int32_t value)
	{
		___highscore_51 = value;
	}

	inline static int32_t get_offset_of_textPlayedCount_52() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___textPlayedCount_52)); }
	inline Text_t1901882714 * get_textPlayedCount_52() const { return ___textPlayedCount_52; }
	inline Text_t1901882714 ** get_address_of_textPlayedCount_52() { return &___textPlayedCount_52; }
	inline void set_textPlayedCount_52(Text_t1901882714 * value)
	{
		___textPlayedCount_52 = value;
		Il2CppCodeGenWriteBarrier((&___textPlayedCount_52), value);
	}

	inline static int32_t get_offset_of_tries_53() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___tries_53)); }
	inline int32_t get_tries_53() const { return ___tries_53; }
	inline int32_t* get_address_of_tries_53() { return &___tries_53; }
	inline void set_tries_53(int32_t value)
	{
		___tries_53 = value;
	}

	inline static int32_t get_offset_of_textLastPoints_54() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___textLastPoints_54)); }
	inline Text_t1901882714 * get_textLastPoints_54() const { return ___textLastPoints_54; }
	inline Text_t1901882714 ** get_address_of_textLastPoints_54() { return &___textLastPoints_54; }
	inline void set_textLastPoints_54(Text_t1901882714 * value)
	{
		___textLastPoints_54 = value;
		Il2CppCodeGenWriteBarrier((&___textLastPoints_54), value);
	}

	inline static int32_t get_offset_of_lastPoints_55() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___lastPoints_55)); }
	inline int32_t get_lastPoints_55() const { return ___lastPoints_55; }
	inline int32_t* get_address_of_lastPoints_55() { return &___lastPoints_55; }
	inline void set_lastPoints_55(int32_t value)
	{
		___lastPoints_55 = value;
	}

	inline static int32_t get_offset_of_ckm_mainTheme_56() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_mainTheme_56)); }
	inline GameObject_t1113636619 * get_ckm_mainTheme_56() const { return ___ckm_mainTheme_56; }
	inline GameObject_t1113636619 ** get_address_of_ckm_mainTheme_56() { return &___ckm_mainTheme_56; }
	inline void set_ckm_mainTheme_56(GameObject_t1113636619 * value)
	{
		___ckm_mainTheme_56 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_mainTheme_56), value);
	}

	inline static int32_t get_offset_of_ckm_nightTheme_57() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_nightTheme_57)); }
	inline GameObject_t1113636619 * get_ckm_nightTheme_57() const { return ___ckm_nightTheme_57; }
	inline GameObject_t1113636619 ** get_address_of_ckm_nightTheme_57() { return &___ckm_nightTheme_57; }
	inline void set_ckm_nightTheme_57(GameObject_t1113636619 * value)
	{
		___ckm_nightTheme_57 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_nightTheme_57), value);
	}

	inline static int32_t get_offset_of_ckm_flappyTheme_58() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_flappyTheme_58)); }
	inline GameObject_t1113636619 * get_ckm_flappyTheme_58() const { return ___ckm_flappyTheme_58; }
	inline GameObject_t1113636619 ** get_address_of_ckm_flappyTheme_58() { return &___ckm_flappyTheme_58; }
	inline void set_ckm_flappyTheme_58(GameObject_t1113636619 * value)
	{
		___ckm_flappyTheme_58 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_flappyTheme_58), value);
	}

	inline static int32_t get_offset_of_ckm_desertTheme_59() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_desertTheme_59)); }
	inline GameObject_t1113636619 * get_ckm_desertTheme_59() const { return ___ckm_desertTheme_59; }
	inline GameObject_t1113636619 ** get_address_of_ckm_desertTheme_59() { return &___ckm_desertTheme_59; }
	inline void set_ckm_desertTheme_59(GameObject_t1113636619 * value)
	{
		___ckm_desertTheme_59 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_desertTheme_59), value);
	}

	inline static int32_t get_offset_of_ckm_noSound_60() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_noSound_60)); }
	inline GameObject_t1113636619 * get_ckm_noSound_60() const { return ___ckm_noSound_60; }
	inline GameObject_t1113636619 ** get_address_of_ckm_noSound_60() { return &___ckm_noSound_60; }
	inline void set_ckm_noSound_60(GameObject_t1113636619 * value)
	{
		___ckm_noSound_60 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_noSound_60), value);
	}

	inline static int32_t get_offset_of_ckm_mainSound_61() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_mainSound_61)); }
	inline GameObject_t1113636619 * get_ckm_mainSound_61() const { return ___ckm_mainSound_61; }
	inline GameObject_t1113636619 ** get_address_of_ckm_mainSound_61() { return &___ckm_mainSound_61; }
	inline void set_ckm_mainSound_61(GameObject_t1113636619 * value)
	{
		___ckm_mainSound_61 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_mainSound_61), value);
	}

	inline static int32_t get_offset_of_ckm_skyflightSound_62() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_skyflightSound_62)); }
	inline GameObject_t1113636619 * get_ckm_skyflightSound_62() const { return ___ckm_skyflightSound_62; }
	inline GameObject_t1113636619 ** get_address_of_ckm_skyflightSound_62() { return &___ckm_skyflightSound_62; }
	inline void set_ckm_skyflightSound_62(GameObject_t1113636619 * value)
	{
		___ckm_skyflightSound_62 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_skyflightSound_62), value);
	}

	inline static int32_t get_offset_of_ckm_dankstormSound_63() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_dankstormSound_63)); }
	inline GameObject_t1113636619 * get_ckm_dankstormSound_63() const { return ___ckm_dankstormSound_63; }
	inline GameObject_t1113636619 ** get_address_of_ckm_dankstormSound_63() { return &___ckm_dankstormSound_63; }
	inline void set_ckm_dankstormSound_63(GameObject_t1113636619 * value)
	{
		___ckm_dankstormSound_63 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_dankstormSound_63), value);
	}

	inline static int32_t get_offset_of_ckm_egyptSound_64() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_egyptSound_64)); }
	inline GameObject_t1113636619 * get_ckm_egyptSound_64() const { return ___ckm_egyptSound_64; }
	inline GameObject_t1113636619 ** get_address_of_ckm_egyptSound_64() { return &___ckm_egyptSound_64; }
	inline void set_ckm_egyptSound_64(GameObject_t1113636619 * value)
	{
		___ckm_egyptSound_64 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_egyptSound_64), value);
	}

	inline static int32_t get_offset_of_texts_65() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___texts_65)); }
	inline GameObject_t1113636619 * get_texts_65() const { return ___texts_65; }
	inline GameObject_t1113636619 ** get_address_of_texts_65() { return &___texts_65; }
	inline void set_texts_65(GameObject_t1113636619 * value)
	{
		___texts_65 = value;
		Il2CppCodeGenWriteBarrier((&___texts_65), value);
	}

	inline static int32_t get_offset_of_svSounds_66() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___svSounds_66)); }
	inline ScrollRect_t4137855814 * get_svSounds_66() const { return ___svSounds_66; }
	inline ScrollRect_t4137855814 ** get_address_of_svSounds_66() { return &___svSounds_66; }
	inline void set_svSounds_66(ScrollRect_t4137855814 * value)
	{
		___svSounds_66 = value;
		Il2CppCodeGenWriteBarrier((&___svSounds_66), value);
	}

	inline static int32_t get_offset_of_svThemes_67() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___svThemes_67)); }
	inline ScrollRect_t4137855814 * get_svThemes_67() const { return ___svThemes_67; }
	inline ScrollRect_t4137855814 ** get_address_of_svThemes_67() { return &___svThemes_67; }
	inline void set_svThemes_67(ScrollRect_t4137855814 * value)
	{
		___svThemes_67 = value;
		Il2CppCodeGenWriteBarrier((&___svThemes_67), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCONTROLLERSCRIPT_T2432915718_H
#ifndef INITTEXTSSCRIPT_T1053055510_H
#define INITTEXTSSCRIPT_T1053055510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitTextsScript
struct  InitTextsScript_t1053055510  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject InitTextsScript::cnv_menu
	GameObject_t1113636619 * ___cnv_menu_2;
	// UnityEngine.GameObject InitTextsScript::cnv_settings
	GameObject_t1113636619 * ___cnv_settings_3;
	// UnityEngine.GameObject InitTextsScript::cnv_game
	GameObject_t1113636619 * ___cnv_game_4;
	// UnityEngine.GameObject InitTextsScript::texts
	GameObject_t1113636619 * ___texts_5;
	// UnityEngine.UI.Text InitTextsScript::textHighscoreCount
	Text_t1901882714 * ___textHighscoreCount_6;
	// System.Int32 InitTextsScript::highscore
	int32_t ___highscore_7;
	// UnityEngine.UI.Text InitTextsScript::textPlayedCount
	Text_t1901882714 * ___textPlayedCount_8;
	// System.Int32 InitTextsScript::tries
	int32_t ___tries_9;
	// UnityEngine.UI.Text InitTextsScript::textLastPoints
	Text_t1901882714 * ___textLastPoints_10;
	// System.Int32 InitTextsScript::lastPoints
	int32_t ___lastPoints_11;
	// UnityEngine.UI.Text InitTextsScript::scoreText
	Text_t1901882714 * ___scoreText_12;
	// UnityEngine.UI.Text InitTextsScript::specialPointsText
	Text_t1901882714 * ___specialPointsText_13;
	// UnityEngine.GameObject InitTextsScript::backgroundSpriteMenu
	GameObject_t1113636619 * ___backgroundSpriteMenu_14;
	// UnityEngine.GameObject InitTextsScript::backgroundSpriteSettings
	GameObject_t1113636619 * ___backgroundSpriteSettings_15;
	// UnityEngine.GameObject InitTextsScript::backgroundSpriteGame
	GameObject_t1113636619 * ___backgroundSpriteGame_16;
	// UnityEngine.Sprite InitTextsScript::mainTheme
	Sprite_t280657092 * ___mainTheme_17;
	// UnityEngine.Sprite InitTextsScript::flappyTheme
	Sprite_t280657092 * ___flappyTheme_18;
	// UnityEngine.Sprite InitTextsScript::nightTheme
	Sprite_t280657092 * ___nightTheme_19;
	// UnityEngine.Sprite InitTextsScript::desertTheme
	Sprite_t280657092 * ___desertTheme_20;
	// UnityEngine.SpriteRenderer InitTextsScript::bgSprite
	SpriteRenderer_t3235626157 * ___bgSprite_21;
	// UnityEngine.GameObject InitTextsScript::audioSource
	GameObject_t1113636619 * ___audioSource_22;
	// UnityEngine.AudioClip InitTextsScript::clip_mainSound
	AudioClip_t3680889665 * ___clip_mainSound_23;
	// UnityEngine.AudioClip InitTextsScript::clip_dankstormSound
	AudioClip_t3680889665 * ___clip_dankstormSound_24;
	// UnityEngine.AudioClip InitTextsScript::clip_skyflightSound
	AudioClip_t3680889665 * ___clip_skyflightSound_25;
	// UnityEngine.AudioClip InitTextsScript::clip_egyptSound
	AudioClip_t3680889665 * ___clip_egyptSound_26;
	// System.Single InitTextsScript::previewTime
	float ___previewTime_27;
	// System.String InitTextsScript::difficulty
	String_t* ___difficulty_28;

public:
	inline static int32_t get_offset_of_cnv_menu_2() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___cnv_menu_2)); }
	inline GameObject_t1113636619 * get_cnv_menu_2() const { return ___cnv_menu_2; }
	inline GameObject_t1113636619 ** get_address_of_cnv_menu_2() { return &___cnv_menu_2; }
	inline void set_cnv_menu_2(GameObject_t1113636619 * value)
	{
		___cnv_menu_2 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_menu_2), value);
	}

	inline static int32_t get_offset_of_cnv_settings_3() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___cnv_settings_3)); }
	inline GameObject_t1113636619 * get_cnv_settings_3() const { return ___cnv_settings_3; }
	inline GameObject_t1113636619 ** get_address_of_cnv_settings_3() { return &___cnv_settings_3; }
	inline void set_cnv_settings_3(GameObject_t1113636619 * value)
	{
		___cnv_settings_3 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_settings_3), value);
	}

	inline static int32_t get_offset_of_cnv_game_4() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___cnv_game_4)); }
	inline GameObject_t1113636619 * get_cnv_game_4() const { return ___cnv_game_4; }
	inline GameObject_t1113636619 ** get_address_of_cnv_game_4() { return &___cnv_game_4; }
	inline void set_cnv_game_4(GameObject_t1113636619 * value)
	{
		___cnv_game_4 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_game_4), value);
	}

	inline static int32_t get_offset_of_texts_5() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___texts_5)); }
	inline GameObject_t1113636619 * get_texts_5() const { return ___texts_5; }
	inline GameObject_t1113636619 ** get_address_of_texts_5() { return &___texts_5; }
	inline void set_texts_5(GameObject_t1113636619 * value)
	{
		___texts_5 = value;
		Il2CppCodeGenWriteBarrier((&___texts_5), value);
	}

	inline static int32_t get_offset_of_textHighscoreCount_6() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___textHighscoreCount_6)); }
	inline Text_t1901882714 * get_textHighscoreCount_6() const { return ___textHighscoreCount_6; }
	inline Text_t1901882714 ** get_address_of_textHighscoreCount_6() { return &___textHighscoreCount_6; }
	inline void set_textHighscoreCount_6(Text_t1901882714 * value)
	{
		___textHighscoreCount_6 = value;
		Il2CppCodeGenWriteBarrier((&___textHighscoreCount_6), value);
	}

	inline static int32_t get_offset_of_highscore_7() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___highscore_7)); }
	inline int32_t get_highscore_7() const { return ___highscore_7; }
	inline int32_t* get_address_of_highscore_7() { return &___highscore_7; }
	inline void set_highscore_7(int32_t value)
	{
		___highscore_7 = value;
	}

	inline static int32_t get_offset_of_textPlayedCount_8() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___textPlayedCount_8)); }
	inline Text_t1901882714 * get_textPlayedCount_8() const { return ___textPlayedCount_8; }
	inline Text_t1901882714 ** get_address_of_textPlayedCount_8() { return &___textPlayedCount_8; }
	inline void set_textPlayedCount_8(Text_t1901882714 * value)
	{
		___textPlayedCount_8 = value;
		Il2CppCodeGenWriteBarrier((&___textPlayedCount_8), value);
	}

	inline static int32_t get_offset_of_tries_9() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___tries_9)); }
	inline int32_t get_tries_9() const { return ___tries_9; }
	inline int32_t* get_address_of_tries_9() { return &___tries_9; }
	inline void set_tries_9(int32_t value)
	{
		___tries_9 = value;
	}

	inline static int32_t get_offset_of_textLastPoints_10() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___textLastPoints_10)); }
	inline Text_t1901882714 * get_textLastPoints_10() const { return ___textLastPoints_10; }
	inline Text_t1901882714 ** get_address_of_textLastPoints_10() { return &___textLastPoints_10; }
	inline void set_textLastPoints_10(Text_t1901882714 * value)
	{
		___textLastPoints_10 = value;
		Il2CppCodeGenWriteBarrier((&___textLastPoints_10), value);
	}

	inline static int32_t get_offset_of_lastPoints_11() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___lastPoints_11)); }
	inline int32_t get_lastPoints_11() const { return ___lastPoints_11; }
	inline int32_t* get_address_of_lastPoints_11() { return &___lastPoints_11; }
	inline void set_lastPoints_11(int32_t value)
	{
		___lastPoints_11 = value;
	}

	inline static int32_t get_offset_of_scoreText_12() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___scoreText_12)); }
	inline Text_t1901882714 * get_scoreText_12() const { return ___scoreText_12; }
	inline Text_t1901882714 ** get_address_of_scoreText_12() { return &___scoreText_12; }
	inline void set_scoreText_12(Text_t1901882714 * value)
	{
		___scoreText_12 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_12), value);
	}

	inline static int32_t get_offset_of_specialPointsText_13() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___specialPointsText_13)); }
	inline Text_t1901882714 * get_specialPointsText_13() const { return ___specialPointsText_13; }
	inline Text_t1901882714 ** get_address_of_specialPointsText_13() { return &___specialPointsText_13; }
	inline void set_specialPointsText_13(Text_t1901882714 * value)
	{
		___specialPointsText_13 = value;
		Il2CppCodeGenWriteBarrier((&___specialPointsText_13), value);
	}

	inline static int32_t get_offset_of_backgroundSpriteMenu_14() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___backgroundSpriteMenu_14)); }
	inline GameObject_t1113636619 * get_backgroundSpriteMenu_14() const { return ___backgroundSpriteMenu_14; }
	inline GameObject_t1113636619 ** get_address_of_backgroundSpriteMenu_14() { return &___backgroundSpriteMenu_14; }
	inline void set_backgroundSpriteMenu_14(GameObject_t1113636619 * value)
	{
		___backgroundSpriteMenu_14 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSpriteMenu_14), value);
	}

	inline static int32_t get_offset_of_backgroundSpriteSettings_15() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___backgroundSpriteSettings_15)); }
	inline GameObject_t1113636619 * get_backgroundSpriteSettings_15() const { return ___backgroundSpriteSettings_15; }
	inline GameObject_t1113636619 ** get_address_of_backgroundSpriteSettings_15() { return &___backgroundSpriteSettings_15; }
	inline void set_backgroundSpriteSettings_15(GameObject_t1113636619 * value)
	{
		___backgroundSpriteSettings_15 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSpriteSettings_15), value);
	}

	inline static int32_t get_offset_of_backgroundSpriteGame_16() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___backgroundSpriteGame_16)); }
	inline GameObject_t1113636619 * get_backgroundSpriteGame_16() const { return ___backgroundSpriteGame_16; }
	inline GameObject_t1113636619 ** get_address_of_backgroundSpriteGame_16() { return &___backgroundSpriteGame_16; }
	inline void set_backgroundSpriteGame_16(GameObject_t1113636619 * value)
	{
		___backgroundSpriteGame_16 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSpriteGame_16), value);
	}

	inline static int32_t get_offset_of_mainTheme_17() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___mainTheme_17)); }
	inline Sprite_t280657092 * get_mainTheme_17() const { return ___mainTheme_17; }
	inline Sprite_t280657092 ** get_address_of_mainTheme_17() { return &___mainTheme_17; }
	inline void set_mainTheme_17(Sprite_t280657092 * value)
	{
		___mainTheme_17 = value;
		Il2CppCodeGenWriteBarrier((&___mainTheme_17), value);
	}

	inline static int32_t get_offset_of_flappyTheme_18() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___flappyTheme_18)); }
	inline Sprite_t280657092 * get_flappyTheme_18() const { return ___flappyTheme_18; }
	inline Sprite_t280657092 ** get_address_of_flappyTheme_18() { return &___flappyTheme_18; }
	inline void set_flappyTheme_18(Sprite_t280657092 * value)
	{
		___flappyTheme_18 = value;
		Il2CppCodeGenWriteBarrier((&___flappyTheme_18), value);
	}

	inline static int32_t get_offset_of_nightTheme_19() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___nightTheme_19)); }
	inline Sprite_t280657092 * get_nightTheme_19() const { return ___nightTheme_19; }
	inline Sprite_t280657092 ** get_address_of_nightTheme_19() { return &___nightTheme_19; }
	inline void set_nightTheme_19(Sprite_t280657092 * value)
	{
		___nightTheme_19 = value;
		Il2CppCodeGenWriteBarrier((&___nightTheme_19), value);
	}

	inline static int32_t get_offset_of_desertTheme_20() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___desertTheme_20)); }
	inline Sprite_t280657092 * get_desertTheme_20() const { return ___desertTheme_20; }
	inline Sprite_t280657092 ** get_address_of_desertTheme_20() { return &___desertTheme_20; }
	inline void set_desertTheme_20(Sprite_t280657092 * value)
	{
		___desertTheme_20 = value;
		Il2CppCodeGenWriteBarrier((&___desertTheme_20), value);
	}

	inline static int32_t get_offset_of_bgSprite_21() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___bgSprite_21)); }
	inline SpriteRenderer_t3235626157 * get_bgSprite_21() const { return ___bgSprite_21; }
	inline SpriteRenderer_t3235626157 ** get_address_of_bgSprite_21() { return &___bgSprite_21; }
	inline void set_bgSprite_21(SpriteRenderer_t3235626157 * value)
	{
		___bgSprite_21 = value;
		Il2CppCodeGenWriteBarrier((&___bgSprite_21), value);
	}

	inline static int32_t get_offset_of_audioSource_22() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___audioSource_22)); }
	inline GameObject_t1113636619 * get_audioSource_22() const { return ___audioSource_22; }
	inline GameObject_t1113636619 ** get_address_of_audioSource_22() { return &___audioSource_22; }
	inline void set_audioSource_22(GameObject_t1113636619 * value)
	{
		___audioSource_22 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_22), value);
	}

	inline static int32_t get_offset_of_clip_mainSound_23() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___clip_mainSound_23)); }
	inline AudioClip_t3680889665 * get_clip_mainSound_23() const { return ___clip_mainSound_23; }
	inline AudioClip_t3680889665 ** get_address_of_clip_mainSound_23() { return &___clip_mainSound_23; }
	inline void set_clip_mainSound_23(AudioClip_t3680889665 * value)
	{
		___clip_mainSound_23 = value;
		Il2CppCodeGenWriteBarrier((&___clip_mainSound_23), value);
	}

	inline static int32_t get_offset_of_clip_dankstormSound_24() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___clip_dankstormSound_24)); }
	inline AudioClip_t3680889665 * get_clip_dankstormSound_24() const { return ___clip_dankstormSound_24; }
	inline AudioClip_t3680889665 ** get_address_of_clip_dankstormSound_24() { return &___clip_dankstormSound_24; }
	inline void set_clip_dankstormSound_24(AudioClip_t3680889665 * value)
	{
		___clip_dankstormSound_24 = value;
		Il2CppCodeGenWriteBarrier((&___clip_dankstormSound_24), value);
	}

	inline static int32_t get_offset_of_clip_skyflightSound_25() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___clip_skyflightSound_25)); }
	inline AudioClip_t3680889665 * get_clip_skyflightSound_25() const { return ___clip_skyflightSound_25; }
	inline AudioClip_t3680889665 ** get_address_of_clip_skyflightSound_25() { return &___clip_skyflightSound_25; }
	inline void set_clip_skyflightSound_25(AudioClip_t3680889665 * value)
	{
		___clip_skyflightSound_25 = value;
		Il2CppCodeGenWriteBarrier((&___clip_skyflightSound_25), value);
	}

	inline static int32_t get_offset_of_clip_egyptSound_26() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___clip_egyptSound_26)); }
	inline AudioClip_t3680889665 * get_clip_egyptSound_26() const { return ___clip_egyptSound_26; }
	inline AudioClip_t3680889665 ** get_address_of_clip_egyptSound_26() { return &___clip_egyptSound_26; }
	inline void set_clip_egyptSound_26(AudioClip_t3680889665 * value)
	{
		___clip_egyptSound_26 = value;
		Il2CppCodeGenWriteBarrier((&___clip_egyptSound_26), value);
	}

	inline static int32_t get_offset_of_previewTime_27() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___previewTime_27)); }
	inline float get_previewTime_27() const { return ___previewTime_27; }
	inline float* get_address_of_previewTime_27() { return &___previewTime_27; }
	inline void set_previewTime_27(float value)
	{
		___previewTime_27 = value;
	}

	inline static int32_t get_offset_of_difficulty_28() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___difficulty_28)); }
	inline String_t* get_difficulty_28() const { return ___difficulty_28; }
	inline String_t** get_address_of_difficulty_28() { return &___difficulty_28; }
	inline void set_difficulty_28(String_t* value)
	{
		___difficulty_28 = value;
		Il2CppCodeGenWriteBarrier((&___difficulty_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITTEXTSSCRIPT_T1053055510_H
#ifndef TRIPPLETRIGGERMIDDLESCRIPT_T4217752517_H
#define TRIPPLETRIGGERMIDDLESCRIPT_T4217752517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerMiddleScript
struct  TrippleTriggerMiddleScript_t4217752517  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TrippleTriggerMiddleScript::Game
	GameObject_t1113636619 * ___Game_2;
	// UnityEngine.GameObject TrippleTriggerMiddleScript::Menu
	GameObject_t1113636619 * ___Menu_3;
	// UnityEngine.AudioSource TrippleTriggerMiddleScript::audioSrc
	AudioSource_t3935305588 * ___audioSrc_4;
	// System.Int32 TrippleTriggerMiddleScript::triesTillAds
	int32_t ___triesTillAds_5;
	// UnityEngine.GameObject TrippleTriggerMiddleScript::btn_play
	GameObject_t1113636619 * ___btn_play_6;
	// UnityEngine.GameObject TrippleTriggerMiddleScript::text_loading
	GameObject_t1113636619 * ___text_loading_7;
	// UnityEngine.Animator TrippleTriggerMiddleScript::anim
	Animator_t434523843 * ___anim_8;
	// System.Single TrippleTriggerMiddleScript::savedTimeScale
	float ___savedTimeScale_9;

public:
	inline static int32_t get_offset_of_Game_2() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___Game_2)); }
	inline GameObject_t1113636619 * get_Game_2() const { return ___Game_2; }
	inline GameObject_t1113636619 ** get_address_of_Game_2() { return &___Game_2; }
	inline void set_Game_2(GameObject_t1113636619 * value)
	{
		___Game_2 = value;
		Il2CppCodeGenWriteBarrier((&___Game_2), value);
	}

	inline static int32_t get_offset_of_Menu_3() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___Menu_3)); }
	inline GameObject_t1113636619 * get_Menu_3() const { return ___Menu_3; }
	inline GameObject_t1113636619 ** get_address_of_Menu_3() { return &___Menu_3; }
	inline void set_Menu_3(GameObject_t1113636619 * value)
	{
		___Menu_3 = value;
		Il2CppCodeGenWriteBarrier((&___Menu_3), value);
	}

	inline static int32_t get_offset_of_audioSrc_4() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___audioSrc_4)); }
	inline AudioSource_t3935305588 * get_audioSrc_4() const { return ___audioSrc_4; }
	inline AudioSource_t3935305588 ** get_address_of_audioSrc_4() { return &___audioSrc_4; }
	inline void set_audioSrc_4(AudioSource_t3935305588 * value)
	{
		___audioSrc_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioSrc_4), value);
	}

	inline static int32_t get_offset_of_triesTillAds_5() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___triesTillAds_5)); }
	inline int32_t get_triesTillAds_5() const { return ___triesTillAds_5; }
	inline int32_t* get_address_of_triesTillAds_5() { return &___triesTillAds_5; }
	inline void set_triesTillAds_5(int32_t value)
	{
		___triesTillAds_5 = value;
	}

	inline static int32_t get_offset_of_btn_play_6() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___btn_play_6)); }
	inline GameObject_t1113636619 * get_btn_play_6() const { return ___btn_play_6; }
	inline GameObject_t1113636619 ** get_address_of_btn_play_6() { return &___btn_play_6; }
	inline void set_btn_play_6(GameObject_t1113636619 * value)
	{
		___btn_play_6 = value;
		Il2CppCodeGenWriteBarrier((&___btn_play_6), value);
	}

	inline static int32_t get_offset_of_text_loading_7() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___text_loading_7)); }
	inline GameObject_t1113636619 * get_text_loading_7() const { return ___text_loading_7; }
	inline GameObject_t1113636619 ** get_address_of_text_loading_7() { return &___text_loading_7; }
	inline void set_text_loading_7(GameObject_t1113636619 * value)
	{
		___text_loading_7 = value;
		Il2CppCodeGenWriteBarrier((&___text_loading_7), value);
	}

	inline static int32_t get_offset_of_anim_8() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___anim_8)); }
	inline Animator_t434523843 * get_anim_8() const { return ___anim_8; }
	inline Animator_t434523843 ** get_address_of_anim_8() { return &___anim_8; }
	inline void set_anim_8(Animator_t434523843 * value)
	{
		___anim_8 = value;
		Il2CppCodeGenWriteBarrier((&___anim_8), value);
	}

	inline static int32_t get_offset_of_savedTimeScale_9() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___savedTimeScale_9)); }
	inline float get_savedTimeScale_9() const { return ___savedTimeScale_9; }
	inline float* get_address_of_savedTimeScale_9() { return &___savedTimeScale_9; }
	inline void set_savedTimeScale_9(float value)
	{
		___savedTimeScale_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIPPLETRIGGERMIDDLESCRIPT_T4217752517_H
#ifndef INITBUTTONSSCRIPT_T1987868624_H
#define INITBUTTONSSCRIPT_T1987868624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitButtonsScript
struct  InitButtonsScript_t1987868624  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject InitButtonsScript::btn_nightThemeInactive
	GameObject_t1113636619 * ___btn_nightThemeInactive_2;
	// UnityEngine.GameObject InitButtonsScript::btn_flappyThemeInactive
	GameObject_t1113636619 * ___btn_flappyThemeInactive_3;
	// UnityEngine.GameObject InitButtonsScript::btn_desertThemeInactive
	GameObject_t1113636619 * ___btn_desertThemeInactive_4;
	// UnityEngine.GameObject InitButtonsScript::btn_skyflightSoundInactive
	GameObject_t1113636619 * ___btn_skyflightSoundInactive_5;
	// UnityEngine.GameObject InitButtonsScript::btn_dankstormSoundInactive
	GameObject_t1113636619 * ___btn_dankstormSoundInactive_6;
	// UnityEngine.GameObject InitButtonsScript::btn_egyptSoundInactive
	GameObject_t1113636619 * ___btn_egyptSoundInactive_7;
	// UnityEngine.GameObject InitButtonsScript::btn_difficulty
	GameObject_t1113636619 * ___btn_difficulty_8;
	// UnityEngine.GameObject InitButtonsScript::ckm_mainTheme
	GameObject_t1113636619 * ___ckm_mainTheme_9;
	// UnityEngine.GameObject InitButtonsScript::ckm_nightTheme
	GameObject_t1113636619 * ___ckm_nightTheme_10;
	// UnityEngine.GameObject InitButtonsScript::ckm_flappyTheme
	GameObject_t1113636619 * ___ckm_flappyTheme_11;
	// UnityEngine.GameObject InitButtonsScript::ckm_desertTheme
	GameObject_t1113636619 * ___ckm_desertTheme_12;
	// UnityEngine.GameObject InitButtonsScript::ckm_noSound
	GameObject_t1113636619 * ___ckm_noSound_13;
	// UnityEngine.GameObject InitButtonsScript::ckm_mainSound
	GameObject_t1113636619 * ___ckm_mainSound_14;
	// UnityEngine.GameObject InitButtonsScript::ckm_skyflightSound
	GameObject_t1113636619 * ___ckm_skyflightSound_15;
	// UnityEngine.GameObject InitButtonsScript::ckm_dankstormSound
	GameObject_t1113636619 * ___ckm_dankstormSound_16;
	// UnityEngine.GameObject InitButtonsScript::ckm_egyptSound
	GameObject_t1113636619 * ___ckm_egyptSound_17;

public:
	inline static int32_t get_offset_of_btn_nightThemeInactive_2() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_nightThemeInactive_2)); }
	inline GameObject_t1113636619 * get_btn_nightThemeInactive_2() const { return ___btn_nightThemeInactive_2; }
	inline GameObject_t1113636619 ** get_address_of_btn_nightThemeInactive_2() { return &___btn_nightThemeInactive_2; }
	inline void set_btn_nightThemeInactive_2(GameObject_t1113636619 * value)
	{
		___btn_nightThemeInactive_2 = value;
		Il2CppCodeGenWriteBarrier((&___btn_nightThemeInactive_2), value);
	}

	inline static int32_t get_offset_of_btn_flappyThemeInactive_3() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_flappyThemeInactive_3)); }
	inline GameObject_t1113636619 * get_btn_flappyThemeInactive_3() const { return ___btn_flappyThemeInactive_3; }
	inline GameObject_t1113636619 ** get_address_of_btn_flappyThemeInactive_3() { return &___btn_flappyThemeInactive_3; }
	inline void set_btn_flappyThemeInactive_3(GameObject_t1113636619 * value)
	{
		___btn_flappyThemeInactive_3 = value;
		Il2CppCodeGenWriteBarrier((&___btn_flappyThemeInactive_3), value);
	}

	inline static int32_t get_offset_of_btn_desertThemeInactive_4() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_desertThemeInactive_4)); }
	inline GameObject_t1113636619 * get_btn_desertThemeInactive_4() const { return ___btn_desertThemeInactive_4; }
	inline GameObject_t1113636619 ** get_address_of_btn_desertThemeInactive_4() { return &___btn_desertThemeInactive_4; }
	inline void set_btn_desertThemeInactive_4(GameObject_t1113636619 * value)
	{
		___btn_desertThemeInactive_4 = value;
		Il2CppCodeGenWriteBarrier((&___btn_desertThemeInactive_4), value);
	}

	inline static int32_t get_offset_of_btn_skyflightSoundInactive_5() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_skyflightSoundInactive_5)); }
	inline GameObject_t1113636619 * get_btn_skyflightSoundInactive_5() const { return ___btn_skyflightSoundInactive_5; }
	inline GameObject_t1113636619 ** get_address_of_btn_skyflightSoundInactive_5() { return &___btn_skyflightSoundInactive_5; }
	inline void set_btn_skyflightSoundInactive_5(GameObject_t1113636619 * value)
	{
		___btn_skyflightSoundInactive_5 = value;
		Il2CppCodeGenWriteBarrier((&___btn_skyflightSoundInactive_5), value);
	}

	inline static int32_t get_offset_of_btn_dankstormSoundInactive_6() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_dankstormSoundInactive_6)); }
	inline GameObject_t1113636619 * get_btn_dankstormSoundInactive_6() const { return ___btn_dankstormSoundInactive_6; }
	inline GameObject_t1113636619 ** get_address_of_btn_dankstormSoundInactive_6() { return &___btn_dankstormSoundInactive_6; }
	inline void set_btn_dankstormSoundInactive_6(GameObject_t1113636619 * value)
	{
		___btn_dankstormSoundInactive_6 = value;
		Il2CppCodeGenWriteBarrier((&___btn_dankstormSoundInactive_6), value);
	}

	inline static int32_t get_offset_of_btn_egyptSoundInactive_7() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_egyptSoundInactive_7)); }
	inline GameObject_t1113636619 * get_btn_egyptSoundInactive_7() const { return ___btn_egyptSoundInactive_7; }
	inline GameObject_t1113636619 ** get_address_of_btn_egyptSoundInactive_7() { return &___btn_egyptSoundInactive_7; }
	inline void set_btn_egyptSoundInactive_7(GameObject_t1113636619 * value)
	{
		___btn_egyptSoundInactive_7 = value;
		Il2CppCodeGenWriteBarrier((&___btn_egyptSoundInactive_7), value);
	}

	inline static int32_t get_offset_of_btn_difficulty_8() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_difficulty_8)); }
	inline GameObject_t1113636619 * get_btn_difficulty_8() const { return ___btn_difficulty_8; }
	inline GameObject_t1113636619 ** get_address_of_btn_difficulty_8() { return &___btn_difficulty_8; }
	inline void set_btn_difficulty_8(GameObject_t1113636619 * value)
	{
		___btn_difficulty_8 = value;
		Il2CppCodeGenWriteBarrier((&___btn_difficulty_8), value);
	}

	inline static int32_t get_offset_of_ckm_mainTheme_9() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_mainTheme_9)); }
	inline GameObject_t1113636619 * get_ckm_mainTheme_9() const { return ___ckm_mainTheme_9; }
	inline GameObject_t1113636619 ** get_address_of_ckm_mainTheme_9() { return &___ckm_mainTheme_9; }
	inline void set_ckm_mainTheme_9(GameObject_t1113636619 * value)
	{
		___ckm_mainTheme_9 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_mainTheme_9), value);
	}

	inline static int32_t get_offset_of_ckm_nightTheme_10() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_nightTheme_10)); }
	inline GameObject_t1113636619 * get_ckm_nightTheme_10() const { return ___ckm_nightTheme_10; }
	inline GameObject_t1113636619 ** get_address_of_ckm_nightTheme_10() { return &___ckm_nightTheme_10; }
	inline void set_ckm_nightTheme_10(GameObject_t1113636619 * value)
	{
		___ckm_nightTheme_10 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_nightTheme_10), value);
	}

	inline static int32_t get_offset_of_ckm_flappyTheme_11() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_flappyTheme_11)); }
	inline GameObject_t1113636619 * get_ckm_flappyTheme_11() const { return ___ckm_flappyTheme_11; }
	inline GameObject_t1113636619 ** get_address_of_ckm_flappyTheme_11() { return &___ckm_flappyTheme_11; }
	inline void set_ckm_flappyTheme_11(GameObject_t1113636619 * value)
	{
		___ckm_flappyTheme_11 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_flappyTheme_11), value);
	}

	inline static int32_t get_offset_of_ckm_desertTheme_12() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_desertTheme_12)); }
	inline GameObject_t1113636619 * get_ckm_desertTheme_12() const { return ___ckm_desertTheme_12; }
	inline GameObject_t1113636619 ** get_address_of_ckm_desertTheme_12() { return &___ckm_desertTheme_12; }
	inline void set_ckm_desertTheme_12(GameObject_t1113636619 * value)
	{
		___ckm_desertTheme_12 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_desertTheme_12), value);
	}

	inline static int32_t get_offset_of_ckm_noSound_13() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_noSound_13)); }
	inline GameObject_t1113636619 * get_ckm_noSound_13() const { return ___ckm_noSound_13; }
	inline GameObject_t1113636619 ** get_address_of_ckm_noSound_13() { return &___ckm_noSound_13; }
	inline void set_ckm_noSound_13(GameObject_t1113636619 * value)
	{
		___ckm_noSound_13 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_noSound_13), value);
	}

	inline static int32_t get_offset_of_ckm_mainSound_14() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_mainSound_14)); }
	inline GameObject_t1113636619 * get_ckm_mainSound_14() const { return ___ckm_mainSound_14; }
	inline GameObject_t1113636619 ** get_address_of_ckm_mainSound_14() { return &___ckm_mainSound_14; }
	inline void set_ckm_mainSound_14(GameObject_t1113636619 * value)
	{
		___ckm_mainSound_14 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_mainSound_14), value);
	}

	inline static int32_t get_offset_of_ckm_skyflightSound_15() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_skyflightSound_15)); }
	inline GameObject_t1113636619 * get_ckm_skyflightSound_15() const { return ___ckm_skyflightSound_15; }
	inline GameObject_t1113636619 ** get_address_of_ckm_skyflightSound_15() { return &___ckm_skyflightSound_15; }
	inline void set_ckm_skyflightSound_15(GameObject_t1113636619 * value)
	{
		___ckm_skyflightSound_15 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_skyflightSound_15), value);
	}

	inline static int32_t get_offset_of_ckm_dankstormSound_16() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_dankstormSound_16)); }
	inline GameObject_t1113636619 * get_ckm_dankstormSound_16() const { return ___ckm_dankstormSound_16; }
	inline GameObject_t1113636619 ** get_address_of_ckm_dankstormSound_16() { return &___ckm_dankstormSound_16; }
	inline void set_ckm_dankstormSound_16(GameObject_t1113636619 * value)
	{
		___ckm_dankstormSound_16 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_dankstormSound_16), value);
	}

	inline static int32_t get_offset_of_ckm_egyptSound_17() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_egyptSound_17)); }
	inline GameObject_t1113636619 * get_ckm_egyptSound_17() const { return ___ckm_egyptSound_17; }
	inline GameObject_t1113636619 ** get_address_of_ckm_egyptSound_17() { return &___ckm_egyptSound_17; }
	inline void set_ckm_egyptSound_17(GameObject_t1113636619 * value)
	{
		___ckm_egyptSound_17 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_egyptSound_17), value);
	}
};

struct InitButtonsScript_t1987868624_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> InitButtonsScript::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_18() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624_StaticFields, ___U3CU3Ef__switchU24map0_18)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_18() const { return ___U3CU3Ef__switchU24map0_18; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_18() { return &___U3CU3Ef__switchU24map0_18; }
	inline void set_U3CU3Ef__switchU24map0_18(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITBUTTONSSCRIPT_T1987868624_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (LoseScript_t212724369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[9] = 
{
	LoseScript_t212724369::get_offset_of_Game_2(),
	LoseScript_t212724369::get_offset_of_Menu_3(),
	LoseScript_t212724369::get_offset_of_Cubes_4(),
	LoseScript_t212724369::get_offset_of_tries_5(),
	LoseScript_t212724369::get_offset_of_difficulty_6(),
	LoseScript_t212724369::get_offset_of_audioSrc_7(),
	LoseScript_t212724369::get_offset_of_triesTillAds_8(),
	LoseScript_t212724369::get_offset_of_savedTimeScale_9(),
	LoseScript_t212724369::get_offset_of_anim_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (U3CDelayGameU3Ec__Iterator0_t4235577792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[5] = 
{
	U3CDelayGameU3Ec__Iterator0_t4235577792::get_offset_of_U3CtimeU3E__0_0(),
	U3CDelayGameU3Ec__Iterator0_t4235577792::get_offset_of_U24this_1(),
	U3CDelayGameU3Ec__Iterator0_t4235577792::get_offset_of_U24current_2(),
	U3CDelayGameU3Ec__Iterator0_t4235577792::get_offset_of_U24disposing_3(),
	U3CDelayGameU3Ec__Iterator0_t4235577792::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[5] = 
{
	U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618::get_offset_of_U3CoptionsU3E__0_0(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618::get_offset_of_U24this_1(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618::get_offset_of_U24current_2(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618::get_offset_of_U24disposing_3(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (U3CHandleAnimationU3Ec__Iterator2_t2293152264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[3] = 
{
	U3CHandleAnimationU3Ec__Iterator2_t2293152264::get_offset_of_U24current_0(),
	U3CHandleAnimationU3Ec__Iterator2_t2293152264::get_offset_of_U24disposing_1(),
	U3CHandleAnimationU3Ec__Iterator2_t2293152264::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (RandomSpawnScript_t4065494415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[11] = 
{
	RandomSpawnScript_t4065494415::get_offset_of_prefab1_2(),
	RandomSpawnScript_t4065494415::get_offset_of_prefab2_3(),
	RandomSpawnScript_t4065494415::get_offset_of_prefab3_4(),
	RandomSpawnScript_t4065494415::get_offset_of_prefab4_5(),
	RandomSpawnScript_t4065494415::get_offset_of_spawnRate_6(),
	RandomSpawnScript_t4065494415::get_offset_of_spawnRateHard_7(),
	RandomSpawnScript_t4065494415::get_offset_of_nextSpawn_8(),
	RandomSpawnScript_t4065494415::get_offset_of_whatToSpawn_9(),
	RandomSpawnScript_t4065494415::get_offset_of_theme_10(),
	RandomSpawnScript_t4065494415::get_offset_of_obstacles_11(),
	RandomSpawnScript_t4065494415::get_offset_of_v3_position_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (TrippleTriggerLeftScript_t4183068999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (TrippleTriggerMiddleScript_t4217752517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[8] = 
{
	TrippleTriggerMiddleScript_t4217752517::get_offset_of_Game_2(),
	TrippleTriggerMiddleScript_t4217752517::get_offset_of_Menu_3(),
	TrippleTriggerMiddleScript_t4217752517::get_offset_of_audioSrc_4(),
	TrippleTriggerMiddleScript_t4217752517::get_offset_of_triesTillAds_5(),
	TrippleTriggerMiddleScript_t4217752517::get_offset_of_btn_play_6(),
	TrippleTriggerMiddleScript_t4217752517::get_offset_of_text_loading_7(),
	TrippleTriggerMiddleScript_t4217752517::get_offset_of_anim_8(),
	TrippleTriggerMiddleScript_t4217752517::get_offset_of_savedTimeScale_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (U3CDelayGameU3Ec__Iterator0_t4237126612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[5] = 
{
	U3CDelayGameU3Ec__Iterator0_t4237126612::get_offset_of_U3CtimeU3E__0_0(),
	U3CDelayGameU3Ec__Iterator0_t4237126612::get_offset_of_U24this_1(),
	U3CDelayGameU3Ec__Iterator0_t4237126612::get_offset_of_U24current_2(),
	U3CDelayGameU3Ec__Iterator0_t4237126612::get_offset_of_U24disposing_3(),
	U3CDelayGameU3Ec__Iterator0_t4237126612::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[5] = 
{
	U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877::get_offset_of_U3CoptionsU3E__0_0(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877::get_offset_of_U24this_1(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877::get_offset_of_U24current_2(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877::get_offset_of_U24disposing_3(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (U3CHandleAnimationU3Ec__Iterator2_t2078686267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[4] = 
{
	U3CHandleAnimationU3Ec__Iterator2_t2078686267::get_offset_of_U24this_0(),
	U3CHandleAnimationU3Ec__Iterator2_t2078686267::get_offset_of_U24current_1(),
	U3CHandleAnimationU3Ec__Iterator2_t2078686267::get_offset_of_U24disposing_2(),
	U3CHandleAnimationU3Ec__Iterator2_t2078686267::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (TrippleTriggerRightScript_t853323302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (ButtonControllerScript_t2432915718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[66] = 
{
	ButtonControllerScript_t2432915718::get_offset_of_audioSource_2(),
	ButtonControllerScript_t2432915718::get_offset_of_clip_mainSound_3(),
	ButtonControllerScript_t2432915718::get_offset_of_clip_dankstormSound_4(),
	ButtonControllerScript_t2432915718::get_offset_of_clip_skyflightSound_5(),
	ButtonControllerScript_t2432915718::get_offset_of_clip_egyptSound_6(),
	ButtonControllerScript_t2432915718::get_offset_of_backgroundSprite_7(),
	ButtonControllerScript_t2432915718::get_offset_of_mainTheme_8(),
	ButtonControllerScript_t2432915718::get_offset_of_flappyTheme_9(),
	ButtonControllerScript_t2432915718::get_offset_of_nightTheme_10(),
	ButtonControllerScript_t2432915718::get_offset_of_desertTheme_11(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_play_12(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_video_13(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_setting_14(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_reset_15(),
	ButtonControllerScript_t2432915718::get_offset_of_txt_reset_16(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_answers_17(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_noSound_18(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_mainSound_19(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_skyflightSound_20(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_dankstormSound_21(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_egyptSound_22(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_back_23(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_buy_24(),
	ButtonControllerScript_t2432915718::get_offset_of_nightThemeCost_25(),
	ButtonControllerScript_t2432915718::get_offset_of_flappyThemeCost_26(),
	ButtonControllerScript_t2432915718::get_offset_of_desertThemeCost_27(),
	ButtonControllerScript_t2432915718::get_offset_of_skyflightSoundCost_28(),
	ButtonControllerScript_t2432915718::get_offset_of_dankstormSoundCost_29(),
	ButtonControllerScript_t2432915718::get_offset_of_egyptSoundCost_30(),
	ButtonControllerScript_t2432915718::get_offset_of_cnv_menu_31(),
	ButtonControllerScript_t2432915718::get_offset_of_cnv_settings_32(),
	ButtonControllerScript_t2432915718::get_offset_of_cnv_game_33(),
	ButtonControllerScript_t2432915718::get_offset_of_go_permanent_34(),
	ButtonControllerScript_t2432915718::get_offset_of_text_buy_35(),
	ButtonControllerScript_t2432915718::get_offset_of_txt_cost_36(),
	ButtonControllerScript_t2432915718::get_offset_of_scoreText_37(),
	ButtonControllerScript_t2432915718::get_offset_of_specialPointsText_38(),
	ButtonControllerScript_t2432915718::get_offset_of_anim_39(),
	ButtonControllerScript_t2432915718::get_offset_of_triesTillAds_40(),
	ButtonControllerScript_t2432915718::get_offset_of_Cube_plusOne_41(),
	ButtonControllerScript_t2432915718::get_offset_of_specialPoints_42(),
	ButtonControllerScript_t2432915718::get_offset_of_singleObstacle_43(),
	ButtonControllerScript_t2432915718::get_offset_of_doubleObstacle_44(),
	ButtonControllerScript_t2432915718::get_offset_of_trippleObstacle_45(),
	ButtonControllerScript_t2432915718::get_offset_of_quattroObstacle_46(),
	ButtonControllerScript_t2432915718::get_offset_of_previewTime_47(),
	ButtonControllerScript_t2432915718::get_offset_of_btn_difficulty_48(),
	ButtonControllerScript_t2432915718::get_offset_of_difficulty_49(),
	ButtonControllerScript_t2432915718::get_offset_of_textHighscoreCount_50(),
	ButtonControllerScript_t2432915718::get_offset_of_highscore_51(),
	ButtonControllerScript_t2432915718::get_offset_of_textPlayedCount_52(),
	ButtonControllerScript_t2432915718::get_offset_of_tries_53(),
	ButtonControllerScript_t2432915718::get_offset_of_textLastPoints_54(),
	ButtonControllerScript_t2432915718::get_offset_of_lastPoints_55(),
	ButtonControllerScript_t2432915718::get_offset_of_ckm_mainTheme_56(),
	ButtonControllerScript_t2432915718::get_offset_of_ckm_nightTheme_57(),
	ButtonControllerScript_t2432915718::get_offset_of_ckm_flappyTheme_58(),
	ButtonControllerScript_t2432915718::get_offset_of_ckm_desertTheme_59(),
	ButtonControllerScript_t2432915718::get_offset_of_ckm_noSound_60(),
	ButtonControllerScript_t2432915718::get_offset_of_ckm_mainSound_61(),
	ButtonControllerScript_t2432915718::get_offset_of_ckm_skyflightSound_62(),
	ButtonControllerScript_t2432915718::get_offset_of_ckm_dankstormSound_63(),
	ButtonControllerScript_t2432915718::get_offset_of_ckm_egyptSound_64(),
	ButtonControllerScript_t2432915718::get_offset_of_texts_65(),
	ButtonControllerScript_t2432915718::get_offset_of_svSounds_66(),
	ButtonControllerScript_t2432915718::get_offset_of_svThemes_67(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (U3CDelayGameStartU3Ec__Iterator0_t3856269498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[4] = 
{
	U3CDelayGameStartU3Ec__Iterator0_t3856269498::get_offset_of_U24this_0(),
	U3CDelayGameStartU3Ec__Iterator0_t3856269498::get_offset_of_U24current_1(),
	U3CDelayGameStartU3Ec__Iterator0_t3856269498::get_offset_of_U24disposing_2(),
	U3CDelayGameStartU3Ec__Iterator0_t3856269498::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[5] = 
{
	U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249::get_offset_of_U3CoptionsU3E__0_0(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249::get_offset_of_U24this_1(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249::get_offset_of_U24current_2(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249::get_offset_of_U24disposing_3(),
	U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (U3CHandleAnimationU3Ec__Iterator2_t1056433757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2014[4] = 
{
	U3CHandleAnimationU3Ec__Iterator2_t1056433757::get_offset_of_U24this_0(),
	U3CHandleAnimationU3Ec__Iterator2_t1056433757::get_offset_of_U24current_1(),
	U3CHandleAnimationU3Ec__Iterator2_t1056433757::get_offset_of_U24disposing_2(),
	U3CHandleAnimationU3Ec__Iterator2_t1056433757::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (InitTextsScript_t1053055510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[27] = 
{
	InitTextsScript_t1053055510::get_offset_of_cnv_menu_2(),
	InitTextsScript_t1053055510::get_offset_of_cnv_settings_3(),
	InitTextsScript_t1053055510::get_offset_of_cnv_game_4(),
	InitTextsScript_t1053055510::get_offset_of_texts_5(),
	InitTextsScript_t1053055510::get_offset_of_textHighscoreCount_6(),
	InitTextsScript_t1053055510::get_offset_of_highscore_7(),
	InitTextsScript_t1053055510::get_offset_of_textPlayedCount_8(),
	InitTextsScript_t1053055510::get_offset_of_tries_9(),
	InitTextsScript_t1053055510::get_offset_of_textLastPoints_10(),
	InitTextsScript_t1053055510::get_offset_of_lastPoints_11(),
	InitTextsScript_t1053055510::get_offset_of_scoreText_12(),
	InitTextsScript_t1053055510::get_offset_of_specialPointsText_13(),
	InitTextsScript_t1053055510::get_offset_of_backgroundSpriteMenu_14(),
	InitTextsScript_t1053055510::get_offset_of_backgroundSpriteSettings_15(),
	InitTextsScript_t1053055510::get_offset_of_backgroundSpriteGame_16(),
	InitTextsScript_t1053055510::get_offset_of_mainTheme_17(),
	InitTextsScript_t1053055510::get_offset_of_flappyTheme_18(),
	InitTextsScript_t1053055510::get_offset_of_nightTheme_19(),
	InitTextsScript_t1053055510::get_offset_of_desertTheme_20(),
	InitTextsScript_t1053055510::get_offset_of_bgSprite_21(),
	InitTextsScript_t1053055510::get_offset_of_audioSource_22(),
	InitTextsScript_t1053055510::get_offset_of_clip_mainSound_23(),
	InitTextsScript_t1053055510::get_offset_of_clip_dankstormSound_24(),
	InitTextsScript_t1053055510::get_offset_of_clip_skyflightSound_25(),
	InitTextsScript_t1053055510::get_offset_of_clip_egyptSound_26(),
	InitTextsScript_t1053055510::get_offset_of_previewTime_27(),
	InitTextsScript_t1053055510::get_offset_of_difficulty_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (InitButtonsScript_t1987868624), -1, sizeof(InitButtonsScript_t1987868624_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2016[17] = 
{
	InitButtonsScript_t1987868624::get_offset_of_btn_nightThemeInactive_2(),
	InitButtonsScript_t1987868624::get_offset_of_btn_flappyThemeInactive_3(),
	InitButtonsScript_t1987868624::get_offset_of_btn_desertThemeInactive_4(),
	InitButtonsScript_t1987868624::get_offset_of_btn_skyflightSoundInactive_5(),
	InitButtonsScript_t1987868624::get_offset_of_btn_dankstormSoundInactive_6(),
	InitButtonsScript_t1987868624::get_offset_of_btn_egyptSoundInactive_7(),
	InitButtonsScript_t1987868624::get_offset_of_btn_difficulty_8(),
	InitButtonsScript_t1987868624::get_offset_of_ckm_mainTheme_9(),
	InitButtonsScript_t1987868624::get_offset_of_ckm_nightTheme_10(),
	InitButtonsScript_t1987868624::get_offset_of_ckm_flappyTheme_11(),
	InitButtonsScript_t1987868624::get_offset_of_ckm_desertTheme_12(),
	InitButtonsScript_t1987868624::get_offset_of_ckm_noSound_13(),
	InitButtonsScript_t1987868624::get_offset_of_ckm_mainSound_14(),
	InitButtonsScript_t1987868624::get_offset_of_ckm_skyflightSound_15(),
	InitButtonsScript_t1987868624::get_offset_of_ckm_dankstormSound_16(),
	InitButtonsScript_t1987868624::get_offset_of_ckm_egyptSound_17(),
	InitButtonsScript_t1987868624_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_18(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
