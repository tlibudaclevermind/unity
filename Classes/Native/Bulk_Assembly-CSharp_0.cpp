﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// BallButtonControllerScript
struct BallButtonControllerScript_t1117378164;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.String
struct String_t;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Animator
struct Animator_t434523843;
// ButtonControllerScript
struct ButtonControllerScript_t2432915718;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// InitTextsScript
struct InitTextsScript_t1053055510;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t682124106;
// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct UnityEvent_1_t978947469;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// InitButtonsScript
struct InitButtonsScript_t1987868624;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// ButtonControllerScript/<DelayGameStart>c__Iterator0
struct U3CDelayGameStartU3Ec__Iterator0_t3856269498;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Sprite
struct Sprite_t280657092;
// ButtonControllerScript/<ShowAdWhenReady>c__Iterator1
struct U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249;
// ButtonControllerScript/<HandleAnimation>c__Iterator2
struct U3CHandleAnimationU3Ec__Iterator2_t1056433757;
// GravityScript
struct GravityScript_t2220762432;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t990845000;
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_t3243021218;
// DestroyerScript
struct DestroyerScript_t3910417206;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3384741;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UnityEngine.UI.Text
struct Text_t1901882714;
// LoseScript
struct LoseScript_t212724369;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// LoseScript/<DelayGame>c__Iterator0
struct U3CDelayGameU3Ec__Iterator0_t4235577792;
// LoseScript/<ShowAdWhenReady>c__Iterator1
struct U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618;
// LoseScript/<HandleAnimation>c__Iterator2
struct U3CHandleAnimationU3Ec__Iterator2_t2293152264;
// RandomSpawnScript
struct RandomSpawnScript_t4065494415;
// TrippleTriggerLeftScript
struct TrippleTriggerLeftScript_t4183068999;
// TrippleTriggerMiddleScript
struct TrippleTriggerMiddleScript_t4217752517;
// TrippleTriggerMiddleScript/<DelayGame>c__Iterator0
struct U3CDelayGameU3Ec__Iterator0_t4237126612;
// TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1
struct U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877;
// TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2
struct U3CHandleAnimationU3Ec__Iterator2_t2078686267;
// TrippleTriggerRightScript
struct TrippleTriggerRightScript_t853323302;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3530625384;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1677636661;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1059417452;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t343079324;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t123837990;
// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t1873685584;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;

extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisAnimator_t434523843_m2351447238_RuntimeMethod_var;
extern String_t* _stringLiteral3941174895;
extern String_t* _stringLiteral4002445229;
extern String_t* _stringLiteral6892883;
extern String_t* _stringLiteral1235497039;
extern String_t* _stringLiteral4079641953;
extern String_t* _stringLiteral1385165106;
extern String_t* _stringLiteral996785950;
extern String_t* _stringLiteral3459875600;
extern String_t* _stringLiteral2263706523;
extern String_t* _stringLiteral3842322295;
extern String_t* _stringLiteral1110670577;
extern const uint32_t BallButtonControllerScript_Update_m1072157986_MetadataUsageId;
extern String_t* _stringLiteral760301857;
extern String_t* _stringLiteral2473678957;
extern String_t* _stringLiteral2528740594;
extern String_t* _stringLiteral653748937;
extern String_t* _stringLiteral294264122;
extern const uint32_t ButtonControllerScript_Start_m3772180747_MetadataUsageId;
extern String_t* _stringLiteral3316871923;
extern String_t* _stringLiteral3896413798;
extern String_t* _stringLiteral2448431576;
extern const uint32_t ButtonControllerScript_OnToggleValueChanged_m884945251_MetadataUsageId;
extern String_t* _stringLiteral1785964129;
extern String_t* _stringLiteral3075782012;
extern String_t* _stringLiteral1114791609;
extern const uint32_t ButtonControllerScript_checkSelectedSound_m3649248901_MetadataUsageId;
extern String_t* _stringLiteral3359376429;
extern String_t* _stringLiteral404843312;
extern String_t* _stringLiteral3790055497;
extern String_t* _stringLiteral186867486;
extern const uint32_t ButtonControllerScript_checkSelectedTheme_m1504630267_MetadataUsageId;
extern RuntimeClass* UnityAction_1_t682124106_il2cpp_TypeInfo_var;
extern RuntimeClass* Advertisement_t842671397_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisInitTextsScript_t1053055510_m1730174322_RuntimeMethod_var;
extern const RuntimeMethod* ButtonControllerScript_OnToggleValueChanged_m884945251_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_1__ctor_m3007623985_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_AddListener_m2847988282_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponentInChildren_TisInitButtonsScript_t1987868624_m2265097840_RuntimeMethod_var;
extern String_t* _stringLiteral167811139;
extern String_t* _stringLiteral870646944;
extern String_t* _stringLiteral2541657273;
extern String_t* _stringLiteral2722989498;
extern String_t* _stringLiteral388954149;
extern String_t* _stringLiteral3681683422;
extern String_t* _stringLiteral3046124294;
extern String_t* _stringLiteral3452614544;
extern String_t* _stringLiteral3831740680;
extern String_t* _stringLiteral2432068731;
extern String_t* _stringLiteral3014706548;
extern String_t* _stringLiteral4224754453;
extern String_t* _stringLiteral441598207;
extern String_t* _stringLiteral4058834769;
extern String_t* _stringLiteral405390407;
extern String_t* _stringLiteral4011269308;
extern String_t* _stringLiteral2777363045;
extern String_t* _stringLiteral3574414313;
extern String_t* _stringLiteral4273456759;
extern String_t* _stringLiteral816225280;
extern String_t* _stringLiteral3921657742;
extern String_t* _stringLiteral2699831051;
extern String_t* _stringLiteral1559865961;
extern String_t* _stringLiteral3775034288;
extern String_t* _stringLiteral681917307;
extern String_t* _stringLiteral166522051;
extern String_t* _stringLiteral1938886732;
extern String_t* _stringLiteral2074989104;
extern String_t* _stringLiteral2070893366;
extern String_t* _stringLiteral2785512304;
extern String_t* _stringLiteral747299879;
extern String_t* _stringLiteral2895553709;
extern String_t* _stringLiteral3654983690;
extern String_t* _stringLiteral3593808015;
extern String_t* _stringLiteral3163906174;
extern String_t* _stringLiteral3060255191;
extern String_t* _stringLiteral4130524952;
extern String_t* _stringLiteral2687637966;
extern String_t* _stringLiteral2272329368;
extern String_t* _stringLiteral1790728806;
extern String_t* _stringLiteral4005421059;
extern String_t* _stringLiteral2378241666;
extern String_t* _stringLiteral1467627447;
extern const uint32_t ButtonControllerScript_Update_m4012678045_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var;
extern const uint32_t ButtonControllerScript_FixedUpdate_m2503950570_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisInitButtonsScript_t1987868624_m198946856_RuntimeMethod_var;
extern const uint32_t ButtonControllerScript_resetThings_m3684079304_MetadataUsageId;
extern RuntimeClass* U3CDelayGameStartU3Ec__Iterator0_t3856269498_il2cpp_TypeInfo_var;
extern const uint32_t ButtonControllerScript_DelayGameStart_m2227959488_MetadataUsageId;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2753579780;
extern String_t* _stringLiteral3941174888;
extern String_t* _stringLiteral1301175002;
extern const uint32_t ButtonControllerScript_checkBuyButtonToShow_m3246718299_MetadataUsageId;
extern String_t* _stringLiteral1228557250;
extern String_t* _stringLiteral3875954633;
extern String_t* _stringLiteral773881782;
extern String_t* _stringLiteral2002597352;
extern const uint32_t ButtonControllerScript_performPurchase_m2064459992_MetadataUsageId;
extern String_t* _stringLiteral772767670;
extern String_t* _stringLiteral1999255016;
extern const uint32_t ButtonControllerScript_selectButton_m619056489_MetadataUsageId;
extern const uint32_t ButtonControllerScript_disableCheckmarks_m343318825_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const uint32_t ButtonControllerScript_selectSound_m808880357_MetadataUsageId;
extern String_t* _stringLiteral230658322;
extern const uint32_t ButtonControllerScript_selectTheme_m4172231223_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var;
extern const uint32_t ButtonControllerScript_changeTheme_m1190884257_MetadataUsageId;
extern RuntimeClass* U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249_il2cpp_TypeInfo_var;
extern const uint32_t ButtonControllerScript_ShowAdWhenReady_m2072552498_MetadataUsageId;
extern RuntimeClass* U3CHandleAnimationU3Ec__Iterator2_t1056433757_il2cpp_TypeInfo_var;
extern const uint32_t ButtonControllerScript_HandleAnimation_m1917644310_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisGravityScript_t2220762432_m2338767488_RuntimeMethod_var;
extern const uint32_t ButtonControllerScript_resetGravity_m3471744663_MetadataUsageId;
extern const uint32_t ButtonControllerScript_playPreviewSound_m1032544213_MetadataUsageId;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern const uint32_t U3CDelayGameStartU3Ec__Iterator0_MoveNext_m649956165_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const uint32_t U3CDelayGameStartU3Ec__Iterator0_Reset_m3146499568_MetadataUsageId;
extern String_t* _stringLiteral1578302179;
extern const uint32_t U3CHandleAnimationU3Ec__Iterator2_MoveNext_m3372310698_MetadataUsageId;
extern const uint32_t U3CHandleAnimationU3Ec__Iterator2_Reset_m213181928_MetadataUsageId;
extern RuntimeClass* ShowOptions_t990845000_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t3243021218_il2cpp_TypeInfo_var;
extern const RuntimeMethod* ButtonControllerScript_HandleShowResult_m200410161_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m3797332079_RuntimeMethod_var;
extern const uint32_t U3CShowAdWhenReadyU3Ec__Iterator1_MoveNext_m3890286113_MetadataUsageId;
extern const uint32_t U3CShowAdWhenReadyU3Ec__Iterator1_Reset_m2101412034_MetadataUsageId;
extern String_t* _stringLiteral823026589;
extern String_t* _stringLiteral2929758584;
extern String_t* _stringLiteral3452654525;
extern const uint32_t DestroyerScript_OnTriggerEnter_m1364252301_MetadataUsageId;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3556801464;
extern const uint32_t GravityScript_FixedUpdate_m3746438327_MetadataUsageId;
extern RuntimeClass* InitButtonsScript_t1987868624_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2736202052_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2392909825_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m282647386_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1013208020_RuntimeMethod_var;
extern const uint32_t InitButtonsScript_InitButtons_m1101143926_MetadataUsageId;
extern const uint32_t InitTextsScript_Start_m1608449831_MetadataUsageId;
extern const uint32_t InitTextsScript_UpdateTexts_m2560498976_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponentsInChildren_TisText_t1901882714_m1445556921_RuntimeMethod_var;
extern const uint32_t InitTextsScript_FixedUpdate_m1485515752_MetadataUsageId;
extern String_t* _stringLiteral3528114727;
extern const uint32_t LoseScript_OnTriggerEnter_m3921108129_MetadataUsageId;
extern RuntimeClass* U3CDelayGameU3Ec__Iterator0_t4235577792_il2cpp_TypeInfo_var;
extern const uint32_t LoseScript_DelayGame_m1373927896_MetadataUsageId;
extern RuntimeClass* U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618_il2cpp_TypeInfo_var;
extern const uint32_t LoseScript_ShowAdWhenReady_m2535654720_MetadataUsageId;
extern RuntimeClass* U3CHandleAnimationU3Ec__Iterator2_t2293152264_il2cpp_TypeInfo_var;
extern const uint32_t LoseScript_HandleAnimation_m1724406564_MetadataUsageId;
extern const uint32_t U3CDelayGameU3Ec__Iterator0_Reset_m1581945427_MetadataUsageId;
extern const uint32_t U3CHandleAnimationU3Ec__Iterator2_MoveNext_m2097147944_MetadataUsageId;
extern const uint32_t U3CHandleAnimationU3Ec__Iterator2_Reset_m2514165367_MetadataUsageId;
extern const RuntimeMethod* LoseScript_HandleShowResult_m3154952640_RuntimeMethod_var;
extern const uint32_t U3CShowAdWhenReadyU3Ec__Iterator1_MoveNext_m2413234193_MetadataUsageId;
extern const uint32_t U3CShowAdWhenReadyU3Ec__Iterator1_Reset_m2750784832_MetadataUsageId;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var;
extern const uint32_t RandomSpawnScript_FixedUpdate_m293722346_MetadataUsageId;
extern String_t* _stringLiteral2261822918;
extern String_t* _stringLiteral237192571;
extern const uint32_t TrippleTriggerLeftScript_OnTriggerEnter_m261571855_MetadataUsageId;
extern String_t* _stringLiteral2238759663;
extern const uint32_t TrippleTriggerMiddleScript_OnTriggerEnter_m3243190582_MetadataUsageId;
extern RuntimeClass* U3CDelayGameU3Ec__Iterator0_t4237126612_il2cpp_TypeInfo_var;
extern const uint32_t TrippleTriggerMiddleScript_DelayGame_m2357289153_MetadataUsageId;
extern RuntimeClass* U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877_il2cpp_TypeInfo_var;
extern const uint32_t TrippleTriggerMiddleScript_ShowAdWhenReady_m3650831147_MetadataUsageId;
extern RuntimeClass* U3CHandleAnimationU3Ec__Iterator2_t2078686267_il2cpp_TypeInfo_var;
extern const uint32_t TrippleTriggerMiddleScript_HandleAnimation_m2765064618_MetadataUsageId;
extern const uint32_t U3CDelayGameU3Ec__Iterator0_Reset_m2029078786_MetadataUsageId;
extern const uint32_t U3CHandleAnimationU3Ec__Iterator2_MoveNext_m10569484_MetadataUsageId;
extern const uint32_t U3CHandleAnimationU3Ec__Iterator2_Reset_m824429254_MetadataUsageId;
extern const RuntimeMethod* TrippleTriggerMiddleScript_HandleShowResult_m438357543_RuntimeMethod_var;
extern const uint32_t U3CShowAdWhenReadyU3Ec__Iterator1_MoveNext_m3353907810_MetadataUsageId;
extern const uint32_t U3CShowAdWhenReadyU3Ec__Iterator1_Reset_m3724331834_MetadataUsageId;
extern const uint32_t TrippleTriggerRightScript_OnTriggerEnter_m4089145672_MetadataUsageId;

struct TextU5BU5D_t422084607;
struct GameObjectU5BU5D_t3328599146;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef U3CHANDLEANIMATIONU3EC__ITERATOR2_T2293152264_H
#define U3CHANDLEANIMATIONU3EC__ITERATOR2_T2293152264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseScript/<HandleAnimation>c__Iterator2
struct  U3CHandleAnimationU3Ec__Iterator2_t2293152264  : public RuntimeObject
{
public:
	// System.Object LoseScript/<HandleAnimation>c__Iterator2::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean LoseScript/<HandleAnimation>c__Iterator2::$disposing
	bool ___U24disposing_1;
	// System.Int32 LoseScript/<HandleAnimation>c__Iterator2::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2293152264, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2293152264, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2293152264, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEANIMATIONU3EC__ITERATOR2_T2293152264_H
#ifndef U3CSHOWADWHENREADYU3EC__ITERATOR1_T4153835618_H
#define U3CSHOWADWHENREADYU3EC__ITERATOR1_T4153835618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseScript/<ShowAdWhenReady>c__Iterator1
struct  U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowOptions LoseScript/<ShowAdWhenReady>c__Iterator1::<options>__0
	ShowOptions_t990845000 * ___U3CoptionsU3E__0_0;
	// LoseScript LoseScript/<ShowAdWhenReady>c__Iterator1::$this
	LoseScript_t212724369 * ___U24this_1;
	// System.Object LoseScript/<ShowAdWhenReady>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean LoseScript/<ShowAdWhenReady>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 LoseScript/<ShowAdWhenReady>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoptionsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618, ___U3CoptionsU3E__0_0)); }
	inline ShowOptions_t990845000 * get_U3CoptionsU3E__0_0() const { return ___U3CoptionsU3E__0_0; }
	inline ShowOptions_t990845000 ** get_address_of_U3CoptionsU3E__0_0() { return &___U3CoptionsU3E__0_0; }
	inline void set_U3CoptionsU3E__0_0(ShowOptions_t990845000 * value)
	{
		___U3CoptionsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoptionsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618, ___U24this_1)); }
	inline LoseScript_t212724369 * get_U24this_1() const { return ___U24this_1; }
	inline LoseScript_t212724369 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LoseScript_t212724369 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWADWHENREADYU3EC__ITERATOR1_T4153835618_H
#ifndef U3CDELAYGAMEU3EC__ITERATOR0_T4237126612_H
#define U3CDELAYGAMEU3EC__ITERATOR0_T4237126612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerMiddleScript/<DelayGame>c__Iterator0
struct  U3CDelayGameU3Ec__Iterator0_t4237126612  : public RuntimeObject
{
public:
	// System.Single TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_0;
	// TrippleTriggerMiddleScript TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::$this
	TrippleTriggerMiddleScript_t4217752517 * ___U24this_1;
	// System.Object TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4237126612, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4237126612, ___U24this_1)); }
	inline TrippleTriggerMiddleScript_t4217752517 * get_U24this_1() const { return ___U24this_1; }
	inline TrippleTriggerMiddleScript_t4217752517 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TrippleTriggerMiddleScript_t4217752517 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4237126612, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4237126612, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4237126612, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYGAMEU3EC__ITERATOR0_T4237126612_H
#ifndef U3CHANDLEANIMATIONU3EC__ITERATOR2_T2078686267_H
#define U3CHANDLEANIMATIONU3EC__ITERATOR2_T2078686267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2
struct  U3CHandleAnimationU3Ec__Iterator2_t2078686267  : public RuntimeObject
{
public:
	// TrippleTriggerMiddleScript TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::$this
	TrippleTriggerMiddleScript_t4217752517 * ___U24this_0;
	// System.Object TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2078686267, ___U24this_0)); }
	inline TrippleTriggerMiddleScript_t4217752517 * get_U24this_0() const { return ___U24this_0; }
	inline TrippleTriggerMiddleScript_t4217752517 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TrippleTriggerMiddleScript_t4217752517 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2078686267, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2078686267, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t2078686267, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEANIMATIONU3EC__ITERATOR2_T2078686267_H
#ifndef U3CSHOWADWHENREADYU3EC__ITERATOR1_T1651327877_H
#define U3CSHOWADWHENREADYU3EC__ITERATOR1_T1651327877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1
struct  U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowOptions TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::<options>__0
	ShowOptions_t990845000 * ___U3CoptionsU3E__0_0;
	// TrippleTriggerMiddleScript TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::$this
	TrippleTriggerMiddleScript_t4217752517 * ___U24this_1;
	// System.Object TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoptionsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877, ___U3CoptionsU3E__0_0)); }
	inline ShowOptions_t990845000 * get_U3CoptionsU3E__0_0() const { return ___U3CoptionsU3E__0_0; }
	inline ShowOptions_t990845000 ** get_address_of_U3CoptionsU3E__0_0() { return &___U3CoptionsU3E__0_0; }
	inline void set_U3CoptionsU3E__0_0(ShowOptions_t990845000 * value)
	{
		___U3CoptionsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoptionsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877, ___U24this_1)); }
	inline TrippleTriggerMiddleScript_t4217752517 * get_U24this_1() const { return ___U24this_1; }
	inline TrippleTriggerMiddleScript_t4217752517 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TrippleTriggerMiddleScript_t4217752517 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWADWHENREADYU3EC__ITERATOR1_T1651327877_H
#ifndef U3CDELAYGAMESTARTU3EC__ITERATOR0_T3856269498_H
#define U3CDELAYGAMESTARTU3EC__ITERATOR0_T3856269498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonControllerScript/<DelayGameStart>c__Iterator0
struct  U3CDelayGameStartU3Ec__Iterator0_t3856269498  : public RuntimeObject
{
public:
	// ButtonControllerScript ButtonControllerScript/<DelayGameStart>c__Iterator0::$this
	ButtonControllerScript_t2432915718 * ___U24this_0;
	// System.Object ButtonControllerScript/<DelayGameStart>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ButtonControllerScript/<DelayGameStart>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ButtonControllerScript/<DelayGameStart>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDelayGameStartU3Ec__Iterator0_t3856269498, ___U24this_0)); }
	inline ButtonControllerScript_t2432915718 * get_U24this_0() const { return ___U24this_0; }
	inline ButtonControllerScript_t2432915718 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ButtonControllerScript_t2432915718 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayGameStartU3Ec__Iterator0_t3856269498, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayGameStartU3Ec__Iterator0_t3856269498, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayGameStartU3Ec__Iterator0_t3856269498, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYGAMESTARTU3EC__ITERATOR0_T3856269498_H
#ifndef U3CHANDLEANIMATIONU3EC__ITERATOR2_T1056433757_H
#define U3CHANDLEANIMATIONU3EC__ITERATOR2_T1056433757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonControllerScript/<HandleAnimation>c__Iterator2
struct  U3CHandleAnimationU3Ec__Iterator2_t1056433757  : public RuntimeObject
{
public:
	// ButtonControllerScript ButtonControllerScript/<HandleAnimation>c__Iterator2::$this
	ButtonControllerScript_t2432915718 * ___U24this_0;
	// System.Object ButtonControllerScript/<HandleAnimation>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ButtonControllerScript/<HandleAnimation>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 ButtonControllerScript/<HandleAnimation>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t1056433757, ___U24this_0)); }
	inline ButtonControllerScript_t2432915718 * get_U24this_0() const { return ___U24this_0; }
	inline ButtonControllerScript_t2432915718 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ButtonControllerScript_t2432915718 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t1056433757, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t1056433757, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CHandleAnimationU3Ec__Iterator2_t1056433757, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEANIMATIONU3EC__ITERATOR2_T1056433757_H
#ifndef U3CSHOWADWHENREADYU3EC__ITERATOR1_T1035390249_H
#define U3CSHOWADWHENREADYU3EC__ITERATOR1_T1035390249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonControllerScript/<ShowAdWhenReady>c__Iterator1
struct  U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowOptions ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::<options>__0
	ShowOptions_t990845000 * ___U3CoptionsU3E__0_0;
	// ButtonControllerScript ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::$this
	ButtonControllerScript_t2432915718 * ___U24this_1;
	// System.Object ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoptionsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249, ___U3CoptionsU3E__0_0)); }
	inline ShowOptions_t990845000 * get_U3CoptionsU3E__0_0() const { return ___U3CoptionsU3E__0_0; }
	inline ShowOptions_t990845000 ** get_address_of_U3CoptionsU3E__0_0() { return &___U3CoptionsU3E__0_0; }
	inline void set_U3CoptionsU3E__0_0(ShowOptions_t990845000 * value)
	{
		___U3CoptionsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoptionsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249, ___U24this_1)); }
	inline ButtonControllerScript_t2432915718 * get_U24this_1() const { return ___U24this_1; }
	inline ButtonControllerScript_t2432915718 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ButtonControllerScript_t2432915718 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWADWHENREADYU3EC__ITERATOR1_T1035390249_H
#ifndef U3CDELAYGAMEU3EC__ITERATOR0_T4235577792_H
#define U3CDELAYGAMEU3EC__ITERATOR0_T4235577792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseScript/<DelayGame>c__Iterator0
struct  U3CDelayGameU3Ec__Iterator0_t4235577792  : public RuntimeObject
{
public:
	// System.Single LoseScript/<DelayGame>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_0;
	// LoseScript LoseScript/<DelayGame>c__Iterator0::$this
	LoseScript_t212724369 * ___U24this_1;
	// System.Object LoseScript/<DelayGame>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean LoseScript/<DelayGame>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 LoseScript/<DelayGame>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4235577792, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4235577792, ___U24this_1)); }
	inline LoseScript_t212724369 * get_U24this_1() const { return ___U24this_1; }
	inline LoseScript_t212724369 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LoseScript_t212724369 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4235577792, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4235577792, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayGameU3Ec__Iterator0_t4235577792, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYGAMEU3EC__ITERATOR0_T4235577792_H
#ifndef DICTIONARY_2_T2736202052_H
#define DICTIONARY_2_T2736202052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct  Dictionary_2_t2736202052  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	Int32U5BU5D_t385246372* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___valueSlots_7)); }
	inline Int32U5BU5D_t385246372* get_valueSlots_7() const { return ___valueSlots_7; }
	inline Int32U5BU5D_t385246372** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(Int32U5BU5D_t385246372* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2736202052_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3530625384 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3530625384 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3530625384 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3530625384 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2736202052_H
#ifndef SHOWOPTIONS_T990845000_H
#define SHOWOPTIONS_T990845000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ShowOptions
struct  ShowOptions_t990845000  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::<resultCallback>k__BackingField
	Action_1_t3243021218 * ___U3CresultCallbackU3Ek__BackingField_0;
	// System.String UnityEngine.Advertisements.ShowOptions::<gamerSid>k__BackingField
	String_t* ___U3CgamerSidU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CresultCallbackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ShowOptions_t990845000, ___U3CresultCallbackU3Ek__BackingField_0)); }
	inline Action_1_t3243021218 * get_U3CresultCallbackU3Ek__BackingField_0() const { return ___U3CresultCallbackU3Ek__BackingField_0; }
	inline Action_1_t3243021218 ** get_address_of_U3CresultCallbackU3Ek__BackingField_0() { return &___U3CresultCallbackU3Ek__BackingField_0; }
	inline void set_U3CresultCallbackU3Ek__BackingField_0(Action_1_t3243021218 * value)
	{
		___U3CresultCallbackU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultCallbackU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CgamerSidU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ShowOptions_t990845000, ___U3CgamerSidU3Ek__BackingField_1)); }
	inline String_t* get_U3CgamerSidU3Ek__BackingField_1() const { return ___U3CgamerSidU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CgamerSidU3Ek__BackingField_1() { return &___U3CgamerSidU3Ek__BackingField_1; }
	inline void set_U3CgamerSidU3Ek__BackingField_1(String_t* value)
	{
		___U3CgamerSidU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgamerSidU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWOPTIONS_T990845000_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef UNITYEVENT_1_T978947469_H
#define UNITYEVENT_1_T978947469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t978947469  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t978947469, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T978947469_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef MOVEMENTTYPE_T4072922106_H
#define MOVEMENTTYPE_T4072922106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/MovementType
struct  MovementType_t4072922106 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/MovementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementType_t4072922106, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T4072922106_H
#ifndef SCROLLBARVISIBILITY_T705693775_H
#define SCROLLBARVISIBILITY_T705693775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/ScrollbarVisibility
struct  ScrollbarVisibility_t705693775 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/ScrollbarVisibility::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t705693775, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBARVISIBILITY_T705693775_H
#ifndef TOGGLEEVENT_T1873685584_H
#define TOGGLEEVENT_T1873685584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle/ToggleEvent
struct  ToggleEvent_t1873685584  : public UnityEvent_1_t978947469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEEVENT_T1873685584_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef SHOWRESULT_T3070553623_H
#define SHOWRESULT_T3070553623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ShowResult
struct  ShowResult_t3070553623 
{
public:
	// System.Int32 UnityEngine.Advertisements.ShowResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShowResult_t3070553623, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWRESULT_T3070553623_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef TOGGLETRANSITION_T3587297765_H
#define TOGGLETRANSITION_T3587297765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle/ToggleTransition
struct  ToggleTransition_t3587297765 
{
public:
	// System.Int32 UnityEngine.UI.Toggle/ToggleTransition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ToggleTransition_t3587297765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLETRANSITION_T3587297765_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef AUDIOCLIP_T3680889665_H
#define AUDIOCLIP_T3680889665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip
struct  AudioClip_t3680889665  : public Object_t631007953
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t1677636661 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t1059417452 * ___m_PCMSetPositionCallback_3;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_2() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMReaderCallback_2)); }
	inline PCMReaderCallback_t1677636661 * get_m_PCMReaderCallback_2() const { return ___m_PCMReaderCallback_2; }
	inline PCMReaderCallback_t1677636661 ** get_address_of_m_PCMReaderCallback_2() { return &___m_PCMReaderCallback_2; }
	inline void set_m_PCMReaderCallback_2(PCMReaderCallback_t1677636661 * value)
	{
		___m_PCMReaderCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMReaderCallback_2), value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_3() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMSetPositionCallback_3)); }
	inline PCMSetPositionCallback_t1059417452 * get_m_PCMSetPositionCallback_3() const { return ___m_PCMSetPositionCallback_3; }
	inline PCMSetPositionCallback_t1059417452 ** get_address_of_m_PCMSetPositionCallback_3() { return &___m_PCMSetPositionCallback_3; }
	inline void set_m_PCMSetPositionCallback_3(PCMSetPositionCallback_t1059417452 * value)
	{
		___m_PCMSetPositionCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMSetPositionCallback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIP_T3680889665_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef COLLIDER_T1773347010_H
#define COLLIDER_T1773347010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1773347010  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1773347010_H
#ifndef ACTION_1_T3243021218_H
#define ACTION_1_T3243021218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct  Action_1_t3243021218  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3243021218_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef UNITYACTION_1_T682124106_H
#define UNITYACTION_1_T682124106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Boolean>
struct  UnityAction_1_t682124106  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T682124106_H
#ifndef ANIMATOR_T434523843_H
#define ANIMATOR_T434523843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t434523843  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T434523843_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public Behaviour_t1437897464
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_2;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_3;

public:
	inline static int32_t get_offset_of_spatializerExtension_2() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_2)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_2() const { return ___spatializerExtension_2; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_2() { return &___spatializerExtension_2; }
	inline void set_spatializerExtension_2(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_2 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_2), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_3() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_3)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_3() const { return ___ambisonicExtension_3; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_3() { return &___ambisonicExtension_3; }
	inline void set_ambisonicExtension_3(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_3 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef SPRITERENDERER_T3235626157_H
#define SPRITERENDERER_T3235626157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3235626157  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3235626157_H
#ifndef GRAVITYSCRIPT_T2220762432_H
#define GRAVITYSCRIPT_T2220762432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GravityScript
struct  GravityScript_t2220762432  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 GravityScript::gravity
	Vector3_t3722313464  ___gravity_2;
	// UnityEngine.Vector3 GravityScript::additional_gravity
	Vector3_t3722313464  ___additional_gravity_3;
	// System.Int32 GravityScript::actScore
	int32_t ___actScore_4;
	// System.Int32 GravityScript::speed
	int32_t ___speed_5;
	// System.Boolean GravityScript::addGravity
	bool ___addGravity_6;
	// System.Int32 GravityScript::moduloSpeed
	int32_t ___moduloSpeed_7;

public:
	inline static int32_t get_offset_of_gravity_2() { return static_cast<int32_t>(offsetof(GravityScript_t2220762432, ___gravity_2)); }
	inline Vector3_t3722313464  get_gravity_2() const { return ___gravity_2; }
	inline Vector3_t3722313464 * get_address_of_gravity_2() { return &___gravity_2; }
	inline void set_gravity_2(Vector3_t3722313464  value)
	{
		___gravity_2 = value;
	}

	inline static int32_t get_offset_of_additional_gravity_3() { return static_cast<int32_t>(offsetof(GravityScript_t2220762432, ___additional_gravity_3)); }
	inline Vector3_t3722313464  get_additional_gravity_3() const { return ___additional_gravity_3; }
	inline Vector3_t3722313464 * get_address_of_additional_gravity_3() { return &___additional_gravity_3; }
	inline void set_additional_gravity_3(Vector3_t3722313464  value)
	{
		___additional_gravity_3 = value;
	}

	inline static int32_t get_offset_of_actScore_4() { return static_cast<int32_t>(offsetof(GravityScript_t2220762432, ___actScore_4)); }
	inline int32_t get_actScore_4() const { return ___actScore_4; }
	inline int32_t* get_address_of_actScore_4() { return &___actScore_4; }
	inline void set_actScore_4(int32_t value)
	{
		___actScore_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(GravityScript_t2220762432, ___speed_5)); }
	inline int32_t get_speed_5() const { return ___speed_5; }
	inline int32_t* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(int32_t value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_addGravity_6() { return static_cast<int32_t>(offsetof(GravityScript_t2220762432, ___addGravity_6)); }
	inline bool get_addGravity_6() const { return ___addGravity_6; }
	inline bool* get_address_of_addGravity_6() { return &___addGravity_6; }
	inline void set_addGravity_6(bool value)
	{
		___addGravity_6 = value;
	}

	inline static int32_t get_offset_of_moduloSpeed_7() { return static_cast<int32_t>(offsetof(GravityScript_t2220762432, ___moduloSpeed_7)); }
	inline int32_t get_moduloSpeed_7() const { return ___moduloSpeed_7; }
	inline int32_t* get_address_of_moduloSpeed_7() { return &___moduloSpeed_7; }
	inline void set_moduloSpeed_7(int32_t value)
	{
		___moduloSpeed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAVITYSCRIPT_T2220762432_H
#ifndef TRIPPLETRIGGERLEFTSCRIPT_T4183068999_H
#define TRIPPLETRIGGERLEFTSCRIPT_T4183068999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerLeftScript
struct  TrippleTriggerLeftScript_t4183068999  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIPPLETRIGGERLEFTSCRIPT_T4183068999_H
#ifndef RANDOMSPAWNSCRIPT_T4065494415_H
#define RANDOMSPAWNSCRIPT_T4065494415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomSpawnScript
struct  RandomSpawnScript_t4065494415  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject RandomSpawnScript::prefab1
	GameObject_t1113636619 * ___prefab1_2;
	// UnityEngine.GameObject RandomSpawnScript::prefab2
	GameObject_t1113636619 * ___prefab2_3;
	// UnityEngine.GameObject RandomSpawnScript::prefab3
	GameObject_t1113636619 * ___prefab3_4;
	// UnityEngine.GameObject RandomSpawnScript::prefab4
	GameObject_t1113636619 * ___prefab4_5;
	// System.Single RandomSpawnScript::spawnRate
	float ___spawnRate_6;
	// System.Single RandomSpawnScript::spawnRateHard
	float ___spawnRateHard_7;
	// System.Single RandomSpawnScript::nextSpawn
	float ___nextSpawn_8;
	// System.Int32 RandomSpawnScript::whatToSpawn
	int32_t ___whatToSpawn_9;
	// System.String RandomSpawnScript::theme
	String_t* ___theme_10;
	// UnityEngine.GameObject[] RandomSpawnScript::obstacles
	GameObjectU5BU5D_t3328599146* ___obstacles_11;
	// UnityEngine.Vector3 RandomSpawnScript::v3_position
	Vector3_t3722313464  ___v3_position_12;

public:
	inline static int32_t get_offset_of_prefab1_2() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___prefab1_2)); }
	inline GameObject_t1113636619 * get_prefab1_2() const { return ___prefab1_2; }
	inline GameObject_t1113636619 ** get_address_of_prefab1_2() { return &___prefab1_2; }
	inline void set_prefab1_2(GameObject_t1113636619 * value)
	{
		___prefab1_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefab1_2), value);
	}

	inline static int32_t get_offset_of_prefab2_3() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___prefab2_3)); }
	inline GameObject_t1113636619 * get_prefab2_3() const { return ___prefab2_3; }
	inline GameObject_t1113636619 ** get_address_of_prefab2_3() { return &___prefab2_3; }
	inline void set_prefab2_3(GameObject_t1113636619 * value)
	{
		___prefab2_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefab2_3), value);
	}

	inline static int32_t get_offset_of_prefab3_4() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___prefab3_4)); }
	inline GameObject_t1113636619 * get_prefab3_4() const { return ___prefab3_4; }
	inline GameObject_t1113636619 ** get_address_of_prefab3_4() { return &___prefab3_4; }
	inline void set_prefab3_4(GameObject_t1113636619 * value)
	{
		___prefab3_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefab3_4), value);
	}

	inline static int32_t get_offset_of_prefab4_5() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___prefab4_5)); }
	inline GameObject_t1113636619 * get_prefab4_5() const { return ___prefab4_5; }
	inline GameObject_t1113636619 ** get_address_of_prefab4_5() { return &___prefab4_5; }
	inline void set_prefab4_5(GameObject_t1113636619 * value)
	{
		___prefab4_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefab4_5), value);
	}

	inline static int32_t get_offset_of_spawnRate_6() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___spawnRate_6)); }
	inline float get_spawnRate_6() const { return ___spawnRate_6; }
	inline float* get_address_of_spawnRate_6() { return &___spawnRate_6; }
	inline void set_spawnRate_6(float value)
	{
		___spawnRate_6 = value;
	}

	inline static int32_t get_offset_of_spawnRateHard_7() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___spawnRateHard_7)); }
	inline float get_spawnRateHard_7() const { return ___spawnRateHard_7; }
	inline float* get_address_of_spawnRateHard_7() { return &___spawnRateHard_7; }
	inline void set_spawnRateHard_7(float value)
	{
		___spawnRateHard_7 = value;
	}

	inline static int32_t get_offset_of_nextSpawn_8() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___nextSpawn_8)); }
	inline float get_nextSpawn_8() const { return ___nextSpawn_8; }
	inline float* get_address_of_nextSpawn_8() { return &___nextSpawn_8; }
	inline void set_nextSpawn_8(float value)
	{
		___nextSpawn_8 = value;
	}

	inline static int32_t get_offset_of_whatToSpawn_9() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___whatToSpawn_9)); }
	inline int32_t get_whatToSpawn_9() const { return ___whatToSpawn_9; }
	inline int32_t* get_address_of_whatToSpawn_9() { return &___whatToSpawn_9; }
	inline void set_whatToSpawn_9(int32_t value)
	{
		___whatToSpawn_9 = value;
	}

	inline static int32_t get_offset_of_theme_10() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___theme_10)); }
	inline String_t* get_theme_10() const { return ___theme_10; }
	inline String_t** get_address_of_theme_10() { return &___theme_10; }
	inline void set_theme_10(String_t* value)
	{
		___theme_10 = value;
		Il2CppCodeGenWriteBarrier((&___theme_10), value);
	}

	inline static int32_t get_offset_of_obstacles_11() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___obstacles_11)); }
	inline GameObjectU5BU5D_t3328599146* get_obstacles_11() const { return ___obstacles_11; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_obstacles_11() { return &___obstacles_11; }
	inline void set_obstacles_11(GameObjectU5BU5D_t3328599146* value)
	{
		___obstacles_11 = value;
		Il2CppCodeGenWriteBarrier((&___obstacles_11), value);
	}

	inline static int32_t get_offset_of_v3_position_12() { return static_cast<int32_t>(offsetof(RandomSpawnScript_t4065494415, ___v3_position_12)); }
	inline Vector3_t3722313464  get_v3_position_12() const { return ___v3_position_12; }
	inline Vector3_t3722313464 * get_address_of_v3_position_12() { return &___v3_position_12; }
	inline void set_v3_position_12(Vector3_t3722313464  value)
	{
		___v3_position_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMSPAWNSCRIPT_T4065494415_H
#ifndef DESTROYERSCRIPT_T3910417206_H
#define DESTROYERSCRIPT_T3910417206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyerScript
struct  DestroyerScript_t3910417206  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text DestroyerScript::scoreText
	Text_t1901882714 * ___scoreText_2;
	// System.Int32 DestroyerScript::actScore
	int32_t ___actScore_3;
	// System.Int32 DestroyerScript::pointcounter
	int32_t ___pointcounter_4;
	// UnityEngine.UI.Text DestroyerScript::specialpointstext
	Text_t1901882714 * ___specialpointstext_5;

public:
	inline static int32_t get_offset_of_scoreText_2() { return static_cast<int32_t>(offsetof(DestroyerScript_t3910417206, ___scoreText_2)); }
	inline Text_t1901882714 * get_scoreText_2() const { return ___scoreText_2; }
	inline Text_t1901882714 ** get_address_of_scoreText_2() { return &___scoreText_2; }
	inline void set_scoreText_2(Text_t1901882714 * value)
	{
		___scoreText_2 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_2), value);
	}

	inline static int32_t get_offset_of_actScore_3() { return static_cast<int32_t>(offsetof(DestroyerScript_t3910417206, ___actScore_3)); }
	inline int32_t get_actScore_3() const { return ___actScore_3; }
	inline int32_t* get_address_of_actScore_3() { return &___actScore_3; }
	inline void set_actScore_3(int32_t value)
	{
		___actScore_3 = value;
	}

	inline static int32_t get_offset_of_pointcounter_4() { return static_cast<int32_t>(offsetof(DestroyerScript_t3910417206, ___pointcounter_4)); }
	inline int32_t get_pointcounter_4() const { return ___pointcounter_4; }
	inline int32_t* get_address_of_pointcounter_4() { return &___pointcounter_4; }
	inline void set_pointcounter_4(int32_t value)
	{
		___pointcounter_4 = value;
	}

	inline static int32_t get_offset_of_specialpointstext_5() { return static_cast<int32_t>(offsetof(DestroyerScript_t3910417206, ___specialpointstext_5)); }
	inline Text_t1901882714 * get_specialpointstext_5() const { return ___specialpointstext_5; }
	inline Text_t1901882714 ** get_address_of_specialpointstext_5() { return &___specialpointstext_5; }
	inline void set_specialpointstext_5(Text_t1901882714 * value)
	{
		___specialpointstext_5 = value;
		Il2CppCodeGenWriteBarrier((&___specialpointstext_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYERSCRIPT_T3910417206_H
#ifndef LOSESCRIPT_T212724369_H
#define LOSESCRIPT_T212724369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseScript
struct  LoseScript_t212724369  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LoseScript::Game
	GameObject_t1113636619 * ___Game_2;
	// UnityEngine.GameObject LoseScript::Menu
	GameObject_t1113636619 * ___Menu_3;
	// UnityEngine.GameObject[] LoseScript::Cubes
	GameObjectU5BU5D_t3328599146* ___Cubes_4;
	// System.Int32 LoseScript::tries
	int32_t ___tries_5;
	// System.String LoseScript::difficulty
	String_t* ___difficulty_6;
	// UnityEngine.AudioSource LoseScript::audioSrc
	AudioSource_t3935305588 * ___audioSrc_7;
	// System.Int32 LoseScript::triesTillAds
	int32_t ___triesTillAds_8;
	// System.Single LoseScript::savedTimeScale
	float ___savedTimeScale_9;
	// UnityEngine.Animator LoseScript::anim
	Animator_t434523843 * ___anim_10;

public:
	inline static int32_t get_offset_of_Game_2() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___Game_2)); }
	inline GameObject_t1113636619 * get_Game_2() const { return ___Game_2; }
	inline GameObject_t1113636619 ** get_address_of_Game_2() { return &___Game_2; }
	inline void set_Game_2(GameObject_t1113636619 * value)
	{
		___Game_2 = value;
		Il2CppCodeGenWriteBarrier((&___Game_2), value);
	}

	inline static int32_t get_offset_of_Menu_3() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___Menu_3)); }
	inline GameObject_t1113636619 * get_Menu_3() const { return ___Menu_3; }
	inline GameObject_t1113636619 ** get_address_of_Menu_3() { return &___Menu_3; }
	inline void set_Menu_3(GameObject_t1113636619 * value)
	{
		___Menu_3 = value;
		Il2CppCodeGenWriteBarrier((&___Menu_3), value);
	}

	inline static int32_t get_offset_of_Cubes_4() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___Cubes_4)); }
	inline GameObjectU5BU5D_t3328599146* get_Cubes_4() const { return ___Cubes_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Cubes_4() { return &___Cubes_4; }
	inline void set_Cubes_4(GameObjectU5BU5D_t3328599146* value)
	{
		___Cubes_4 = value;
		Il2CppCodeGenWriteBarrier((&___Cubes_4), value);
	}

	inline static int32_t get_offset_of_tries_5() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___tries_5)); }
	inline int32_t get_tries_5() const { return ___tries_5; }
	inline int32_t* get_address_of_tries_5() { return &___tries_5; }
	inline void set_tries_5(int32_t value)
	{
		___tries_5 = value;
	}

	inline static int32_t get_offset_of_difficulty_6() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___difficulty_6)); }
	inline String_t* get_difficulty_6() const { return ___difficulty_6; }
	inline String_t** get_address_of_difficulty_6() { return &___difficulty_6; }
	inline void set_difficulty_6(String_t* value)
	{
		___difficulty_6 = value;
		Il2CppCodeGenWriteBarrier((&___difficulty_6), value);
	}

	inline static int32_t get_offset_of_audioSrc_7() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___audioSrc_7)); }
	inline AudioSource_t3935305588 * get_audioSrc_7() const { return ___audioSrc_7; }
	inline AudioSource_t3935305588 ** get_address_of_audioSrc_7() { return &___audioSrc_7; }
	inline void set_audioSrc_7(AudioSource_t3935305588 * value)
	{
		___audioSrc_7 = value;
		Il2CppCodeGenWriteBarrier((&___audioSrc_7), value);
	}

	inline static int32_t get_offset_of_triesTillAds_8() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___triesTillAds_8)); }
	inline int32_t get_triesTillAds_8() const { return ___triesTillAds_8; }
	inline int32_t* get_address_of_triesTillAds_8() { return &___triesTillAds_8; }
	inline void set_triesTillAds_8(int32_t value)
	{
		___triesTillAds_8 = value;
	}

	inline static int32_t get_offset_of_savedTimeScale_9() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___savedTimeScale_9)); }
	inline float get_savedTimeScale_9() const { return ___savedTimeScale_9; }
	inline float* get_address_of_savedTimeScale_9() { return &___savedTimeScale_9; }
	inline void set_savedTimeScale_9(float value)
	{
		___savedTimeScale_9 = value;
	}

	inline static int32_t get_offset_of_anim_10() { return static_cast<int32_t>(offsetof(LoseScript_t212724369, ___anim_10)); }
	inline Animator_t434523843 * get_anim_10() const { return ___anim_10; }
	inline Animator_t434523843 ** get_address_of_anim_10() { return &___anim_10; }
	inline void set_anim_10(Animator_t434523843 * value)
	{
		___anim_10 = value;
		Il2CppCodeGenWriteBarrier((&___anim_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOSESCRIPT_T212724369_H
#ifndef TRIPPLETRIGGERMIDDLESCRIPT_T4217752517_H
#define TRIPPLETRIGGERMIDDLESCRIPT_T4217752517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerMiddleScript
struct  TrippleTriggerMiddleScript_t4217752517  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TrippleTriggerMiddleScript::Game
	GameObject_t1113636619 * ___Game_2;
	// UnityEngine.GameObject TrippleTriggerMiddleScript::Menu
	GameObject_t1113636619 * ___Menu_3;
	// UnityEngine.AudioSource TrippleTriggerMiddleScript::audioSrc
	AudioSource_t3935305588 * ___audioSrc_4;
	// System.Int32 TrippleTriggerMiddleScript::triesTillAds
	int32_t ___triesTillAds_5;
	// UnityEngine.GameObject TrippleTriggerMiddleScript::btn_play
	GameObject_t1113636619 * ___btn_play_6;
	// UnityEngine.GameObject TrippleTriggerMiddleScript::text_loading
	GameObject_t1113636619 * ___text_loading_7;
	// UnityEngine.Animator TrippleTriggerMiddleScript::anim
	Animator_t434523843 * ___anim_8;
	// System.Single TrippleTriggerMiddleScript::savedTimeScale
	float ___savedTimeScale_9;

public:
	inline static int32_t get_offset_of_Game_2() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___Game_2)); }
	inline GameObject_t1113636619 * get_Game_2() const { return ___Game_2; }
	inline GameObject_t1113636619 ** get_address_of_Game_2() { return &___Game_2; }
	inline void set_Game_2(GameObject_t1113636619 * value)
	{
		___Game_2 = value;
		Il2CppCodeGenWriteBarrier((&___Game_2), value);
	}

	inline static int32_t get_offset_of_Menu_3() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___Menu_3)); }
	inline GameObject_t1113636619 * get_Menu_3() const { return ___Menu_3; }
	inline GameObject_t1113636619 ** get_address_of_Menu_3() { return &___Menu_3; }
	inline void set_Menu_3(GameObject_t1113636619 * value)
	{
		___Menu_3 = value;
		Il2CppCodeGenWriteBarrier((&___Menu_3), value);
	}

	inline static int32_t get_offset_of_audioSrc_4() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___audioSrc_4)); }
	inline AudioSource_t3935305588 * get_audioSrc_4() const { return ___audioSrc_4; }
	inline AudioSource_t3935305588 ** get_address_of_audioSrc_4() { return &___audioSrc_4; }
	inline void set_audioSrc_4(AudioSource_t3935305588 * value)
	{
		___audioSrc_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioSrc_4), value);
	}

	inline static int32_t get_offset_of_triesTillAds_5() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___triesTillAds_5)); }
	inline int32_t get_triesTillAds_5() const { return ___triesTillAds_5; }
	inline int32_t* get_address_of_triesTillAds_5() { return &___triesTillAds_5; }
	inline void set_triesTillAds_5(int32_t value)
	{
		___triesTillAds_5 = value;
	}

	inline static int32_t get_offset_of_btn_play_6() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___btn_play_6)); }
	inline GameObject_t1113636619 * get_btn_play_6() const { return ___btn_play_6; }
	inline GameObject_t1113636619 ** get_address_of_btn_play_6() { return &___btn_play_6; }
	inline void set_btn_play_6(GameObject_t1113636619 * value)
	{
		___btn_play_6 = value;
		Il2CppCodeGenWriteBarrier((&___btn_play_6), value);
	}

	inline static int32_t get_offset_of_text_loading_7() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___text_loading_7)); }
	inline GameObject_t1113636619 * get_text_loading_7() const { return ___text_loading_7; }
	inline GameObject_t1113636619 ** get_address_of_text_loading_7() { return &___text_loading_7; }
	inline void set_text_loading_7(GameObject_t1113636619 * value)
	{
		___text_loading_7 = value;
		Il2CppCodeGenWriteBarrier((&___text_loading_7), value);
	}

	inline static int32_t get_offset_of_anim_8() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___anim_8)); }
	inline Animator_t434523843 * get_anim_8() const { return ___anim_8; }
	inline Animator_t434523843 ** get_address_of_anim_8() { return &___anim_8; }
	inline void set_anim_8(Animator_t434523843 * value)
	{
		___anim_8 = value;
		Il2CppCodeGenWriteBarrier((&___anim_8), value);
	}

	inline static int32_t get_offset_of_savedTimeScale_9() { return static_cast<int32_t>(offsetof(TrippleTriggerMiddleScript_t4217752517, ___savedTimeScale_9)); }
	inline float get_savedTimeScale_9() const { return ___savedTimeScale_9; }
	inline float* get_address_of_savedTimeScale_9() { return &___savedTimeScale_9; }
	inline void set_savedTimeScale_9(float value)
	{
		___savedTimeScale_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIPPLETRIGGERMIDDLESCRIPT_T4217752517_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef BUTTONCONTROLLERSCRIPT_T2432915718_H
#define BUTTONCONTROLLERSCRIPT_T2432915718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonControllerScript
struct  ButtonControllerScript_t2432915718  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ButtonControllerScript::audioSource
	GameObject_t1113636619 * ___audioSource_2;
	// UnityEngine.AudioClip ButtonControllerScript::clip_mainSound
	AudioClip_t3680889665 * ___clip_mainSound_3;
	// UnityEngine.AudioClip ButtonControllerScript::clip_dankstormSound
	AudioClip_t3680889665 * ___clip_dankstormSound_4;
	// UnityEngine.AudioClip ButtonControllerScript::clip_skyflightSound
	AudioClip_t3680889665 * ___clip_skyflightSound_5;
	// UnityEngine.AudioClip ButtonControllerScript::clip_egyptSound
	AudioClip_t3680889665 * ___clip_egyptSound_6;
	// UnityEngine.GameObject ButtonControllerScript::backgroundSprite
	GameObject_t1113636619 * ___backgroundSprite_7;
	// UnityEngine.Sprite ButtonControllerScript::mainTheme
	Sprite_t280657092 * ___mainTheme_8;
	// UnityEngine.Sprite ButtonControllerScript::flappyTheme
	Sprite_t280657092 * ___flappyTheme_9;
	// UnityEngine.Sprite ButtonControllerScript::nightTheme
	Sprite_t280657092 * ___nightTheme_10;
	// UnityEngine.Sprite ButtonControllerScript::desertTheme
	Sprite_t280657092 * ___desertTheme_11;
	// UnityEngine.GameObject ButtonControllerScript::btn_play
	GameObject_t1113636619 * ___btn_play_12;
	// UnityEngine.GameObject ButtonControllerScript::btn_video
	GameObject_t1113636619 * ___btn_video_13;
	// UnityEngine.GameObject ButtonControllerScript::btn_setting
	GameObject_t1113636619 * ___btn_setting_14;
	// UnityEngine.GameObject ButtonControllerScript::btn_reset
	GameObject_t1113636619 * ___btn_reset_15;
	// UnityEngine.GameObject ButtonControllerScript::txt_reset
	GameObject_t1113636619 * ___txt_reset_16;
	// UnityEngine.GameObject ButtonControllerScript::btn_answers
	GameObject_t1113636619 * ___btn_answers_17;
	// UnityEngine.GameObject ButtonControllerScript::btn_noSound
	GameObject_t1113636619 * ___btn_noSound_18;
	// UnityEngine.GameObject ButtonControllerScript::btn_mainSound
	GameObject_t1113636619 * ___btn_mainSound_19;
	// UnityEngine.GameObject ButtonControllerScript::btn_skyflightSound
	GameObject_t1113636619 * ___btn_skyflightSound_20;
	// UnityEngine.GameObject ButtonControllerScript::btn_dankstormSound
	GameObject_t1113636619 * ___btn_dankstormSound_21;
	// UnityEngine.GameObject ButtonControllerScript::btn_egyptSound
	GameObject_t1113636619 * ___btn_egyptSound_22;
	// UnityEngine.GameObject ButtonControllerScript::btn_back
	GameObject_t1113636619 * ___btn_back_23;
	// UnityEngine.GameObject ButtonControllerScript::btn_buy
	GameObject_t1113636619 * ___btn_buy_24;
	// System.Int32 ButtonControllerScript::nightThemeCost
	int32_t ___nightThemeCost_25;
	// System.Int32 ButtonControllerScript::flappyThemeCost
	int32_t ___flappyThemeCost_26;
	// System.Int32 ButtonControllerScript::desertThemeCost
	int32_t ___desertThemeCost_27;
	// System.Int32 ButtonControllerScript::skyflightSoundCost
	int32_t ___skyflightSoundCost_28;
	// System.Int32 ButtonControllerScript::dankstormSoundCost
	int32_t ___dankstormSoundCost_29;
	// System.Int32 ButtonControllerScript::egyptSoundCost
	int32_t ___egyptSoundCost_30;
	// UnityEngine.GameObject ButtonControllerScript::cnv_menu
	GameObject_t1113636619 * ___cnv_menu_31;
	// UnityEngine.GameObject ButtonControllerScript::cnv_settings
	GameObject_t1113636619 * ___cnv_settings_32;
	// UnityEngine.GameObject ButtonControllerScript::cnv_game
	GameObject_t1113636619 * ___cnv_game_33;
	// UnityEngine.GameObject ButtonControllerScript::go_permanent
	GameObject_t1113636619 * ___go_permanent_34;
	// UnityEngine.GameObject ButtonControllerScript::text_buy
	GameObject_t1113636619 * ___text_buy_35;
	// UnityEngine.UI.Text ButtonControllerScript::txt_cost
	Text_t1901882714 * ___txt_cost_36;
	// UnityEngine.UI.Text ButtonControllerScript::scoreText
	Text_t1901882714 * ___scoreText_37;
	// UnityEngine.UI.Text ButtonControllerScript::specialPointsText
	Text_t1901882714 * ___specialPointsText_38;
	// UnityEngine.Animator ButtonControllerScript::anim
	Animator_t434523843 * ___anim_39;
	// System.Int32 ButtonControllerScript::triesTillAds
	int32_t ___triesTillAds_40;
	// UnityEngine.GameObject ButtonControllerScript::Cube_plusOne
	GameObject_t1113636619 * ___Cube_plusOne_41;
	// System.Int32 ButtonControllerScript::specialPoints
	int32_t ___specialPoints_42;
	// UnityEngine.GameObject ButtonControllerScript::singleObstacle
	GameObject_t1113636619 * ___singleObstacle_43;
	// UnityEngine.GameObject ButtonControllerScript::doubleObstacle
	GameObject_t1113636619 * ___doubleObstacle_44;
	// UnityEngine.GameObject ButtonControllerScript::trippleObstacle
	GameObject_t1113636619 * ___trippleObstacle_45;
	// UnityEngine.GameObject ButtonControllerScript::quattroObstacle
	GameObject_t1113636619 * ___quattroObstacle_46;
	// System.Single ButtonControllerScript::previewTime
	float ___previewTime_47;
	// UnityEngine.UI.Toggle ButtonControllerScript::btn_difficulty
	Toggle_t2735377061 * ___btn_difficulty_48;
	// System.String ButtonControllerScript::difficulty
	String_t* ___difficulty_49;
	// UnityEngine.UI.Text ButtonControllerScript::textHighscoreCount
	Text_t1901882714 * ___textHighscoreCount_50;
	// System.Int32 ButtonControllerScript::highscore
	int32_t ___highscore_51;
	// UnityEngine.UI.Text ButtonControllerScript::textPlayedCount
	Text_t1901882714 * ___textPlayedCount_52;
	// System.Int32 ButtonControllerScript::tries
	int32_t ___tries_53;
	// UnityEngine.UI.Text ButtonControllerScript::textLastPoints
	Text_t1901882714 * ___textLastPoints_54;
	// System.Int32 ButtonControllerScript::lastPoints
	int32_t ___lastPoints_55;
	// UnityEngine.GameObject ButtonControllerScript::ckm_mainTheme
	GameObject_t1113636619 * ___ckm_mainTheme_56;
	// UnityEngine.GameObject ButtonControllerScript::ckm_nightTheme
	GameObject_t1113636619 * ___ckm_nightTheme_57;
	// UnityEngine.GameObject ButtonControllerScript::ckm_flappyTheme
	GameObject_t1113636619 * ___ckm_flappyTheme_58;
	// UnityEngine.GameObject ButtonControllerScript::ckm_desertTheme
	GameObject_t1113636619 * ___ckm_desertTheme_59;
	// UnityEngine.GameObject ButtonControllerScript::ckm_noSound
	GameObject_t1113636619 * ___ckm_noSound_60;
	// UnityEngine.GameObject ButtonControllerScript::ckm_mainSound
	GameObject_t1113636619 * ___ckm_mainSound_61;
	// UnityEngine.GameObject ButtonControllerScript::ckm_skyflightSound
	GameObject_t1113636619 * ___ckm_skyflightSound_62;
	// UnityEngine.GameObject ButtonControllerScript::ckm_dankstormSound
	GameObject_t1113636619 * ___ckm_dankstormSound_63;
	// UnityEngine.GameObject ButtonControllerScript::ckm_egyptSound
	GameObject_t1113636619 * ___ckm_egyptSound_64;
	// UnityEngine.GameObject ButtonControllerScript::texts
	GameObject_t1113636619 * ___texts_65;
	// UnityEngine.UI.ScrollRect ButtonControllerScript::svSounds
	ScrollRect_t4137855814 * ___svSounds_66;
	// UnityEngine.UI.ScrollRect ButtonControllerScript::svThemes
	ScrollRect_t4137855814 * ___svThemes_67;

public:
	inline static int32_t get_offset_of_audioSource_2() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___audioSource_2)); }
	inline GameObject_t1113636619 * get_audioSource_2() const { return ___audioSource_2; }
	inline GameObject_t1113636619 ** get_address_of_audioSource_2() { return &___audioSource_2; }
	inline void set_audioSource_2(GameObject_t1113636619 * value)
	{
		___audioSource_2 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_2), value);
	}

	inline static int32_t get_offset_of_clip_mainSound_3() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___clip_mainSound_3)); }
	inline AudioClip_t3680889665 * get_clip_mainSound_3() const { return ___clip_mainSound_3; }
	inline AudioClip_t3680889665 ** get_address_of_clip_mainSound_3() { return &___clip_mainSound_3; }
	inline void set_clip_mainSound_3(AudioClip_t3680889665 * value)
	{
		___clip_mainSound_3 = value;
		Il2CppCodeGenWriteBarrier((&___clip_mainSound_3), value);
	}

	inline static int32_t get_offset_of_clip_dankstormSound_4() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___clip_dankstormSound_4)); }
	inline AudioClip_t3680889665 * get_clip_dankstormSound_4() const { return ___clip_dankstormSound_4; }
	inline AudioClip_t3680889665 ** get_address_of_clip_dankstormSound_4() { return &___clip_dankstormSound_4; }
	inline void set_clip_dankstormSound_4(AudioClip_t3680889665 * value)
	{
		___clip_dankstormSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___clip_dankstormSound_4), value);
	}

	inline static int32_t get_offset_of_clip_skyflightSound_5() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___clip_skyflightSound_5)); }
	inline AudioClip_t3680889665 * get_clip_skyflightSound_5() const { return ___clip_skyflightSound_5; }
	inline AudioClip_t3680889665 ** get_address_of_clip_skyflightSound_5() { return &___clip_skyflightSound_5; }
	inline void set_clip_skyflightSound_5(AudioClip_t3680889665 * value)
	{
		___clip_skyflightSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___clip_skyflightSound_5), value);
	}

	inline static int32_t get_offset_of_clip_egyptSound_6() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___clip_egyptSound_6)); }
	inline AudioClip_t3680889665 * get_clip_egyptSound_6() const { return ___clip_egyptSound_6; }
	inline AudioClip_t3680889665 ** get_address_of_clip_egyptSound_6() { return &___clip_egyptSound_6; }
	inline void set_clip_egyptSound_6(AudioClip_t3680889665 * value)
	{
		___clip_egyptSound_6 = value;
		Il2CppCodeGenWriteBarrier((&___clip_egyptSound_6), value);
	}

	inline static int32_t get_offset_of_backgroundSprite_7() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___backgroundSprite_7)); }
	inline GameObject_t1113636619 * get_backgroundSprite_7() const { return ___backgroundSprite_7; }
	inline GameObject_t1113636619 ** get_address_of_backgroundSprite_7() { return &___backgroundSprite_7; }
	inline void set_backgroundSprite_7(GameObject_t1113636619 * value)
	{
		___backgroundSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSprite_7), value);
	}

	inline static int32_t get_offset_of_mainTheme_8() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___mainTheme_8)); }
	inline Sprite_t280657092 * get_mainTheme_8() const { return ___mainTheme_8; }
	inline Sprite_t280657092 ** get_address_of_mainTheme_8() { return &___mainTheme_8; }
	inline void set_mainTheme_8(Sprite_t280657092 * value)
	{
		___mainTheme_8 = value;
		Il2CppCodeGenWriteBarrier((&___mainTheme_8), value);
	}

	inline static int32_t get_offset_of_flappyTheme_9() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___flappyTheme_9)); }
	inline Sprite_t280657092 * get_flappyTheme_9() const { return ___flappyTheme_9; }
	inline Sprite_t280657092 ** get_address_of_flappyTheme_9() { return &___flappyTheme_9; }
	inline void set_flappyTheme_9(Sprite_t280657092 * value)
	{
		___flappyTheme_9 = value;
		Il2CppCodeGenWriteBarrier((&___flappyTheme_9), value);
	}

	inline static int32_t get_offset_of_nightTheme_10() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___nightTheme_10)); }
	inline Sprite_t280657092 * get_nightTheme_10() const { return ___nightTheme_10; }
	inline Sprite_t280657092 ** get_address_of_nightTheme_10() { return &___nightTheme_10; }
	inline void set_nightTheme_10(Sprite_t280657092 * value)
	{
		___nightTheme_10 = value;
		Il2CppCodeGenWriteBarrier((&___nightTheme_10), value);
	}

	inline static int32_t get_offset_of_desertTheme_11() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___desertTheme_11)); }
	inline Sprite_t280657092 * get_desertTheme_11() const { return ___desertTheme_11; }
	inline Sprite_t280657092 ** get_address_of_desertTheme_11() { return &___desertTheme_11; }
	inline void set_desertTheme_11(Sprite_t280657092 * value)
	{
		___desertTheme_11 = value;
		Il2CppCodeGenWriteBarrier((&___desertTheme_11), value);
	}

	inline static int32_t get_offset_of_btn_play_12() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_play_12)); }
	inline GameObject_t1113636619 * get_btn_play_12() const { return ___btn_play_12; }
	inline GameObject_t1113636619 ** get_address_of_btn_play_12() { return &___btn_play_12; }
	inline void set_btn_play_12(GameObject_t1113636619 * value)
	{
		___btn_play_12 = value;
		Il2CppCodeGenWriteBarrier((&___btn_play_12), value);
	}

	inline static int32_t get_offset_of_btn_video_13() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_video_13)); }
	inline GameObject_t1113636619 * get_btn_video_13() const { return ___btn_video_13; }
	inline GameObject_t1113636619 ** get_address_of_btn_video_13() { return &___btn_video_13; }
	inline void set_btn_video_13(GameObject_t1113636619 * value)
	{
		___btn_video_13 = value;
		Il2CppCodeGenWriteBarrier((&___btn_video_13), value);
	}

	inline static int32_t get_offset_of_btn_setting_14() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_setting_14)); }
	inline GameObject_t1113636619 * get_btn_setting_14() const { return ___btn_setting_14; }
	inline GameObject_t1113636619 ** get_address_of_btn_setting_14() { return &___btn_setting_14; }
	inline void set_btn_setting_14(GameObject_t1113636619 * value)
	{
		___btn_setting_14 = value;
		Il2CppCodeGenWriteBarrier((&___btn_setting_14), value);
	}

	inline static int32_t get_offset_of_btn_reset_15() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_reset_15)); }
	inline GameObject_t1113636619 * get_btn_reset_15() const { return ___btn_reset_15; }
	inline GameObject_t1113636619 ** get_address_of_btn_reset_15() { return &___btn_reset_15; }
	inline void set_btn_reset_15(GameObject_t1113636619 * value)
	{
		___btn_reset_15 = value;
		Il2CppCodeGenWriteBarrier((&___btn_reset_15), value);
	}

	inline static int32_t get_offset_of_txt_reset_16() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___txt_reset_16)); }
	inline GameObject_t1113636619 * get_txt_reset_16() const { return ___txt_reset_16; }
	inline GameObject_t1113636619 ** get_address_of_txt_reset_16() { return &___txt_reset_16; }
	inline void set_txt_reset_16(GameObject_t1113636619 * value)
	{
		___txt_reset_16 = value;
		Il2CppCodeGenWriteBarrier((&___txt_reset_16), value);
	}

	inline static int32_t get_offset_of_btn_answers_17() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_answers_17)); }
	inline GameObject_t1113636619 * get_btn_answers_17() const { return ___btn_answers_17; }
	inline GameObject_t1113636619 ** get_address_of_btn_answers_17() { return &___btn_answers_17; }
	inline void set_btn_answers_17(GameObject_t1113636619 * value)
	{
		___btn_answers_17 = value;
		Il2CppCodeGenWriteBarrier((&___btn_answers_17), value);
	}

	inline static int32_t get_offset_of_btn_noSound_18() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_noSound_18)); }
	inline GameObject_t1113636619 * get_btn_noSound_18() const { return ___btn_noSound_18; }
	inline GameObject_t1113636619 ** get_address_of_btn_noSound_18() { return &___btn_noSound_18; }
	inline void set_btn_noSound_18(GameObject_t1113636619 * value)
	{
		___btn_noSound_18 = value;
		Il2CppCodeGenWriteBarrier((&___btn_noSound_18), value);
	}

	inline static int32_t get_offset_of_btn_mainSound_19() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_mainSound_19)); }
	inline GameObject_t1113636619 * get_btn_mainSound_19() const { return ___btn_mainSound_19; }
	inline GameObject_t1113636619 ** get_address_of_btn_mainSound_19() { return &___btn_mainSound_19; }
	inline void set_btn_mainSound_19(GameObject_t1113636619 * value)
	{
		___btn_mainSound_19 = value;
		Il2CppCodeGenWriteBarrier((&___btn_mainSound_19), value);
	}

	inline static int32_t get_offset_of_btn_skyflightSound_20() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_skyflightSound_20)); }
	inline GameObject_t1113636619 * get_btn_skyflightSound_20() const { return ___btn_skyflightSound_20; }
	inline GameObject_t1113636619 ** get_address_of_btn_skyflightSound_20() { return &___btn_skyflightSound_20; }
	inline void set_btn_skyflightSound_20(GameObject_t1113636619 * value)
	{
		___btn_skyflightSound_20 = value;
		Il2CppCodeGenWriteBarrier((&___btn_skyflightSound_20), value);
	}

	inline static int32_t get_offset_of_btn_dankstormSound_21() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_dankstormSound_21)); }
	inline GameObject_t1113636619 * get_btn_dankstormSound_21() const { return ___btn_dankstormSound_21; }
	inline GameObject_t1113636619 ** get_address_of_btn_dankstormSound_21() { return &___btn_dankstormSound_21; }
	inline void set_btn_dankstormSound_21(GameObject_t1113636619 * value)
	{
		___btn_dankstormSound_21 = value;
		Il2CppCodeGenWriteBarrier((&___btn_dankstormSound_21), value);
	}

	inline static int32_t get_offset_of_btn_egyptSound_22() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_egyptSound_22)); }
	inline GameObject_t1113636619 * get_btn_egyptSound_22() const { return ___btn_egyptSound_22; }
	inline GameObject_t1113636619 ** get_address_of_btn_egyptSound_22() { return &___btn_egyptSound_22; }
	inline void set_btn_egyptSound_22(GameObject_t1113636619 * value)
	{
		___btn_egyptSound_22 = value;
		Il2CppCodeGenWriteBarrier((&___btn_egyptSound_22), value);
	}

	inline static int32_t get_offset_of_btn_back_23() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_back_23)); }
	inline GameObject_t1113636619 * get_btn_back_23() const { return ___btn_back_23; }
	inline GameObject_t1113636619 ** get_address_of_btn_back_23() { return &___btn_back_23; }
	inline void set_btn_back_23(GameObject_t1113636619 * value)
	{
		___btn_back_23 = value;
		Il2CppCodeGenWriteBarrier((&___btn_back_23), value);
	}

	inline static int32_t get_offset_of_btn_buy_24() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_buy_24)); }
	inline GameObject_t1113636619 * get_btn_buy_24() const { return ___btn_buy_24; }
	inline GameObject_t1113636619 ** get_address_of_btn_buy_24() { return &___btn_buy_24; }
	inline void set_btn_buy_24(GameObject_t1113636619 * value)
	{
		___btn_buy_24 = value;
		Il2CppCodeGenWriteBarrier((&___btn_buy_24), value);
	}

	inline static int32_t get_offset_of_nightThemeCost_25() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___nightThemeCost_25)); }
	inline int32_t get_nightThemeCost_25() const { return ___nightThemeCost_25; }
	inline int32_t* get_address_of_nightThemeCost_25() { return &___nightThemeCost_25; }
	inline void set_nightThemeCost_25(int32_t value)
	{
		___nightThemeCost_25 = value;
	}

	inline static int32_t get_offset_of_flappyThemeCost_26() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___flappyThemeCost_26)); }
	inline int32_t get_flappyThemeCost_26() const { return ___flappyThemeCost_26; }
	inline int32_t* get_address_of_flappyThemeCost_26() { return &___flappyThemeCost_26; }
	inline void set_flappyThemeCost_26(int32_t value)
	{
		___flappyThemeCost_26 = value;
	}

	inline static int32_t get_offset_of_desertThemeCost_27() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___desertThemeCost_27)); }
	inline int32_t get_desertThemeCost_27() const { return ___desertThemeCost_27; }
	inline int32_t* get_address_of_desertThemeCost_27() { return &___desertThemeCost_27; }
	inline void set_desertThemeCost_27(int32_t value)
	{
		___desertThemeCost_27 = value;
	}

	inline static int32_t get_offset_of_skyflightSoundCost_28() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___skyflightSoundCost_28)); }
	inline int32_t get_skyflightSoundCost_28() const { return ___skyflightSoundCost_28; }
	inline int32_t* get_address_of_skyflightSoundCost_28() { return &___skyflightSoundCost_28; }
	inline void set_skyflightSoundCost_28(int32_t value)
	{
		___skyflightSoundCost_28 = value;
	}

	inline static int32_t get_offset_of_dankstormSoundCost_29() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___dankstormSoundCost_29)); }
	inline int32_t get_dankstormSoundCost_29() const { return ___dankstormSoundCost_29; }
	inline int32_t* get_address_of_dankstormSoundCost_29() { return &___dankstormSoundCost_29; }
	inline void set_dankstormSoundCost_29(int32_t value)
	{
		___dankstormSoundCost_29 = value;
	}

	inline static int32_t get_offset_of_egyptSoundCost_30() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___egyptSoundCost_30)); }
	inline int32_t get_egyptSoundCost_30() const { return ___egyptSoundCost_30; }
	inline int32_t* get_address_of_egyptSoundCost_30() { return &___egyptSoundCost_30; }
	inline void set_egyptSoundCost_30(int32_t value)
	{
		___egyptSoundCost_30 = value;
	}

	inline static int32_t get_offset_of_cnv_menu_31() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___cnv_menu_31)); }
	inline GameObject_t1113636619 * get_cnv_menu_31() const { return ___cnv_menu_31; }
	inline GameObject_t1113636619 ** get_address_of_cnv_menu_31() { return &___cnv_menu_31; }
	inline void set_cnv_menu_31(GameObject_t1113636619 * value)
	{
		___cnv_menu_31 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_menu_31), value);
	}

	inline static int32_t get_offset_of_cnv_settings_32() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___cnv_settings_32)); }
	inline GameObject_t1113636619 * get_cnv_settings_32() const { return ___cnv_settings_32; }
	inline GameObject_t1113636619 ** get_address_of_cnv_settings_32() { return &___cnv_settings_32; }
	inline void set_cnv_settings_32(GameObject_t1113636619 * value)
	{
		___cnv_settings_32 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_settings_32), value);
	}

	inline static int32_t get_offset_of_cnv_game_33() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___cnv_game_33)); }
	inline GameObject_t1113636619 * get_cnv_game_33() const { return ___cnv_game_33; }
	inline GameObject_t1113636619 ** get_address_of_cnv_game_33() { return &___cnv_game_33; }
	inline void set_cnv_game_33(GameObject_t1113636619 * value)
	{
		___cnv_game_33 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_game_33), value);
	}

	inline static int32_t get_offset_of_go_permanent_34() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___go_permanent_34)); }
	inline GameObject_t1113636619 * get_go_permanent_34() const { return ___go_permanent_34; }
	inline GameObject_t1113636619 ** get_address_of_go_permanent_34() { return &___go_permanent_34; }
	inline void set_go_permanent_34(GameObject_t1113636619 * value)
	{
		___go_permanent_34 = value;
		Il2CppCodeGenWriteBarrier((&___go_permanent_34), value);
	}

	inline static int32_t get_offset_of_text_buy_35() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___text_buy_35)); }
	inline GameObject_t1113636619 * get_text_buy_35() const { return ___text_buy_35; }
	inline GameObject_t1113636619 ** get_address_of_text_buy_35() { return &___text_buy_35; }
	inline void set_text_buy_35(GameObject_t1113636619 * value)
	{
		___text_buy_35 = value;
		Il2CppCodeGenWriteBarrier((&___text_buy_35), value);
	}

	inline static int32_t get_offset_of_txt_cost_36() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___txt_cost_36)); }
	inline Text_t1901882714 * get_txt_cost_36() const { return ___txt_cost_36; }
	inline Text_t1901882714 ** get_address_of_txt_cost_36() { return &___txt_cost_36; }
	inline void set_txt_cost_36(Text_t1901882714 * value)
	{
		___txt_cost_36 = value;
		Il2CppCodeGenWriteBarrier((&___txt_cost_36), value);
	}

	inline static int32_t get_offset_of_scoreText_37() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___scoreText_37)); }
	inline Text_t1901882714 * get_scoreText_37() const { return ___scoreText_37; }
	inline Text_t1901882714 ** get_address_of_scoreText_37() { return &___scoreText_37; }
	inline void set_scoreText_37(Text_t1901882714 * value)
	{
		___scoreText_37 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_37), value);
	}

	inline static int32_t get_offset_of_specialPointsText_38() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___specialPointsText_38)); }
	inline Text_t1901882714 * get_specialPointsText_38() const { return ___specialPointsText_38; }
	inline Text_t1901882714 ** get_address_of_specialPointsText_38() { return &___specialPointsText_38; }
	inline void set_specialPointsText_38(Text_t1901882714 * value)
	{
		___specialPointsText_38 = value;
		Il2CppCodeGenWriteBarrier((&___specialPointsText_38), value);
	}

	inline static int32_t get_offset_of_anim_39() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___anim_39)); }
	inline Animator_t434523843 * get_anim_39() const { return ___anim_39; }
	inline Animator_t434523843 ** get_address_of_anim_39() { return &___anim_39; }
	inline void set_anim_39(Animator_t434523843 * value)
	{
		___anim_39 = value;
		Il2CppCodeGenWriteBarrier((&___anim_39), value);
	}

	inline static int32_t get_offset_of_triesTillAds_40() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___triesTillAds_40)); }
	inline int32_t get_triesTillAds_40() const { return ___triesTillAds_40; }
	inline int32_t* get_address_of_triesTillAds_40() { return &___triesTillAds_40; }
	inline void set_triesTillAds_40(int32_t value)
	{
		___triesTillAds_40 = value;
	}

	inline static int32_t get_offset_of_Cube_plusOne_41() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___Cube_plusOne_41)); }
	inline GameObject_t1113636619 * get_Cube_plusOne_41() const { return ___Cube_plusOne_41; }
	inline GameObject_t1113636619 ** get_address_of_Cube_plusOne_41() { return &___Cube_plusOne_41; }
	inline void set_Cube_plusOne_41(GameObject_t1113636619 * value)
	{
		___Cube_plusOne_41 = value;
		Il2CppCodeGenWriteBarrier((&___Cube_plusOne_41), value);
	}

	inline static int32_t get_offset_of_specialPoints_42() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___specialPoints_42)); }
	inline int32_t get_specialPoints_42() const { return ___specialPoints_42; }
	inline int32_t* get_address_of_specialPoints_42() { return &___specialPoints_42; }
	inline void set_specialPoints_42(int32_t value)
	{
		___specialPoints_42 = value;
	}

	inline static int32_t get_offset_of_singleObstacle_43() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___singleObstacle_43)); }
	inline GameObject_t1113636619 * get_singleObstacle_43() const { return ___singleObstacle_43; }
	inline GameObject_t1113636619 ** get_address_of_singleObstacle_43() { return &___singleObstacle_43; }
	inline void set_singleObstacle_43(GameObject_t1113636619 * value)
	{
		___singleObstacle_43 = value;
		Il2CppCodeGenWriteBarrier((&___singleObstacle_43), value);
	}

	inline static int32_t get_offset_of_doubleObstacle_44() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___doubleObstacle_44)); }
	inline GameObject_t1113636619 * get_doubleObstacle_44() const { return ___doubleObstacle_44; }
	inline GameObject_t1113636619 ** get_address_of_doubleObstacle_44() { return &___doubleObstacle_44; }
	inline void set_doubleObstacle_44(GameObject_t1113636619 * value)
	{
		___doubleObstacle_44 = value;
		Il2CppCodeGenWriteBarrier((&___doubleObstacle_44), value);
	}

	inline static int32_t get_offset_of_trippleObstacle_45() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___trippleObstacle_45)); }
	inline GameObject_t1113636619 * get_trippleObstacle_45() const { return ___trippleObstacle_45; }
	inline GameObject_t1113636619 ** get_address_of_trippleObstacle_45() { return &___trippleObstacle_45; }
	inline void set_trippleObstacle_45(GameObject_t1113636619 * value)
	{
		___trippleObstacle_45 = value;
		Il2CppCodeGenWriteBarrier((&___trippleObstacle_45), value);
	}

	inline static int32_t get_offset_of_quattroObstacle_46() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___quattroObstacle_46)); }
	inline GameObject_t1113636619 * get_quattroObstacle_46() const { return ___quattroObstacle_46; }
	inline GameObject_t1113636619 ** get_address_of_quattroObstacle_46() { return &___quattroObstacle_46; }
	inline void set_quattroObstacle_46(GameObject_t1113636619 * value)
	{
		___quattroObstacle_46 = value;
		Il2CppCodeGenWriteBarrier((&___quattroObstacle_46), value);
	}

	inline static int32_t get_offset_of_previewTime_47() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___previewTime_47)); }
	inline float get_previewTime_47() const { return ___previewTime_47; }
	inline float* get_address_of_previewTime_47() { return &___previewTime_47; }
	inline void set_previewTime_47(float value)
	{
		___previewTime_47 = value;
	}

	inline static int32_t get_offset_of_btn_difficulty_48() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___btn_difficulty_48)); }
	inline Toggle_t2735377061 * get_btn_difficulty_48() const { return ___btn_difficulty_48; }
	inline Toggle_t2735377061 ** get_address_of_btn_difficulty_48() { return &___btn_difficulty_48; }
	inline void set_btn_difficulty_48(Toggle_t2735377061 * value)
	{
		___btn_difficulty_48 = value;
		Il2CppCodeGenWriteBarrier((&___btn_difficulty_48), value);
	}

	inline static int32_t get_offset_of_difficulty_49() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___difficulty_49)); }
	inline String_t* get_difficulty_49() const { return ___difficulty_49; }
	inline String_t** get_address_of_difficulty_49() { return &___difficulty_49; }
	inline void set_difficulty_49(String_t* value)
	{
		___difficulty_49 = value;
		Il2CppCodeGenWriteBarrier((&___difficulty_49), value);
	}

	inline static int32_t get_offset_of_textHighscoreCount_50() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___textHighscoreCount_50)); }
	inline Text_t1901882714 * get_textHighscoreCount_50() const { return ___textHighscoreCount_50; }
	inline Text_t1901882714 ** get_address_of_textHighscoreCount_50() { return &___textHighscoreCount_50; }
	inline void set_textHighscoreCount_50(Text_t1901882714 * value)
	{
		___textHighscoreCount_50 = value;
		Il2CppCodeGenWriteBarrier((&___textHighscoreCount_50), value);
	}

	inline static int32_t get_offset_of_highscore_51() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___highscore_51)); }
	inline int32_t get_highscore_51() const { return ___highscore_51; }
	inline int32_t* get_address_of_highscore_51() { return &___highscore_51; }
	inline void set_highscore_51(int32_t value)
	{
		___highscore_51 = value;
	}

	inline static int32_t get_offset_of_textPlayedCount_52() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___textPlayedCount_52)); }
	inline Text_t1901882714 * get_textPlayedCount_52() const { return ___textPlayedCount_52; }
	inline Text_t1901882714 ** get_address_of_textPlayedCount_52() { return &___textPlayedCount_52; }
	inline void set_textPlayedCount_52(Text_t1901882714 * value)
	{
		___textPlayedCount_52 = value;
		Il2CppCodeGenWriteBarrier((&___textPlayedCount_52), value);
	}

	inline static int32_t get_offset_of_tries_53() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___tries_53)); }
	inline int32_t get_tries_53() const { return ___tries_53; }
	inline int32_t* get_address_of_tries_53() { return &___tries_53; }
	inline void set_tries_53(int32_t value)
	{
		___tries_53 = value;
	}

	inline static int32_t get_offset_of_textLastPoints_54() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___textLastPoints_54)); }
	inline Text_t1901882714 * get_textLastPoints_54() const { return ___textLastPoints_54; }
	inline Text_t1901882714 ** get_address_of_textLastPoints_54() { return &___textLastPoints_54; }
	inline void set_textLastPoints_54(Text_t1901882714 * value)
	{
		___textLastPoints_54 = value;
		Il2CppCodeGenWriteBarrier((&___textLastPoints_54), value);
	}

	inline static int32_t get_offset_of_lastPoints_55() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___lastPoints_55)); }
	inline int32_t get_lastPoints_55() const { return ___lastPoints_55; }
	inline int32_t* get_address_of_lastPoints_55() { return &___lastPoints_55; }
	inline void set_lastPoints_55(int32_t value)
	{
		___lastPoints_55 = value;
	}

	inline static int32_t get_offset_of_ckm_mainTheme_56() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_mainTheme_56)); }
	inline GameObject_t1113636619 * get_ckm_mainTheme_56() const { return ___ckm_mainTheme_56; }
	inline GameObject_t1113636619 ** get_address_of_ckm_mainTheme_56() { return &___ckm_mainTheme_56; }
	inline void set_ckm_mainTheme_56(GameObject_t1113636619 * value)
	{
		___ckm_mainTheme_56 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_mainTheme_56), value);
	}

	inline static int32_t get_offset_of_ckm_nightTheme_57() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_nightTheme_57)); }
	inline GameObject_t1113636619 * get_ckm_nightTheme_57() const { return ___ckm_nightTheme_57; }
	inline GameObject_t1113636619 ** get_address_of_ckm_nightTheme_57() { return &___ckm_nightTheme_57; }
	inline void set_ckm_nightTheme_57(GameObject_t1113636619 * value)
	{
		___ckm_nightTheme_57 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_nightTheme_57), value);
	}

	inline static int32_t get_offset_of_ckm_flappyTheme_58() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_flappyTheme_58)); }
	inline GameObject_t1113636619 * get_ckm_flappyTheme_58() const { return ___ckm_flappyTheme_58; }
	inline GameObject_t1113636619 ** get_address_of_ckm_flappyTheme_58() { return &___ckm_flappyTheme_58; }
	inline void set_ckm_flappyTheme_58(GameObject_t1113636619 * value)
	{
		___ckm_flappyTheme_58 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_flappyTheme_58), value);
	}

	inline static int32_t get_offset_of_ckm_desertTheme_59() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_desertTheme_59)); }
	inline GameObject_t1113636619 * get_ckm_desertTheme_59() const { return ___ckm_desertTheme_59; }
	inline GameObject_t1113636619 ** get_address_of_ckm_desertTheme_59() { return &___ckm_desertTheme_59; }
	inline void set_ckm_desertTheme_59(GameObject_t1113636619 * value)
	{
		___ckm_desertTheme_59 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_desertTheme_59), value);
	}

	inline static int32_t get_offset_of_ckm_noSound_60() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_noSound_60)); }
	inline GameObject_t1113636619 * get_ckm_noSound_60() const { return ___ckm_noSound_60; }
	inline GameObject_t1113636619 ** get_address_of_ckm_noSound_60() { return &___ckm_noSound_60; }
	inline void set_ckm_noSound_60(GameObject_t1113636619 * value)
	{
		___ckm_noSound_60 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_noSound_60), value);
	}

	inline static int32_t get_offset_of_ckm_mainSound_61() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_mainSound_61)); }
	inline GameObject_t1113636619 * get_ckm_mainSound_61() const { return ___ckm_mainSound_61; }
	inline GameObject_t1113636619 ** get_address_of_ckm_mainSound_61() { return &___ckm_mainSound_61; }
	inline void set_ckm_mainSound_61(GameObject_t1113636619 * value)
	{
		___ckm_mainSound_61 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_mainSound_61), value);
	}

	inline static int32_t get_offset_of_ckm_skyflightSound_62() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_skyflightSound_62)); }
	inline GameObject_t1113636619 * get_ckm_skyflightSound_62() const { return ___ckm_skyflightSound_62; }
	inline GameObject_t1113636619 ** get_address_of_ckm_skyflightSound_62() { return &___ckm_skyflightSound_62; }
	inline void set_ckm_skyflightSound_62(GameObject_t1113636619 * value)
	{
		___ckm_skyflightSound_62 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_skyflightSound_62), value);
	}

	inline static int32_t get_offset_of_ckm_dankstormSound_63() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_dankstormSound_63)); }
	inline GameObject_t1113636619 * get_ckm_dankstormSound_63() const { return ___ckm_dankstormSound_63; }
	inline GameObject_t1113636619 ** get_address_of_ckm_dankstormSound_63() { return &___ckm_dankstormSound_63; }
	inline void set_ckm_dankstormSound_63(GameObject_t1113636619 * value)
	{
		___ckm_dankstormSound_63 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_dankstormSound_63), value);
	}

	inline static int32_t get_offset_of_ckm_egyptSound_64() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___ckm_egyptSound_64)); }
	inline GameObject_t1113636619 * get_ckm_egyptSound_64() const { return ___ckm_egyptSound_64; }
	inline GameObject_t1113636619 ** get_address_of_ckm_egyptSound_64() { return &___ckm_egyptSound_64; }
	inline void set_ckm_egyptSound_64(GameObject_t1113636619 * value)
	{
		___ckm_egyptSound_64 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_egyptSound_64), value);
	}

	inline static int32_t get_offset_of_texts_65() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___texts_65)); }
	inline GameObject_t1113636619 * get_texts_65() const { return ___texts_65; }
	inline GameObject_t1113636619 ** get_address_of_texts_65() { return &___texts_65; }
	inline void set_texts_65(GameObject_t1113636619 * value)
	{
		___texts_65 = value;
		Il2CppCodeGenWriteBarrier((&___texts_65), value);
	}

	inline static int32_t get_offset_of_svSounds_66() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___svSounds_66)); }
	inline ScrollRect_t4137855814 * get_svSounds_66() const { return ___svSounds_66; }
	inline ScrollRect_t4137855814 ** get_address_of_svSounds_66() { return &___svSounds_66; }
	inline void set_svSounds_66(ScrollRect_t4137855814 * value)
	{
		___svSounds_66 = value;
		Il2CppCodeGenWriteBarrier((&___svSounds_66), value);
	}

	inline static int32_t get_offset_of_svThemes_67() { return static_cast<int32_t>(offsetof(ButtonControllerScript_t2432915718, ___svThemes_67)); }
	inline ScrollRect_t4137855814 * get_svThemes_67() const { return ___svThemes_67; }
	inline ScrollRect_t4137855814 ** get_address_of_svThemes_67() { return &___svThemes_67; }
	inline void set_svThemes_67(ScrollRect_t4137855814 * value)
	{
		___svThemes_67 = value;
		Il2CppCodeGenWriteBarrier((&___svThemes_67), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCONTROLLERSCRIPT_T2432915718_H
#ifndef BALLBUTTONCONTROLLERSCRIPT_T1117378164_H
#define BALLBUTTONCONTROLLERSCRIPT_T1117378164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallButtonControllerScript
struct  BallButtonControllerScript_t1117378164  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator BallButtonControllerScript::anim
	Animator_t434523843 * ___anim_2;

public:
	inline static int32_t get_offset_of_anim_2() { return static_cast<int32_t>(offsetof(BallButtonControllerScript_t1117378164, ___anim_2)); }
	inline Animator_t434523843 * get_anim_2() const { return ___anim_2; }
	inline Animator_t434523843 ** get_address_of_anim_2() { return &___anim_2; }
	inline void set_anim_2(Animator_t434523843 * value)
	{
		___anim_2 = value;
		Il2CppCodeGenWriteBarrier((&___anim_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLBUTTONCONTROLLERSCRIPT_T1117378164_H
#ifndef INITTEXTSSCRIPT_T1053055510_H
#define INITTEXTSSCRIPT_T1053055510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitTextsScript
struct  InitTextsScript_t1053055510  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject InitTextsScript::cnv_menu
	GameObject_t1113636619 * ___cnv_menu_2;
	// UnityEngine.GameObject InitTextsScript::cnv_settings
	GameObject_t1113636619 * ___cnv_settings_3;
	// UnityEngine.GameObject InitTextsScript::cnv_game
	GameObject_t1113636619 * ___cnv_game_4;
	// UnityEngine.GameObject InitTextsScript::texts
	GameObject_t1113636619 * ___texts_5;
	// UnityEngine.UI.Text InitTextsScript::textHighscoreCount
	Text_t1901882714 * ___textHighscoreCount_6;
	// System.Int32 InitTextsScript::highscore
	int32_t ___highscore_7;
	// UnityEngine.UI.Text InitTextsScript::textPlayedCount
	Text_t1901882714 * ___textPlayedCount_8;
	// System.Int32 InitTextsScript::tries
	int32_t ___tries_9;
	// UnityEngine.UI.Text InitTextsScript::textLastPoints
	Text_t1901882714 * ___textLastPoints_10;
	// System.Int32 InitTextsScript::lastPoints
	int32_t ___lastPoints_11;
	// UnityEngine.UI.Text InitTextsScript::scoreText
	Text_t1901882714 * ___scoreText_12;
	// UnityEngine.UI.Text InitTextsScript::specialPointsText
	Text_t1901882714 * ___specialPointsText_13;
	// UnityEngine.GameObject InitTextsScript::backgroundSpriteMenu
	GameObject_t1113636619 * ___backgroundSpriteMenu_14;
	// UnityEngine.GameObject InitTextsScript::backgroundSpriteSettings
	GameObject_t1113636619 * ___backgroundSpriteSettings_15;
	// UnityEngine.GameObject InitTextsScript::backgroundSpriteGame
	GameObject_t1113636619 * ___backgroundSpriteGame_16;
	// UnityEngine.Sprite InitTextsScript::mainTheme
	Sprite_t280657092 * ___mainTheme_17;
	// UnityEngine.Sprite InitTextsScript::flappyTheme
	Sprite_t280657092 * ___flappyTheme_18;
	// UnityEngine.Sprite InitTextsScript::nightTheme
	Sprite_t280657092 * ___nightTheme_19;
	// UnityEngine.Sprite InitTextsScript::desertTheme
	Sprite_t280657092 * ___desertTheme_20;
	// UnityEngine.SpriteRenderer InitTextsScript::bgSprite
	SpriteRenderer_t3235626157 * ___bgSprite_21;
	// UnityEngine.GameObject InitTextsScript::audioSource
	GameObject_t1113636619 * ___audioSource_22;
	// UnityEngine.AudioClip InitTextsScript::clip_mainSound
	AudioClip_t3680889665 * ___clip_mainSound_23;
	// UnityEngine.AudioClip InitTextsScript::clip_dankstormSound
	AudioClip_t3680889665 * ___clip_dankstormSound_24;
	// UnityEngine.AudioClip InitTextsScript::clip_skyflightSound
	AudioClip_t3680889665 * ___clip_skyflightSound_25;
	// UnityEngine.AudioClip InitTextsScript::clip_egyptSound
	AudioClip_t3680889665 * ___clip_egyptSound_26;
	// System.Single InitTextsScript::previewTime
	float ___previewTime_27;
	// System.String InitTextsScript::difficulty
	String_t* ___difficulty_28;

public:
	inline static int32_t get_offset_of_cnv_menu_2() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___cnv_menu_2)); }
	inline GameObject_t1113636619 * get_cnv_menu_2() const { return ___cnv_menu_2; }
	inline GameObject_t1113636619 ** get_address_of_cnv_menu_2() { return &___cnv_menu_2; }
	inline void set_cnv_menu_2(GameObject_t1113636619 * value)
	{
		___cnv_menu_2 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_menu_2), value);
	}

	inline static int32_t get_offset_of_cnv_settings_3() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___cnv_settings_3)); }
	inline GameObject_t1113636619 * get_cnv_settings_3() const { return ___cnv_settings_3; }
	inline GameObject_t1113636619 ** get_address_of_cnv_settings_3() { return &___cnv_settings_3; }
	inline void set_cnv_settings_3(GameObject_t1113636619 * value)
	{
		___cnv_settings_3 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_settings_3), value);
	}

	inline static int32_t get_offset_of_cnv_game_4() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___cnv_game_4)); }
	inline GameObject_t1113636619 * get_cnv_game_4() const { return ___cnv_game_4; }
	inline GameObject_t1113636619 ** get_address_of_cnv_game_4() { return &___cnv_game_4; }
	inline void set_cnv_game_4(GameObject_t1113636619 * value)
	{
		___cnv_game_4 = value;
		Il2CppCodeGenWriteBarrier((&___cnv_game_4), value);
	}

	inline static int32_t get_offset_of_texts_5() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___texts_5)); }
	inline GameObject_t1113636619 * get_texts_5() const { return ___texts_5; }
	inline GameObject_t1113636619 ** get_address_of_texts_5() { return &___texts_5; }
	inline void set_texts_5(GameObject_t1113636619 * value)
	{
		___texts_5 = value;
		Il2CppCodeGenWriteBarrier((&___texts_5), value);
	}

	inline static int32_t get_offset_of_textHighscoreCount_6() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___textHighscoreCount_6)); }
	inline Text_t1901882714 * get_textHighscoreCount_6() const { return ___textHighscoreCount_6; }
	inline Text_t1901882714 ** get_address_of_textHighscoreCount_6() { return &___textHighscoreCount_6; }
	inline void set_textHighscoreCount_6(Text_t1901882714 * value)
	{
		___textHighscoreCount_6 = value;
		Il2CppCodeGenWriteBarrier((&___textHighscoreCount_6), value);
	}

	inline static int32_t get_offset_of_highscore_7() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___highscore_7)); }
	inline int32_t get_highscore_7() const { return ___highscore_7; }
	inline int32_t* get_address_of_highscore_7() { return &___highscore_7; }
	inline void set_highscore_7(int32_t value)
	{
		___highscore_7 = value;
	}

	inline static int32_t get_offset_of_textPlayedCount_8() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___textPlayedCount_8)); }
	inline Text_t1901882714 * get_textPlayedCount_8() const { return ___textPlayedCount_8; }
	inline Text_t1901882714 ** get_address_of_textPlayedCount_8() { return &___textPlayedCount_8; }
	inline void set_textPlayedCount_8(Text_t1901882714 * value)
	{
		___textPlayedCount_8 = value;
		Il2CppCodeGenWriteBarrier((&___textPlayedCount_8), value);
	}

	inline static int32_t get_offset_of_tries_9() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___tries_9)); }
	inline int32_t get_tries_9() const { return ___tries_9; }
	inline int32_t* get_address_of_tries_9() { return &___tries_9; }
	inline void set_tries_9(int32_t value)
	{
		___tries_9 = value;
	}

	inline static int32_t get_offset_of_textLastPoints_10() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___textLastPoints_10)); }
	inline Text_t1901882714 * get_textLastPoints_10() const { return ___textLastPoints_10; }
	inline Text_t1901882714 ** get_address_of_textLastPoints_10() { return &___textLastPoints_10; }
	inline void set_textLastPoints_10(Text_t1901882714 * value)
	{
		___textLastPoints_10 = value;
		Il2CppCodeGenWriteBarrier((&___textLastPoints_10), value);
	}

	inline static int32_t get_offset_of_lastPoints_11() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___lastPoints_11)); }
	inline int32_t get_lastPoints_11() const { return ___lastPoints_11; }
	inline int32_t* get_address_of_lastPoints_11() { return &___lastPoints_11; }
	inline void set_lastPoints_11(int32_t value)
	{
		___lastPoints_11 = value;
	}

	inline static int32_t get_offset_of_scoreText_12() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___scoreText_12)); }
	inline Text_t1901882714 * get_scoreText_12() const { return ___scoreText_12; }
	inline Text_t1901882714 ** get_address_of_scoreText_12() { return &___scoreText_12; }
	inline void set_scoreText_12(Text_t1901882714 * value)
	{
		___scoreText_12 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_12), value);
	}

	inline static int32_t get_offset_of_specialPointsText_13() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___specialPointsText_13)); }
	inline Text_t1901882714 * get_specialPointsText_13() const { return ___specialPointsText_13; }
	inline Text_t1901882714 ** get_address_of_specialPointsText_13() { return &___specialPointsText_13; }
	inline void set_specialPointsText_13(Text_t1901882714 * value)
	{
		___specialPointsText_13 = value;
		Il2CppCodeGenWriteBarrier((&___specialPointsText_13), value);
	}

	inline static int32_t get_offset_of_backgroundSpriteMenu_14() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___backgroundSpriteMenu_14)); }
	inline GameObject_t1113636619 * get_backgroundSpriteMenu_14() const { return ___backgroundSpriteMenu_14; }
	inline GameObject_t1113636619 ** get_address_of_backgroundSpriteMenu_14() { return &___backgroundSpriteMenu_14; }
	inline void set_backgroundSpriteMenu_14(GameObject_t1113636619 * value)
	{
		___backgroundSpriteMenu_14 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSpriteMenu_14), value);
	}

	inline static int32_t get_offset_of_backgroundSpriteSettings_15() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___backgroundSpriteSettings_15)); }
	inline GameObject_t1113636619 * get_backgroundSpriteSettings_15() const { return ___backgroundSpriteSettings_15; }
	inline GameObject_t1113636619 ** get_address_of_backgroundSpriteSettings_15() { return &___backgroundSpriteSettings_15; }
	inline void set_backgroundSpriteSettings_15(GameObject_t1113636619 * value)
	{
		___backgroundSpriteSettings_15 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSpriteSettings_15), value);
	}

	inline static int32_t get_offset_of_backgroundSpriteGame_16() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___backgroundSpriteGame_16)); }
	inline GameObject_t1113636619 * get_backgroundSpriteGame_16() const { return ___backgroundSpriteGame_16; }
	inline GameObject_t1113636619 ** get_address_of_backgroundSpriteGame_16() { return &___backgroundSpriteGame_16; }
	inline void set_backgroundSpriteGame_16(GameObject_t1113636619 * value)
	{
		___backgroundSpriteGame_16 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundSpriteGame_16), value);
	}

	inline static int32_t get_offset_of_mainTheme_17() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___mainTheme_17)); }
	inline Sprite_t280657092 * get_mainTheme_17() const { return ___mainTheme_17; }
	inline Sprite_t280657092 ** get_address_of_mainTheme_17() { return &___mainTheme_17; }
	inline void set_mainTheme_17(Sprite_t280657092 * value)
	{
		___mainTheme_17 = value;
		Il2CppCodeGenWriteBarrier((&___mainTheme_17), value);
	}

	inline static int32_t get_offset_of_flappyTheme_18() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___flappyTheme_18)); }
	inline Sprite_t280657092 * get_flappyTheme_18() const { return ___flappyTheme_18; }
	inline Sprite_t280657092 ** get_address_of_flappyTheme_18() { return &___flappyTheme_18; }
	inline void set_flappyTheme_18(Sprite_t280657092 * value)
	{
		___flappyTheme_18 = value;
		Il2CppCodeGenWriteBarrier((&___flappyTheme_18), value);
	}

	inline static int32_t get_offset_of_nightTheme_19() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___nightTheme_19)); }
	inline Sprite_t280657092 * get_nightTheme_19() const { return ___nightTheme_19; }
	inline Sprite_t280657092 ** get_address_of_nightTheme_19() { return &___nightTheme_19; }
	inline void set_nightTheme_19(Sprite_t280657092 * value)
	{
		___nightTheme_19 = value;
		Il2CppCodeGenWriteBarrier((&___nightTheme_19), value);
	}

	inline static int32_t get_offset_of_desertTheme_20() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___desertTheme_20)); }
	inline Sprite_t280657092 * get_desertTheme_20() const { return ___desertTheme_20; }
	inline Sprite_t280657092 ** get_address_of_desertTheme_20() { return &___desertTheme_20; }
	inline void set_desertTheme_20(Sprite_t280657092 * value)
	{
		___desertTheme_20 = value;
		Il2CppCodeGenWriteBarrier((&___desertTheme_20), value);
	}

	inline static int32_t get_offset_of_bgSprite_21() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___bgSprite_21)); }
	inline SpriteRenderer_t3235626157 * get_bgSprite_21() const { return ___bgSprite_21; }
	inline SpriteRenderer_t3235626157 ** get_address_of_bgSprite_21() { return &___bgSprite_21; }
	inline void set_bgSprite_21(SpriteRenderer_t3235626157 * value)
	{
		___bgSprite_21 = value;
		Il2CppCodeGenWriteBarrier((&___bgSprite_21), value);
	}

	inline static int32_t get_offset_of_audioSource_22() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___audioSource_22)); }
	inline GameObject_t1113636619 * get_audioSource_22() const { return ___audioSource_22; }
	inline GameObject_t1113636619 ** get_address_of_audioSource_22() { return &___audioSource_22; }
	inline void set_audioSource_22(GameObject_t1113636619 * value)
	{
		___audioSource_22 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_22), value);
	}

	inline static int32_t get_offset_of_clip_mainSound_23() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___clip_mainSound_23)); }
	inline AudioClip_t3680889665 * get_clip_mainSound_23() const { return ___clip_mainSound_23; }
	inline AudioClip_t3680889665 ** get_address_of_clip_mainSound_23() { return &___clip_mainSound_23; }
	inline void set_clip_mainSound_23(AudioClip_t3680889665 * value)
	{
		___clip_mainSound_23 = value;
		Il2CppCodeGenWriteBarrier((&___clip_mainSound_23), value);
	}

	inline static int32_t get_offset_of_clip_dankstormSound_24() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___clip_dankstormSound_24)); }
	inline AudioClip_t3680889665 * get_clip_dankstormSound_24() const { return ___clip_dankstormSound_24; }
	inline AudioClip_t3680889665 ** get_address_of_clip_dankstormSound_24() { return &___clip_dankstormSound_24; }
	inline void set_clip_dankstormSound_24(AudioClip_t3680889665 * value)
	{
		___clip_dankstormSound_24 = value;
		Il2CppCodeGenWriteBarrier((&___clip_dankstormSound_24), value);
	}

	inline static int32_t get_offset_of_clip_skyflightSound_25() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___clip_skyflightSound_25)); }
	inline AudioClip_t3680889665 * get_clip_skyflightSound_25() const { return ___clip_skyflightSound_25; }
	inline AudioClip_t3680889665 ** get_address_of_clip_skyflightSound_25() { return &___clip_skyflightSound_25; }
	inline void set_clip_skyflightSound_25(AudioClip_t3680889665 * value)
	{
		___clip_skyflightSound_25 = value;
		Il2CppCodeGenWriteBarrier((&___clip_skyflightSound_25), value);
	}

	inline static int32_t get_offset_of_clip_egyptSound_26() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___clip_egyptSound_26)); }
	inline AudioClip_t3680889665 * get_clip_egyptSound_26() const { return ___clip_egyptSound_26; }
	inline AudioClip_t3680889665 ** get_address_of_clip_egyptSound_26() { return &___clip_egyptSound_26; }
	inline void set_clip_egyptSound_26(AudioClip_t3680889665 * value)
	{
		___clip_egyptSound_26 = value;
		Il2CppCodeGenWriteBarrier((&___clip_egyptSound_26), value);
	}

	inline static int32_t get_offset_of_previewTime_27() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___previewTime_27)); }
	inline float get_previewTime_27() const { return ___previewTime_27; }
	inline float* get_address_of_previewTime_27() { return &___previewTime_27; }
	inline void set_previewTime_27(float value)
	{
		___previewTime_27 = value;
	}

	inline static int32_t get_offset_of_difficulty_28() { return static_cast<int32_t>(offsetof(InitTextsScript_t1053055510, ___difficulty_28)); }
	inline String_t* get_difficulty_28() const { return ___difficulty_28; }
	inline String_t** get_address_of_difficulty_28() { return &___difficulty_28; }
	inline void set_difficulty_28(String_t* value)
	{
		___difficulty_28 = value;
		Il2CppCodeGenWriteBarrier((&___difficulty_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITTEXTSSCRIPT_T1053055510_H
#ifndef INITBUTTONSSCRIPT_T1987868624_H
#define INITBUTTONSSCRIPT_T1987868624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitButtonsScript
struct  InitButtonsScript_t1987868624  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject InitButtonsScript::btn_nightThemeInactive
	GameObject_t1113636619 * ___btn_nightThemeInactive_2;
	// UnityEngine.GameObject InitButtonsScript::btn_flappyThemeInactive
	GameObject_t1113636619 * ___btn_flappyThemeInactive_3;
	// UnityEngine.GameObject InitButtonsScript::btn_desertThemeInactive
	GameObject_t1113636619 * ___btn_desertThemeInactive_4;
	// UnityEngine.GameObject InitButtonsScript::btn_skyflightSoundInactive
	GameObject_t1113636619 * ___btn_skyflightSoundInactive_5;
	// UnityEngine.GameObject InitButtonsScript::btn_dankstormSoundInactive
	GameObject_t1113636619 * ___btn_dankstormSoundInactive_6;
	// UnityEngine.GameObject InitButtonsScript::btn_egyptSoundInactive
	GameObject_t1113636619 * ___btn_egyptSoundInactive_7;
	// UnityEngine.GameObject InitButtonsScript::btn_difficulty
	GameObject_t1113636619 * ___btn_difficulty_8;
	// UnityEngine.GameObject InitButtonsScript::ckm_mainTheme
	GameObject_t1113636619 * ___ckm_mainTheme_9;
	// UnityEngine.GameObject InitButtonsScript::ckm_nightTheme
	GameObject_t1113636619 * ___ckm_nightTheme_10;
	// UnityEngine.GameObject InitButtonsScript::ckm_flappyTheme
	GameObject_t1113636619 * ___ckm_flappyTheme_11;
	// UnityEngine.GameObject InitButtonsScript::ckm_desertTheme
	GameObject_t1113636619 * ___ckm_desertTheme_12;
	// UnityEngine.GameObject InitButtonsScript::ckm_noSound
	GameObject_t1113636619 * ___ckm_noSound_13;
	// UnityEngine.GameObject InitButtonsScript::ckm_mainSound
	GameObject_t1113636619 * ___ckm_mainSound_14;
	// UnityEngine.GameObject InitButtonsScript::ckm_skyflightSound
	GameObject_t1113636619 * ___ckm_skyflightSound_15;
	// UnityEngine.GameObject InitButtonsScript::ckm_dankstormSound
	GameObject_t1113636619 * ___ckm_dankstormSound_16;
	// UnityEngine.GameObject InitButtonsScript::ckm_egyptSound
	GameObject_t1113636619 * ___ckm_egyptSound_17;

public:
	inline static int32_t get_offset_of_btn_nightThemeInactive_2() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_nightThemeInactive_2)); }
	inline GameObject_t1113636619 * get_btn_nightThemeInactive_2() const { return ___btn_nightThemeInactive_2; }
	inline GameObject_t1113636619 ** get_address_of_btn_nightThemeInactive_2() { return &___btn_nightThemeInactive_2; }
	inline void set_btn_nightThemeInactive_2(GameObject_t1113636619 * value)
	{
		___btn_nightThemeInactive_2 = value;
		Il2CppCodeGenWriteBarrier((&___btn_nightThemeInactive_2), value);
	}

	inline static int32_t get_offset_of_btn_flappyThemeInactive_3() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_flappyThemeInactive_3)); }
	inline GameObject_t1113636619 * get_btn_flappyThemeInactive_3() const { return ___btn_flappyThemeInactive_3; }
	inline GameObject_t1113636619 ** get_address_of_btn_flappyThemeInactive_3() { return &___btn_flappyThemeInactive_3; }
	inline void set_btn_flappyThemeInactive_3(GameObject_t1113636619 * value)
	{
		___btn_flappyThemeInactive_3 = value;
		Il2CppCodeGenWriteBarrier((&___btn_flappyThemeInactive_3), value);
	}

	inline static int32_t get_offset_of_btn_desertThemeInactive_4() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_desertThemeInactive_4)); }
	inline GameObject_t1113636619 * get_btn_desertThemeInactive_4() const { return ___btn_desertThemeInactive_4; }
	inline GameObject_t1113636619 ** get_address_of_btn_desertThemeInactive_4() { return &___btn_desertThemeInactive_4; }
	inline void set_btn_desertThemeInactive_4(GameObject_t1113636619 * value)
	{
		___btn_desertThemeInactive_4 = value;
		Il2CppCodeGenWriteBarrier((&___btn_desertThemeInactive_4), value);
	}

	inline static int32_t get_offset_of_btn_skyflightSoundInactive_5() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_skyflightSoundInactive_5)); }
	inline GameObject_t1113636619 * get_btn_skyflightSoundInactive_5() const { return ___btn_skyflightSoundInactive_5; }
	inline GameObject_t1113636619 ** get_address_of_btn_skyflightSoundInactive_5() { return &___btn_skyflightSoundInactive_5; }
	inline void set_btn_skyflightSoundInactive_5(GameObject_t1113636619 * value)
	{
		___btn_skyflightSoundInactive_5 = value;
		Il2CppCodeGenWriteBarrier((&___btn_skyflightSoundInactive_5), value);
	}

	inline static int32_t get_offset_of_btn_dankstormSoundInactive_6() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_dankstormSoundInactive_6)); }
	inline GameObject_t1113636619 * get_btn_dankstormSoundInactive_6() const { return ___btn_dankstormSoundInactive_6; }
	inline GameObject_t1113636619 ** get_address_of_btn_dankstormSoundInactive_6() { return &___btn_dankstormSoundInactive_6; }
	inline void set_btn_dankstormSoundInactive_6(GameObject_t1113636619 * value)
	{
		___btn_dankstormSoundInactive_6 = value;
		Il2CppCodeGenWriteBarrier((&___btn_dankstormSoundInactive_6), value);
	}

	inline static int32_t get_offset_of_btn_egyptSoundInactive_7() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_egyptSoundInactive_7)); }
	inline GameObject_t1113636619 * get_btn_egyptSoundInactive_7() const { return ___btn_egyptSoundInactive_7; }
	inline GameObject_t1113636619 ** get_address_of_btn_egyptSoundInactive_7() { return &___btn_egyptSoundInactive_7; }
	inline void set_btn_egyptSoundInactive_7(GameObject_t1113636619 * value)
	{
		___btn_egyptSoundInactive_7 = value;
		Il2CppCodeGenWriteBarrier((&___btn_egyptSoundInactive_7), value);
	}

	inline static int32_t get_offset_of_btn_difficulty_8() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___btn_difficulty_8)); }
	inline GameObject_t1113636619 * get_btn_difficulty_8() const { return ___btn_difficulty_8; }
	inline GameObject_t1113636619 ** get_address_of_btn_difficulty_8() { return &___btn_difficulty_8; }
	inline void set_btn_difficulty_8(GameObject_t1113636619 * value)
	{
		___btn_difficulty_8 = value;
		Il2CppCodeGenWriteBarrier((&___btn_difficulty_8), value);
	}

	inline static int32_t get_offset_of_ckm_mainTheme_9() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_mainTheme_9)); }
	inline GameObject_t1113636619 * get_ckm_mainTheme_9() const { return ___ckm_mainTheme_9; }
	inline GameObject_t1113636619 ** get_address_of_ckm_mainTheme_9() { return &___ckm_mainTheme_9; }
	inline void set_ckm_mainTheme_9(GameObject_t1113636619 * value)
	{
		___ckm_mainTheme_9 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_mainTheme_9), value);
	}

	inline static int32_t get_offset_of_ckm_nightTheme_10() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_nightTheme_10)); }
	inline GameObject_t1113636619 * get_ckm_nightTheme_10() const { return ___ckm_nightTheme_10; }
	inline GameObject_t1113636619 ** get_address_of_ckm_nightTheme_10() { return &___ckm_nightTheme_10; }
	inline void set_ckm_nightTheme_10(GameObject_t1113636619 * value)
	{
		___ckm_nightTheme_10 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_nightTheme_10), value);
	}

	inline static int32_t get_offset_of_ckm_flappyTheme_11() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_flappyTheme_11)); }
	inline GameObject_t1113636619 * get_ckm_flappyTheme_11() const { return ___ckm_flappyTheme_11; }
	inline GameObject_t1113636619 ** get_address_of_ckm_flappyTheme_11() { return &___ckm_flappyTheme_11; }
	inline void set_ckm_flappyTheme_11(GameObject_t1113636619 * value)
	{
		___ckm_flappyTheme_11 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_flappyTheme_11), value);
	}

	inline static int32_t get_offset_of_ckm_desertTheme_12() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_desertTheme_12)); }
	inline GameObject_t1113636619 * get_ckm_desertTheme_12() const { return ___ckm_desertTheme_12; }
	inline GameObject_t1113636619 ** get_address_of_ckm_desertTheme_12() { return &___ckm_desertTheme_12; }
	inline void set_ckm_desertTheme_12(GameObject_t1113636619 * value)
	{
		___ckm_desertTheme_12 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_desertTheme_12), value);
	}

	inline static int32_t get_offset_of_ckm_noSound_13() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_noSound_13)); }
	inline GameObject_t1113636619 * get_ckm_noSound_13() const { return ___ckm_noSound_13; }
	inline GameObject_t1113636619 ** get_address_of_ckm_noSound_13() { return &___ckm_noSound_13; }
	inline void set_ckm_noSound_13(GameObject_t1113636619 * value)
	{
		___ckm_noSound_13 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_noSound_13), value);
	}

	inline static int32_t get_offset_of_ckm_mainSound_14() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_mainSound_14)); }
	inline GameObject_t1113636619 * get_ckm_mainSound_14() const { return ___ckm_mainSound_14; }
	inline GameObject_t1113636619 ** get_address_of_ckm_mainSound_14() { return &___ckm_mainSound_14; }
	inline void set_ckm_mainSound_14(GameObject_t1113636619 * value)
	{
		___ckm_mainSound_14 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_mainSound_14), value);
	}

	inline static int32_t get_offset_of_ckm_skyflightSound_15() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_skyflightSound_15)); }
	inline GameObject_t1113636619 * get_ckm_skyflightSound_15() const { return ___ckm_skyflightSound_15; }
	inline GameObject_t1113636619 ** get_address_of_ckm_skyflightSound_15() { return &___ckm_skyflightSound_15; }
	inline void set_ckm_skyflightSound_15(GameObject_t1113636619 * value)
	{
		___ckm_skyflightSound_15 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_skyflightSound_15), value);
	}

	inline static int32_t get_offset_of_ckm_dankstormSound_16() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_dankstormSound_16)); }
	inline GameObject_t1113636619 * get_ckm_dankstormSound_16() const { return ___ckm_dankstormSound_16; }
	inline GameObject_t1113636619 ** get_address_of_ckm_dankstormSound_16() { return &___ckm_dankstormSound_16; }
	inline void set_ckm_dankstormSound_16(GameObject_t1113636619 * value)
	{
		___ckm_dankstormSound_16 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_dankstormSound_16), value);
	}

	inline static int32_t get_offset_of_ckm_egyptSound_17() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624, ___ckm_egyptSound_17)); }
	inline GameObject_t1113636619 * get_ckm_egyptSound_17() const { return ___ckm_egyptSound_17; }
	inline GameObject_t1113636619 ** get_address_of_ckm_egyptSound_17() { return &___ckm_egyptSound_17; }
	inline void set_ckm_egyptSound_17(GameObject_t1113636619 * value)
	{
		___ckm_egyptSound_17 = value;
		Il2CppCodeGenWriteBarrier((&___ckm_egyptSound_17), value);
	}
};

struct InitButtonsScript_t1987868624_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> InitButtonsScript::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_18() { return static_cast<int32_t>(offsetof(InitButtonsScript_t1987868624_StaticFields, ___U3CU3Ef__switchU24map0_18)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_18() const { return ___U3CU3Ef__switchU24map0_18; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_18() { return &___U3CU3Ef__switchU24map0_18; }
	inline void set_U3CU3Ef__switchU24map0_18(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITBUTTONSSCRIPT_T1987868624_H
#ifndef TRIPPLETRIGGERRIGHTSCRIPT_T853323302_H
#define TRIPPLETRIGGERRIGHTSCRIPT_T853323302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrippleTriggerRightScript
struct  TrippleTriggerRightScript_t853323302  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIPPLETRIGGERRIGHTSCRIPT_T853323302_H
#ifndef SCROLLRECT_T4137855814_H
#define SCROLLRECT_T4137855814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect
struct  ScrollRect_t4137855814  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t3704657025 * ___m_Content_2;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_3;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_4;
	// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_5;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_6;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_7;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_8;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_9;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t3704657025 * ___m_Viewport_10;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_t1494447233 * ___m_HorizontalScrollbar_11;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_t1494447233 * ___m_VerticalScrollbar_12;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_13;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_14;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_15;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_16;
	// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_t343079324 * ___m_OnValueChanged_17;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_t2156229523  ___m_PointerStartLocalCursor_18;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_t2156229523  ___m_ContentStartPosition_19;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t3704657025 * ___m_ViewRect_20;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_t2266837910  ___m_ContentBounds_21;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_t2266837910  ___m_ViewBounds_22;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_t2156229523  ___m_Velocity_23;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_t2156229523  ___m_PrevPosition_25;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_t2266837910  ___m_PrevContentBounds_26;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_t2266837910  ___m_PrevViewBounds_27;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_28;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_29;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_30;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_31;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_32;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t3704657025 * ___m_Rect_33;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t3704657025 * ___m_HorizontalScrollbarRect_34;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t3704657025 * ___m_VerticalScrollbarRect_35;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_36;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_37;

public:
	inline static int32_t get_offset_of_m_Content_2() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Content_2)); }
	inline RectTransform_t3704657025 * get_m_Content_2() const { return ___m_Content_2; }
	inline RectTransform_t3704657025 ** get_address_of_m_Content_2() { return &___m_Content_2; }
	inline void set_m_Content_2(RectTransform_t3704657025 * value)
	{
		___m_Content_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_2), value);
	}

	inline static int32_t get_offset_of_m_Horizontal_3() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Horizontal_3)); }
	inline bool get_m_Horizontal_3() const { return ___m_Horizontal_3; }
	inline bool* get_address_of_m_Horizontal_3() { return &___m_Horizontal_3; }
	inline void set_m_Horizontal_3(bool value)
	{
		___m_Horizontal_3 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_4() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Vertical_4)); }
	inline bool get_m_Vertical_4() const { return ___m_Vertical_4; }
	inline bool* get_address_of_m_Vertical_4() { return &___m_Vertical_4; }
	inline void set_m_Vertical_4(bool value)
	{
		___m_Vertical_4 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_5() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_MovementType_5)); }
	inline int32_t get_m_MovementType_5() const { return ___m_MovementType_5; }
	inline int32_t* get_address_of_m_MovementType_5() { return &___m_MovementType_5; }
	inline void set_m_MovementType_5(int32_t value)
	{
		___m_MovementType_5 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_6() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Elasticity_6)); }
	inline float get_m_Elasticity_6() const { return ___m_Elasticity_6; }
	inline float* get_address_of_m_Elasticity_6() { return &___m_Elasticity_6; }
	inline void set_m_Elasticity_6(float value)
	{
		___m_Elasticity_6 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_7() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Inertia_7)); }
	inline bool get_m_Inertia_7() const { return ___m_Inertia_7; }
	inline bool* get_address_of_m_Inertia_7() { return &___m_Inertia_7; }
	inline void set_m_Inertia_7(bool value)
	{
		___m_Inertia_7 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_8() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_DecelerationRate_8)); }
	inline float get_m_DecelerationRate_8() const { return ___m_DecelerationRate_8; }
	inline float* get_address_of_m_DecelerationRate_8() { return &___m_DecelerationRate_8; }
	inline void set_m_DecelerationRate_8(float value)
	{
		___m_DecelerationRate_8 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_9() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ScrollSensitivity_9)); }
	inline float get_m_ScrollSensitivity_9() const { return ___m_ScrollSensitivity_9; }
	inline float* get_address_of_m_ScrollSensitivity_9() { return &___m_ScrollSensitivity_9; }
	inline void set_m_ScrollSensitivity_9(float value)
	{
		___m_ScrollSensitivity_9 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_10() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Viewport_10)); }
	inline RectTransform_t3704657025 * get_m_Viewport_10() const { return ___m_Viewport_10; }
	inline RectTransform_t3704657025 ** get_address_of_m_Viewport_10() { return &___m_Viewport_10; }
	inline void set_m_Viewport_10(RectTransform_t3704657025 * value)
	{
		___m_Viewport_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Viewport_10), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_11() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbar_11)); }
	inline Scrollbar_t1494447233 * get_m_HorizontalScrollbar_11() const { return ___m_HorizontalScrollbar_11; }
	inline Scrollbar_t1494447233 ** get_address_of_m_HorizontalScrollbar_11() { return &___m_HorizontalScrollbar_11; }
	inline void set_m_HorizontalScrollbar_11(Scrollbar_t1494447233 * value)
	{
		___m_HorizontalScrollbar_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbar_11), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_12() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbar_12)); }
	inline Scrollbar_t1494447233 * get_m_VerticalScrollbar_12() const { return ___m_VerticalScrollbar_12; }
	inline Scrollbar_t1494447233 ** get_address_of_m_VerticalScrollbar_12() { return &___m_VerticalScrollbar_12; }
	inline void set_m_VerticalScrollbar_12(Scrollbar_t1494447233 * value)
	{
		___m_VerticalScrollbar_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbar_12), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_13() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarVisibility_13)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_13() const { return ___m_HorizontalScrollbarVisibility_13; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_13() { return &___m_HorizontalScrollbarVisibility_13; }
	inline void set_m_HorizontalScrollbarVisibility_13(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_13 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_14() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarVisibility_14)); }
	inline int32_t get_m_VerticalScrollbarVisibility_14() const { return ___m_VerticalScrollbarVisibility_14; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_14() { return &___m_VerticalScrollbarVisibility_14; }
	inline void set_m_VerticalScrollbarVisibility_14(int32_t value)
	{
		___m_VerticalScrollbarVisibility_14 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_15() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarSpacing_15)); }
	inline float get_m_HorizontalScrollbarSpacing_15() const { return ___m_HorizontalScrollbarSpacing_15; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_15() { return &___m_HorizontalScrollbarSpacing_15; }
	inline void set_m_HorizontalScrollbarSpacing_15(float value)
	{
		___m_HorizontalScrollbarSpacing_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_16() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarSpacing_16)); }
	inline float get_m_VerticalScrollbarSpacing_16() const { return ___m_VerticalScrollbarSpacing_16; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_16() { return &___m_VerticalScrollbarSpacing_16; }
	inline void set_m_VerticalScrollbarSpacing_16(float value)
	{
		___m_VerticalScrollbarSpacing_16 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_17() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_OnValueChanged_17)); }
	inline ScrollRectEvent_t343079324 * get_m_OnValueChanged_17() const { return ___m_OnValueChanged_17; }
	inline ScrollRectEvent_t343079324 ** get_address_of_m_OnValueChanged_17() { return &___m_OnValueChanged_17; }
	inline void set_m_OnValueChanged_17(ScrollRectEvent_t343079324 * value)
	{
		___m_OnValueChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_17), value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_18() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PointerStartLocalCursor_18)); }
	inline Vector2_t2156229523  get_m_PointerStartLocalCursor_18() const { return ___m_PointerStartLocalCursor_18; }
	inline Vector2_t2156229523 * get_address_of_m_PointerStartLocalCursor_18() { return &___m_PointerStartLocalCursor_18; }
	inline void set_m_PointerStartLocalCursor_18(Vector2_t2156229523  value)
	{
		___m_PointerStartLocalCursor_18 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_19() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ContentStartPosition_19)); }
	inline Vector2_t2156229523  get_m_ContentStartPosition_19() const { return ___m_ContentStartPosition_19; }
	inline Vector2_t2156229523 * get_address_of_m_ContentStartPosition_19() { return &___m_ContentStartPosition_19; }
	inline void set_m_ContentStartPosition_19(Vector2_t2156229523  value)
	{
		___m_ContentStartPosition_19 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_20() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ViewRect_20)); }
	inline RectTransform_t3704657025 * get_m_ViewRect_20() const { return ___m_ViewRect_20; }
	inline RectTransform_t3704657025 ** get_address_of_m_ViewRect_20() { return &___m_ViewRect_20; }
	inline void set_m_ViewRect_20(RectTransform_t3704657025 * value)
	{
		___m_ViewRect_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ViewRect_20), value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_21() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ContentBounds_21)); }
	inline Bounds_t2266837910  get_m_ContentBounds_21() const { return ___m_ContentBounds_21; }
	inline Bounds_t2266837910 * get_address_of_m_ContentBounds_21() { return &___m_ContentBounds_21; }
	inline void set_m_ContentBounds_21(Bounds_t2266837910  value)
	{
		___m_ContentBounds_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_22() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_ViewBounds_22)); }
	inline Bounds_t2266837910  get_m_ViewBounds_22() const { return ___m_ViewBounds_22; }
	inline Bounds_t2266837910 * get_address_of_m_ViewBounds_22() { return &___m_ViewBounds_22; }
	inline void set_m_ViewBounds_22(Bounds_t2266837910  value)
	{
		___m_ViewBounds_22 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_23() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Velocity_23)); }
	inline Vector2_t2156229523  get_m_Velocity_23() const { return ___m_Velocity_23; }
	inline Vector2_t2156229523 * get_address_of_m_Velocity_23() { return &___m_Velocity_23; }
	inline void set_m_Velocity_23(Vector2_t2156229523  value)
	{
		___m_Velocity_23 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_24() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Dragging_24)); }
	inline bool get_m_Dragging_24() const { return ___m_Dragging_24; }
	inline bool* get_address_of_m_Dragging_24() { return &___m_Dragging_24; }
	inline void set_m_Dragging_24(bool value)
	{
		___m_Dragging_24 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_25() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevPosition_25)); }
	inline Vector2_t2156229523  get_m_PrevPosition_25() const { return ___m_PrevPosition_25; }
	inline Vector2_t2156229523 * get_address_of_m_PrevPosition_25() { return &___m_PrevPosition_25; }
	inline void set_m_PrevPosition_25(Vector2_t2156229523  value)
	{
		___m_PrevPosition_25 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_26() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevContentBounds_26)); }
	inline Bounds_t2266837910  get_m_PrevContentBounds_26() const { return ___m_PrevContentBounds_26; }
	inline Bounds_t2266837910 * get_address_of_m_PrevContentBounds_26() { return &___m_PrevContentBounds_26; }
	inline void set_m_PrevContentBounds_26(Bounds_t2266837910  value)
	{
		___m_PrevContentBounds_26 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_27() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_PrevViewBounds_27)); }
	inline Bounds_t2266837910  get_m_PrevViewBounds_27() const { return ___m_PrevViewBounds_27; }
	inline Bounds_t2266837910 * get_address_of_m_PrevViewBounds_27() { return &___m_PrevViewBounds_27; }
	inline void set_m_PrevViewBounds_27(Bounds_t2266837910  value)
	{
		___m_PrevViewBounds_27 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_28() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HasRebuiltLayout_28)); }
	inline bool get_m_HasRebuiltLayout_28() const { return ___m_HasRebuiltLayout_28; }
	inline bool* get_address_of_m_HasRebuiltLayout_28() { return &___m_HasRebuiltLayout_28; }
	inline void set_m_HasRebuiltLayout_28(bool value)
	{
		___m_HasRebuiltLayout_28 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_29() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HSliderExpand_29)); }
	inline bool get_m_HSliderExpand_29() const { return ___m_HSliderExpand_29; }
	inline bool* get_address_of_m_HSliderExpand_29() { return &___m_HSliderExpand_29; }
	inline void set_m_HSliderExpand_29(bool value)
	{
		___m_HSliderExpand_29 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_30() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VSliderExpand_30)); }
	inline bool get_m_VSliderExpand_30() const { return ___m_VSliderExpand_30; }
	inline bool* get_address_of_m_VSliderExpand_30() { return &___m_VSliderExpand_30; }
	inline void set_m_VSliderExpand_30(bool value)
	{
		___m_VSliderExpand_30 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_31() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HSliderHeight_31)); }
	inline float get_m_HSliderHeight_31() const { return ___m_HSliderHeight_31; }
	inline float* get_address_of_m_HSliderHeight_31() { return &___m_HSliderHeight_31; }
	inline void set_m_HSliderHeight_31(float value)
	{
		___m_HSliderHeight_31 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_32() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VSliderWidth_32)); }
	inline float get_m_VSliderWidth_32() const { return ___m_VSliderWidth_32; }
	inline float* get_address_of_m_VSliderWidth_32() { return &___m_VSliderWidth_32; }
	inline void set_m_VSliderWidth_32(float value)
	{
		___m_VSliderWidth_32 = value;
	}

	inline static int32_t get_offset_of_m_Rect_33() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Rect_33)); }
	inline RectTransform_t3704657025 * get_m_Rect_33() const { return ___m_Rect_33; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_33() { return &___m_Rect_33; }
	inline void set_m_Rect_33(RectTransform_t3704657025 * value)
	{
		___m_Rect_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_33), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_34() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_HorizontalScrollbarRect_34)); }
	inline RectTransform_t3704657025 * get_m_HorizontalScrollbarRect_34() const { return ___m_HorizontalScrollbarRect_34; }
	inline RectTransform_t3704657025 ** get_address_of_m_HorizontalScrollbarRect_34() { return &___m_HorizontalScrollbarRect_34; }
	inline void set_m_HorizontalScrollbarRect_34(RectTransform_t3704657025 * value)
	{
		___m_HorizontalScrollbarRect_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbarRect_34), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_35() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_VerticalScrollbarRect_35)); }
	inline RectTransform_t3704657025 * get_m_VerticalScrollbarRect_35() const { return ___m_VerticalScrollbarRect_35; }
	inline RectTransform_t3704657025 ** get_address_of_m_VerticalScrollbarRect_35() { return &___m_VerticalScrollbarRect_35; }
	inline void set_m_VerticalScrollbarRect_35(RectTransform_t3704657025 * value)
	{
		___m_VerticalScrollbarRect_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbarRect_35), value);
	}

	inline static int32_t get_offset_of_m_Tracker_36() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Tracker_36)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_36() const { return ___m_Tracker_36; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_36() { return &___m_Tracker_36; }
	inline void set_m_Tracker_36(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_36 = value;
	}

	inline static int32_t get_offset_of_m_Corners_37() { return static_cast<int32_t>(offsetof(ScrollRect_t4137855814, ___m_Corners_37)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_37() const { return ___m_Corners_37; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_37() { return &___m_Corners_37; }
	inline void set_m_Corners_37(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECT_T4137855814_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TOGGLE_T2735377061_H
#define TOGGLE_T2735377061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle
struct  Toggle_t2735377061  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Toggle/ToggleTransition UnityEngine.UI.Toggle::toggleTransition
	int32_t ___toggleTransition_16;
	// UnityEngine.UI.Graphic UnityEngine.UI.Toggle::graphic
	Graphic_t1660335611 * ___graphic_17;
	// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::m_Group
	ToggleGroup_t123837990 * ___m_Group_18;
	// UnityEngine.UI.Toggle/ToggleEvent UnityEngine.UI.Toggle::onValueChanged
	ToggleEvent_t1873685584 * ___onValueChanged_19;
	// System.Boolean UnityEngine.UI.Toggle::m_IsOn
	bool ___m_IsOn_20;

public:
	inline static int32_t get_offset_of_toggleTransition_16() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___toggleTransition_16)); }
	inline int32_t get_toggleTransition_16() const { return ___toggleTransition_16; }
	inline int32_t* get_address_of_toggleTransition_16() { return &___toggleTransition_16; }
	inline void set_toggleTransition_16(int32_t value)
	{
		___toggleTransition_16 = value;
	}

	inline static int32_t get_offset_of_graphic_17() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___graphic_17)); }
	inline Graphic_t1660335611 * get_graphic_17() const { return ___graphic_17; }
	inline Graphic_t1660335611 ** get_address_of_graphic_17() { return &___graphic_17; }
	inline void set_graphic_17(Graphic_t1660335611 * value)
	{
		___graphic_17 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_17), value);
	}

	inline static int32_t get_offset_of_m_Group_18() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___m_Group_18)); }
	inline ToggleGroup_t123837990 * get_m_Group_18() const { return ___m_Group_18; }
	inline ToggleGroup_t123837990 ** get_address_of_m_Group_18() { return &___m_Group_18; }
	inline void set_m_Group_18(ToggleGroup_t123837990 * value)
	{
		___m_Group_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Group_18), value);
	}

	inline static int32_t get_offset_of_onValueChanged_19() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___onValueChanged_19)); }
	inline ToggleEvent_t1873685584 * get_onValueChanged_19() const { return ___onValueChanged_19; }
	inline ToggleEvent_t1873685584 ** get_address_of_onValueChanged_19() { return &___onValueChanged_19; }
	inline void set_onValueChanged_19(ToggleEvent_t1873685584 * value)
	{
		___onValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_19), value);
	}

	inline static int32_t get_offset_of_m_IsOn_20() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___m_IsOn_20)); }
	inline bool get_m_IsOn_20() const { return ___m_IsOn_20; }
	inline bool* get_address_of_m_IsOn_20() { return &___m_IsOn_20; }
	inline void set_m_IsOn_20(bool value)
	{
		___m_IsOn_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLE_T2735377061_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Text_t1901882714 * m_Items[1];

public:
	inline Text_t1901882714 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Text_t1901882714 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Text_t1901882714 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Text_t1901882714 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Text_t1901882714 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Text_t1901882714 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3007623985_gshared (UnityAction_1_t682124106 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
extern "C"  void UnityEvent_1_AddListener_m2847988282_gshared (UnityEvent_1_t978947469 * __this, UnityAction_1_t682124106 * p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponentInChildren_TisRuntimeObject_m1513755678_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.Advertisements.ShowResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3797332079_gshared (Action_1_t3243021218 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m182537451_gshared (Dictionary_2_t3384741 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1279427033_gshared (Dictionary_2_t3384741 * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3959998165_gshared (Dictionary_2_t3384741 * __this, RuntimeObject * p0, int32_t* p1, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* GameObject_GetComponentsInChildren_TisRuntimeObject_m1982918030_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m1135049463_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern "C"  String_t* PlayerPrefs_GetString_m389913383 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern "C"  void PlayerPrefs_SetString_m2101271233 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonDown(System.String)
extern "C"  bool CrossPlatformInputManager_GetButtonDown_m1689635996 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t434523843_m2351447238(__this, method) ((  Animator_t434523843 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.Animator::Play(System.String)
extern "C"  void Animator_Play_m1697843332 (Animator_t434523843 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonUp(System.String)
extern "C"  bool CrossPlatformInputManager_GetButtonUp_m100471868 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonUp(System.String)
extern "C"  void CrossPlatformInputManager_SetButtonUp_m3204959434 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::checkSelectedSound()
extern "C"  void ButtonControllerScript_checkSelectedSound_m3649248901 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::checkSelectedTheme()
extern "C"  void ButtonControllerScript_checkSelectedTheme_m1504630267 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m2842000469 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::selectSound(System.String)
extern "C"  void ButtonControllerScript_selectSound_m808880357 (ButtonControllerScript_t2432915718 * __this, String_t* ___sound0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::selectTheme(System.String)
extern "C"  void ButtonControllerScript_selectTheme_m4172231223 (ButtonControllerScript_t2432915718 * __this, String_t* ___theme0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::get_activeSelf()
extern "C"  bool GameObject_get_activeSelf_m1767405923 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<InitTextsScript>()
#define GameObject_GetComponent_TisInitTextsScript_t1053055510_m1730174322(__this, method) ((  InitTextsScript_t1053055510 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void InitTextsScript::UpdateTexts()
extern "C"  void InitTextsScript_UpdateTexts_m2560498976 (InitTextsScript_t1053055510 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m3007623985(__this, p0, p1, method) ((  void (*) (UnityAction_1_t682124106 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m3007623985_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
#define UnityEvent_1_AddListener_m2847988282(__this, p0, method) ((  void (*) (UnityEvent_1_t978947469 *, UnityAction_1_t682124106 *, const RuntimeMethod*))UnityEvent_1_AddListener_m2847988282_gshared)(__this, p0, method)
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
extern "C"  void Toggle_set_isOn_m3548357404 (Toggle_t2735377061 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C"  int32_t PlayerPrefs_GetInt_m3797620966 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::resetGravity()
extern "C"  void ButtonControllerScript_resetGravity_m3471744663 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ButtonControllerScript::DelayGameStart()
extern "C"  RuntimeObject* ButtonControllerScript_DelayGameStart_m2227959488 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
extern "C"  void Advertisement_Initialize_m1051450402 (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ButtonControllerScript::ShowAdWhenReady()
extern "C"  RuntimeObject* ButtonControllerScript_ShowAdWhenReady_m2072552498 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponentInChildren<InitButtonsScript>()
#define GameObject_GetComponentInChildren_TisInitButtonsScript_t1987868624_m2265097840(__this, method) ((  InitButtonsScript_t1987868624 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_m1513755678_gshared)(__this, method)
// System.Void InitButtonsScript::InitButtons()
extern "C"  void InitButtonsScript_InitButtons_m1101143926 (InitButtonsScript_t1987868624 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::resetThings()
extern "C"  void ButtonControllerScript_resetThings_m3684079304 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ScrollRect::set_verticalNormalizedPosition(System.Single)
extern "C"  void ScrollRect_set_verticalNormalizedPosition_m1452826170 (ScrollRect_t4137855814 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::selectButton()
extern "C"  void ButtonControllerScript_selectButton_m619056489 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::checkBuyButtonToShow(System.Int32,System.String)
extern "C"  void ButtonControllerScript_checkBuyButtonToShow_m3246718299 (ButtonControllerScript_t2432915718 * __this, int32_t ___cost0, String_t* ___buttonName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::changeTheme(System.String)
extern "C"  void ButtonControllerScript_changeTheme_m1190884257 (ButtonControllerScript_t2432915718 * __this, String_t* ___theme0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::playPreviewSound(System.String)
extern "C"  void ButtonControllerScript_playPreviewSound_m1032544213 (ButtonControllerScript_t2432915718 * __this, String_t* ___previewSound0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::performPurchase()
extern "C"  void ButtonControllerScript_performPurchase_m2064459992 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(__this, method) ((  AudioSource_t3935305588 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Single UnityEngine.AudioSource::get_time()
extern "C"  float AudioSource_get_time_m2174765632 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<InitButtonsScript>()
#define GameObject_GetComponent_TisInitButtonsScript_t1987868624_m198946856(__this, method) ((  InitButtonsScript_t1987868624 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void ButtonControllerScript/<DelayGameStart>c__Iterator0::.ctor()
extern "C"  void U3CDelayGameStartU3Ec__Iterator0__ctor_m3486972112 (U3CDelayGameStartU3Ec__Iterator0_t3856269498 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32)
extern "C"  String_t* String_Substring_m2848979100 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript::disableCheckmarks(System.String)
extern "C"  void ButtonControllerScript_disableCheckmarks_m343318825 (ButtonControllerScript_t2432915718 * __this, String_t* ___category0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C"  AudioClip_t3680889665 * AudioSource_get_clip_m1234340632 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C"  void AudioSource_set_clip_m31653938 (AudioSource_t3935305588 * __this, AudioClip_t3680889665 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m48294159 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(__this, method) ((  SpriteRenderer_t3235626157 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_set_sprite_m1286893786 (SpriteRenderer_t3235626157 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::.ctor()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1__ctor_m4043849735 (U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ButtonControllerScript::HandleAnimation()
extern "C"  RuntimeObject* ButtonControllerScript_HandleAnimation_m1917644310 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonControllerScript/<HandleAnimation>c__Iterator2::.ctor()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2__ctor_m3913915067 (U3CHandleAnimationU3Ec__Iterator2_t1056433757 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<GravityScript>()
#define GameObject_GetComponent_TisGravityScript_t2220762432_m2338767488(__this, method) ((  GravityScript_t2220762432 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void GravityScript::resetGravity()
extern "C"  void GravityScript_resetGravity_m3601770064 (GravityScript_t2220762432 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m215368492 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
extern "C"  bool Advertisement_get_isShowing_m3507224835 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::IsReady()
extern "C"  bool Advertisement_IsReady_m2792558112 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern "C"  void ShowOptions__ctor_m2194205660 (ShowOptions_t990845000 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.Advertisements.ShowResult>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m3797332079(__this, p0, p1, method) ((  void (*) (Action_1_t3243021218 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m3797332079_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern "C"  void ShowOptions_set_resultCallback_m3887508449 (ShowOptions_t990845000 * __this, Action_1_t3243021218 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::Show(UnityEngine.Advertisements.ShowOptions)
extern "C"  void Advertisement_Show_m53580060 (RuntimeObject * __this /* static, unused */, ShowOptions_t990845000 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m4211327027 (Object_t631007953 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GameObject::get_tag()
extern "C"  String_t* GameObject_get_tag_m3951609671 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m3053443106 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2392909825(__this, p0, method) ((  void (*) (Dictionary_2_t2736202052 *, int32_t, const RuntimeMethod*))Dictionary_2__ctor_m182537451_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1)
#define Dictionary_2_Add_m282647386(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2736202052 *, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_Add_m1279427033_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1013208020(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t2736202052 *, String_t*, int32_t*, const RuntimeMethod*))Dictionary_2_TryGetValue_m3959998165_gshared)(__this, p0, p1, method)
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.SpriteRenderer::get_sprite()
extern "C"  Sprite_t280657092 * SpriteRenderer_get_sprite_m663386871 (SpriteRenderer_t3235626157 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.UI.Text>()
#define GameObject_GetComponentsInChildren_TisText_t1901882714_m1445556921(__this, method) ((  TextU5BU5D_t422084607* (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m1982918030_gshared)(__this, method)
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2555686324  Color_get_black_m719512684 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m286683560 (Color_t2555686324 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoseScript::DelayGame()
extern "C"  RuntimeObject* LoseScript_DelayGame_m1373927896 (LoseScript_t212724369 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoseScript::ShowAdWhenReady()
extern "C"  RuntimeObject* LoseScript_ShowAdWhenReady_m2535654720 (LoseScript_t212724369 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C"  void Time_set_timeScale_m1127545364 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C"  GameObjectU5BU5D_t3328599146* GameObject_FindGameObjectsWithTag_m2585173894 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Contains(System.String)
extern "C"  bool String_Contains_m1147431944 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::set_pause(System.Boolean)
extern "C"  void AudioListener_set_pause_m2425921647 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C"  bool Behaviour_get_isActiveAndEnabled_m3143666263 (Behaviour_t1437897464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LoseScript/<DelayGame>c__Iterator0::.ctor()
extern "C"  void U3CDelayGameU3Ec__Iterator0__ctor_m604480062 (U3CDelayGameU3Ec__Iterator0_t4235577792 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LoseScript/<ShowAdWhenReady>c__Iterator1::.ctor()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1__ctor_m2670077801 (U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoseScript::HandleAnimation()
extern "C"  RuntimeObject* LoseScript_HandleAnimation_m1724406564 (LoseScript_t212724369 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LoseScript/<HandleAnimation>c__Iterator2::.ctor()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2__ctor_m403717454 (U3CHandleAnimationU3Ec__Iterator2_t2293152264 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_timeScale()
extern "C"  float Time_get_timeScale_m701790074 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Stop()
extern "C"  void AudioSource_Stop_m2682712816 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m3141794964 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1113636619_m3006960551(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Vector3_t3722313464 , Quaternion_t2301928331 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1135049463_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m2716693327 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TrippleTriggerMiddleScript::DelayGame()
extern "C"  RuntimeObject* TrippleTriggerMiddleScript_DelayGame_m2357289153 (TrippleTriggerMiddleScript_t4217752517 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TrippleTriggerMiddleScript::ShowAdWhenReady()
extern "C"  RuntimeObject* TrippleTriggerMiddleScript_ShowAdWhenReady_m3650831147 (TrippleTriggerMiddleScript_t4217752517 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::.ctor()
extern "C"  void U3CDelayGameU3Ec__Iterator0__ctor_m243069393 (U3CDelayGameU3Ec__Iterator0_t4237126612 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::.ctor()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1__ctor_m2848285895 (U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TrippleTriggerMiddleScript::HandleAnimation()
extern "C"  RuntimeObject* TrippleTriggerMiddleScript_HandleAnimation_m2765064618 (TrippleTriggerMiddleScript_t4217752517 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::.ctor()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2__ctor_m517399459 (U3CHandleAnimationU3Ec__Iterator2_t2078686267 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BallButtonControllerScript::.ctor()
extern "C"  void BallButtonControllerScript__ctor_m1249165316 (BallButtonControllerScript_t1117378164 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BallButtonControllerScript::Update()
extern "C"  void BallButtonControllerScript_Update_m1072157986 (BallButtonControllerScript_t1117378164 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BallButtonControllerScript_Update_m1072157986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3941174895, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral4002445229, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3941174895, L_2, /*hidden argument*/NULL);
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_3 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral6892883, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0053;
		}
	}
	{
		Animator_t434523843 * L_4 = Component_GetComponent_TisAnimator_t434523843_m2351447238(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t434523843_m2351447238_RuntimeMethod_var);
		__this->set_anim_2(L_4);
		Animator_t434523843 * L_5 = __this->get_anim_2();
		NullCheck(L_5);
		Animator_Play_m1697843332(L_5, _stringLiteral1235497039, /*hidden argument*/NULL);
	}

IL_0053:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_6 = CrossPlatformInputManager_GetButtonUp_m100471868(NULL /*static, unused*/, _stringLiteral6892883, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_007e;
		}
	}
	{
		Animator_t434523843 * L_7 = Component_GetComponent_TisAnimator_t434523843_m2351447238(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t434523843_m2351447238_RuntimeMethod_var);
		__this->set_anim_2(L_7);
		Animator_t434523843 * L_8 = __this->get_anim_2();
		NullCheck(L_8);
		Animator_Play_m1697843332(L_8, _stringLiteral4079641953, /*hidden argument*/NULL);
	}

IL_007e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_9 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral1385165106, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00a9;
		}
	}
	{
		Animator_t434523843 * L_10 = Component_GetComponent_TisAnimator_t434523843_m2351447238(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t434523843_m2351447238_RuntimeMethod_var);
		__this->set_anim_2(L_10);
		Animator_t434523843 * L_11 = __this->get_anim_2();
		NullCheck(L_11);
		Animator_Play_m1697843332(L_11, _stringLiteral996785950, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_12 = CrossPlatformInputManager_GetButtonUp_m100471868(NULL /*static, unused*/, _stringLiteral1385165106, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00d4;
		}
	}
	{
		Animator_t434523843 * L_13 = Component_GetComponent_TisAnimator_t434523843_m2351447238(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t434523843_m2351447238_RuntimeMethod_var);
		__this->set_anim_2(L_13);
		Animator_t434523843 * L_14 = __this->get_anim_2();
		NullCheck(L_14);
		Animator_Play_m1697843332(L_14, _stringLiteral3459875600, /*hidden argument*/NULL);
	}

IL_00d4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_15 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral2263706523, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00ff;
		}
	}
	{
		Animator_t434523843 * L_16 = Component_GetComponent_TisAnimator_t434523843_m2351447238(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t434523843_m2351447238_RuntimeMethod_var);
		__this->set_anim_2(L_16);
		Animator_t434523843 * L_17 = __this->get_anim_2();
		NullCheck(L_17);
		Animator_Play_m1697843332(L_17, _stringLiteral3842322295, /*hidden argument*/NULL);
	}

IL_00ff:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_18 = CrossPlatformInputManager_GetButtonUp_m100471868(NULL /*static, unused*/, _stringLiteral2263706523, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_012a;
		}
	}
	{
		Animator_t434523843 * L_19 = Component_GetComponent_TisAnimator_t434523843_m2351447238(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t434523843_m2351447238_RuntimeMethod_var);
		__this->set_anim_2(L_19);
		Animator_t434523843 * L_20 = __this->get_anim_2();
		NullCheck(L_20);
		Animator_Play_m1697843332(L_20, _stringLiteral1110670577, /*hidden argument*/NULL);
	}

IL_012a:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonControllerScript::.ctor()
extern "C"  void ButtonControllerScript__ctor_m814897415 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonControllerScript::Start()
extern "C"  void ButtonControllerScript_Start_m3772180747 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_Start_m3772180747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetButtonUp_m3204959434(NULL /*static, unused*/, _stringLiteral760301857, /*hidden argument*/NULL);
		ButtonControllerScript_checkSelectedSound_m3649248901(__this, /*hidden argument*/NULL);
		ButtonControllerScript_checkSelectedTheme_m1504630267(__this, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2473678957, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral2528740594, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral653748937, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral294264122, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonControllerScript::OnToggleValueChanged(System.Boolean)
extern "C"  void ButtonControllerScript_OnToggleValueChanged_m884945251 (ButtonControllerScript_t2432915718 * __this, bool ___isOn0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_OnToggleValueChanged_m884945251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___isOn0;
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3316871923, _stringLiteral3896413798, /*hidden argument*/NULL);
		goto IL_0029;
	}

IL_001a:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3316871923, _stringLiteral2448431576, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void ButtonControllerScript::checkSelectedSound()
extern "C"  void ButtonControllerScript_checkSelectedSound_m3649248901 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_checkSelectedSound_m3649248901_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral1785964129, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3075782012, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		V_0 = _stringLiteral1114791609;
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral1114791609, /*hidden argument*/NULL);
	}

IL_003b:
	{
		String_t* L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3075782012, _stringLiteral1114791609, /*hidden argument*/NULL);
	}

IL_005a:
	{
		String_t* L_8 = V_0;
		ButtonControllerScript_selectSound_m808880357(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonControllerScript::checkSelectedTheme()
extern "C"  void ButtonControllerScript_checkSelectedTheme_m1504630267 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_checkSelectedTheme_m1504630267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3359376429, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral404843312, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3359376429, _stringLiteral3790055497, /*hidden argument*/NULL);
		V_0 = _stringLiteral3790055497;
	}

IL_003b:
	{
		String_t* L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral186867486, _stringLiteral3790055497, /*hidden argument*/NULL);
	}

IL_005a:
	{
		String_t* L_8 = V_0;
		ButtonControllerScript_selectTheme_m4172231223(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonControllerScript::Update()
extern "C"  void ButtonControllerScript_Update_m4012678045 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_Update_m4012678045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	InitButtonsScript_t1987868624 * V_6 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_cnv_game_33();
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m1767405923(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		goto IL_087a;
	}

IL_0015:
	{
		GameObject_t1113636619 * L_2 = __this->get_cnv_menu_31();
		NullCheck(L_2);
		bool L_3 = GameObject_get_activeSelf_m1767405923(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_03dc;
		}
	}
	{
		GameObject_t1113636619 * L_4 = __this->get_go_permanent_34();
		NullCheck(L_4);
		InitTextsScript_t1053055510 * L_5 = GameObject_GetComponent_TisInitTextsScript_t1053055510_m1730174322(L_4, /*hidden argument*/GameObject_GetComponent_TisInitTextsScript_t1053055510_m1730174322_RuntimeMethod_var);
		NullCheck(L_5);
		InitTextsScript_UpdateTexts_m2560498976(L_5, /*hidden argument*/NULL);
		String_t* L_6 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3316871923, /*hidden argument*/NULL);
		__this->set_difficulty_49(L_6);
		Toggle_t2735377061 * L_7 = __this->get_btn_difficulty_48();
		NullCheck(L_7);
		ToggleEvent_t1873685584 * L_8 = L_7->get_onValueChanged_19();
		intptr_t L_9 = (intptr_t)ButtonControllerScript_OnToggleValueChanged_m884945251_RuntimeMethod_var;
		UnityAction_1_t682124106 * L_10 = (UnityAction_1_t682124106 *)il2cpp_codegen_object_new(UnityAction_1_t682124106_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3007623985(L_10, __this, L_9, /*hidden argument*/UnityAction_1__ctor_m3007623985_RuntimeMethod_var);
		NullCheck(L_8);
		UnityEvent_1_AddListener_m2847988282(L_8, L_10, /*hidden argument*/UnityEvent_1_AddListener_m2847988282_RuntimeMethod_var);
		String_t* L_11 = __this->get_difficulty_49();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m920492651(NULL /*static, unused*/, L_11, _stringLiteral3896413798, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_13 = __this->get_difficulty_49();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_15 = String_op_Equality_m920492651(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0105;
		}
	}

IL_008b:
	{
		Toggle_t2735377061 * L_16 = __this->get_btn_difficulty_48();
		NullCheck(L_16);
		Toggle_set_isOn_m3548357404(L_16, (bool)1, /*hidden argument*/NULL);
		Text_t1901882714 * L_17 = __this->get_textHighscoreCount_50();
		int32_t L_18 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral167811139, /*hidden argument*/NULL);
		V_0 = L_18;
		String_t* L_19 = Int32_ToString_m141394615((&V_0), /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_17, L_19);
		Text_t1901882714 * L_20 = __this->get_textPlayedCount_52();
		int32_t L_21 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral870646944, /*hidden argument*/NULL);
		V_1 = L_21;
		String_t* L_22 = Int32_ToString_m141394615((&V_1), /*hidden argument*/NULL);
		NullCheck(L_20);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_20, L_22);
		Text_t1901882714 * L_23 = __this->get_textLastPoints_54();
		int32_t L_24 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral2541657273, /*hidden argument*/NULL);
		V_2 = L_24;
		String_t* L_25 = Int32_ToString_m141394615((&V_2), /*hidden argument*/NULL);
		NullCheck(L_23);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_23, L_25);
		goto IL_0191;
	}

IL_0105:
	{
		String_t* L_26 = __this->get_difficulty_49();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_op_Equality_m920492651(NULL /*static, unused*/, L_26, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0191;
		}
	}
	{
		Toggle_t2735377061 * L_28 = __this->get_btn_difficulty_48();
		NullCheck(L_28);
		Toggle_set_isOn_m3548357404(L_28, (bool)0, /*hidden argument*/NULL);
		Text_t1901882714 * L_29 = __this->get_textHighscoreCount_50();
		int32_t L_30 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral2722989498, /*hidden argument*/NULL);
		V_3 = L_30;
		String_t* L_31 = Int32_ToString_m141394615((&V_3), /*hidden argument*/NULL);
		NullCheck(L_29);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_29, L_31);
		Text_t1901882714 * L_32 = __this->get_textPlayedCount_52();
		int32_t L_33 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral388954149, /*hidden argument*/NULL);
		V_4 = L_33;
		String_t* L_34 = Int32_ToString_m141394615((&V_4), /*hidden argument*/NULL);
		NullCheck(L_32);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_32, L_34);
		Text_t1901882714 * L_35 = __this->get_textLastPoints_54();
		int32_t L_36 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3681683422, /*hidden argument*/NULL);
		V_5 = L_36;
		String_t* L_37 = Int32_ToString_m141394615((&V_5), /*hidden argument*/NULL);
		NullCheck(L_35);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_35, L_37);
	}

IL_0191:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_38 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral760301857, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_023b;
		}
	}
	{
		int32_t L_39 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral2473678957, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2473678957, L_39, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3046124294, 0, /*hidden argument*/NULL);
		Text_t1901882714 * L_40 = __this->get_scoreText_37();
		NullCheck(L_40);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_40, _stringLiteral3452614544);
		ButtonControllerScript_resetGravity_m3471744663(__this, /*hidden argument*/NULL);
		int32_t L_41 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		__this->set_specialPoints_42(L_41);
		Text_t1901882714 * L_42 = __this->get_specialPointsText_38();
		int32_t* L_43 = __this->get_address_of_specialPoints_42();
		String_t* L_44 = Int32_ToString_m141394615(L_43, /*hidden argument*/NULL);
		NullCheck(L_42);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_42, L_44);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetButtonUp_m3204959434(NULL /*static, unused*/, _stringLiteral760301857, /*hidden argument*/NULL);
		CrossPlatformInputManager_SetButtonUp_m3204959434(NULL /*static, unused*/, _stringLiteral6892883, /*hidden argument*/NULL);
		CrossPlatformInputManager_SetButtonUp_m3204959434(NULL /*static, unused*/, _stringLiteral1385165106, /*hidden argument*/NULL);
		CrossPlatformInputManager_SetButtonUp_m3204959434(NULL /*static, unused*/, _stringLiteral2263706523, /*hidden argument*/NULL);
		RuntimeObject* L_45 = ButtonControllerScript_DelayGameStart_m2227959488(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_45, /*hidden argument*/NULL);
		goto IL_03d7;
	}

IL_023b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_46 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral2432068731, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0267;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		Advertisement_Initialize_m1051450402(NULL /*static, unused*/, _stringLiteral3014706548, (bool)1, /*hidden argument*/NULL);
		RuntimeObject* L_47 = ButtonControllerScript_ShowAdWhenReady_m2072552498(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_47, /*hidden argument*/NULL);
		goto IL_03d7;
	}

IL_0267:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_48 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral4224754453, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_02b3;
		}
	}
	{
		GameObject_t1113636619 * L_49 = __this->get_cnv_settings_32();
		NullCheck(L_49);
		GameObject_SetActive_m796801857(L_49, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_50 = __this->get_cnv_game_33();
		NullCheck(L_50);
		GameObject_SetActive_m796801857(L_50, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_51 = __this->get_cnv_menu_31();
		NullCheck(L_51);
		GameObject_SetActive_m796801857(L_51, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_52 = __this->get_cnv_settings_32();
		NullCheck(L_52);
		InitButtonsScript_t1987868624 * L_53 = GameObject_GetComponentInChildren_TisInitButtonsScript_t1987868624_m2265097840(L_52, /*hidden argument*/GameObject_GetComponentInChildren_TisInitButtonsScript_t1987868624_m2265097840_RuntimeMethod_var);
		V_6 = L_53;
		InitButtonsScript_t1987868624 * L_54 = V_6;
		NullCheck(L_54);
		InitButtonsScript_InitButtons_m1101143926(L_54, /*hidden argument*/NULL);
		goto IL_03d7;
	}

IL_02b3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_55 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral441598207, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0314;
		}
	}
	{
		GameObject_t1113636619 * L_56 = __this->get_btn_reset_15();
		NullCheck(L_56);
		GameObject_SetActive_m796801857(L_56, (bool)0, /*hidden argument*/NULL);
		Toggle_t2735377061 * L_57 = __this->get_btn_difficulty_48();
		NullCheck(L_57);
		GameObject_t1113636619 * L_58 = Component_get_gameObject_m442555142(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		GameObject_SetActive_m796801857(L_58, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_59 = __this->get_txt_reset_16();
		NullCheck(L_59);
		GameObject_SetActive_m796801857(L_59, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_60 = __this->get_btn_answers_17();
		NullCheck(L_60);
		GameObject_SetActive_m796801857(L_60, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_61 = __this->get_btn_video_13();
		NullCheck(L_61);
		GameObject_SetActive_m796801857(L_61, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_62 = __this->get_btn_setting_14();
		NullCheck(L_62);
		GameObject_SetActive_m796801857(L_62, (bool)0, /*hidden argument*/NULL);
		goto IL_03d7;
	}

IL_0314:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_63 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral4058834769, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_037b;
		}
	}
	{
		GameObject_t1113636619 * L_64 = __this->get_btn_reset_15();
		NullCheck(L_64);
		GameObject_SetActive_m796801857(L_64, (bool)1, /*hidden argument*/NULL);
		Toggle_t2735377061 * L_65 = __this->get_btn_difficulty_48();
		NullCheck(L_65);
		GameObject_t1113636619 * L_66 = Component_get_gameObject_m442555142(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		GameObject_SetActive_m796801857(L_66, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_67 = __this->get_txt_reset_16();
		NullCheck(L_67);
		GameObject_SetActive_m796801857(L_67, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_68 = __this->get_btn_answers_17();
		NullCheck(L_68);
		GameObject_SetActive_m796801857(L_68, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_69 = __this->get_btn_video_13();
		NullCheck(L_69);
		GameObject_SetActive_m796801857(L_69, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_70 = __this->get_btn_setting_14();
		NullCheck(L_70);
		GameObject_SetActive_m796801857(L_70, (bool)1, /*hidden argument*/NULL);
		ButtonControllerScript_resetThings_m3684079304(__this, /*hidden argument*/NULL);
		goto IL_03d7;
	}

IL_037b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_71 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral405390407, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_03d7;
		}
	}
	{
		GameObject_t1113636619 * L_72 = __this->get_btn_reset_15();
		NullCheck(L_72);
		GameObject_SetActive_m796801857(L_72, (bool)1, /*hidden argument*/NULL);
		Toggle_t2735377061 * L_73 = __this->get_btn_difficulty_48();
		NullCheck(L_73);
		GameObject_t1113636619 * L_74 = Component_get_gameObject_m442555142(L_73, /*hidden argument*/NULL);
		NullCheck(L_74);
		GameObject_SetActive_m796801857(L_74, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_75 = __this->get_txt_reset_16();
		NullCheck(L_75);
		GameObject_SetActive_m796801857(L_75, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_76 = __this->get_btn_answers_17();
		NullCheck(L_76);
		GameObject_SetActive_m796801857(L_76, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_77 = __this->get_btn_video_13();
		NullCheck(L_77);
		GameObject_SetActive_m796801857(L_77, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_78 = __this->get_btn_setting_14();
		NullCheck(L_78);
		GameObject_SetActive_m796801857(L_78, (bool)1, /*hidden argument*/NULL);
	}

IL_03d7:
	{
		goto IL_087a;
	}

IL_03dc:
	{
		GameObject_t1113636619 * L_79 = __this->get_cnv_settings_32();
		NullCheck(L_79);
		bool L_80 = GameObject_get_activeSelf_m1767405923(L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_087a;
		}
	}
	{
		String_t* L_81 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral4011269308, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_82 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_83 = String_op_Equality_m920492651(NULL /*static, unused*/, L_81, L_82, /*hidden argument*/NULL);
		if (!L_83)
		{
			goto IL_0415;
		}
	}
	{
		String_t* L_84 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral1785964129, /*hidden argument*/NULL);
		ButtonControllerScript_selectSound_m808880357(__this, L_84, /*hidden argument*/NULL);
	}

IL_0415:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_85 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral2777363045, /*hidden argument*/NULL);
		if (!L_85)
		{
			goto IL_04a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_86 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3574414313, L_86, /*hidden argument*/NULL);
		String_t* L_87 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral4273456759, L_87, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_88 = __this->get_cnv_game_33();
		NullCheck(L_88);
		GameObject_SetActive_m796801857(L_88, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_89 = __this->get_cnv_menu_31();
		NullCheck(L_89);
		GameObject_SetActive_m796801857(L_89, (bool)1, /*hidden argument*/NULL);
		ScrollRect_t4137855814 * L_90 = __this->get_svSounds_66();
		NullCheck(L_90);
		ScrollRect_set_verticalNormalizedPosition_m1452826170(L_90, (1.0f), /*hidden argument*/NULL);
		ScrollRect_t4137855814 * L_91 = __this->get_svThemes_67();
		NullCheck(L_91);
		ScrollRect_set_verticalNormalizedPosition_m1452826170(L_91, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_92 = __this->get_cnv_settings_32();
		NullCheck(L_92);
		GameObject_SetActive_m796801857(L_92, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_93 = __this->get_btn_buy_24();
		NullCheck(L_93);
		GameObject_SetActive_m796801857(L_93, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_94 = __this->get_text_buy_35();
		NullCheck(L_94);
		GameObject_SetActive_m796801857(L_94, (bool)0, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_04a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_95 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral816225280, /*hidden argument*/NULL);
		if (!L_95)
		{
			goto IL_04d7;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral404843312, _stringLiteral816225280, /*hidden argument*/NULL);
		ButtonControllerScript_selectTheme_m4172231223(__this, _stringLiteral3790055497, /*hidden argument*/NULL);
		ButtonControllerScript_selectButton_m619056489(__this, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_04d7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_96 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral3921657742, /*hidden argument*/NULL);
		if (!L_96)
		{
			goto IL_050b;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral404843312, _stringLiteral3921657742, /*hidden argument*/NULL);
		ButtonControllerScript_selectTheme_m4172231223(__this, _stringLiteral2699831051, /*hidden argument*/NULL);
		ButtonControllerScript_selectButton_m619056489(__this, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_050b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_97 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral1559865961, /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_053f;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral404843312, _stringLiteral1559865961, /*hidden argument*/NULL);
		ButtonControllerScript_selectTheme_m4172231223(__this, _stringLiteral3775034288, /*hidden argument*/NULL);
		ButtonControllerScript_selectButton_m619056489(__this, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_053f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_98 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral681917307, /*hidden argument*/NULL);
		if (!L_98)
		{
			goto IL_0573;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral404843312, _stringLiteral681917307, /*hidden argument*/NULL);
		ButtonControllerScript_selectTheme_m4172231223(__this, _stringLiteral166522051, /*hidden argument*/NULL);
		ButtonControllerScript_selectButton_m619056489(__this, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_0573:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_99 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral1938886732, /*hidden argument*/NULL);
		if (!L_99)
		{
			goto IL_05c5;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3075782012, _stringLiteral1938886732, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral2074989104, /*hidden argument*/NULL);
		ButtonControllerScript_selectSound_m808880357(__this, _stringLiteral2074989104, /*hidden argument*/NULL);
		ButtonControllerScript_selectButton_m619056489(__this, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral4273456759, _stringLiteral4002445229, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_05c5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_100 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral2070893366, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_0617;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3075782012, _stringLiteral2070893366, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral1114791609, /*hidden argument*/NULL);
		ButtonControllerScript_selectSound_m808880357(__this, _stringLiteral1114791609, /*hidden argument*/NULL);
		ButtonControllerScript_selectButton_m619056489(__this, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral4273456759, _stringLiteral4002445229, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_0617:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_101 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral2785512304, /*hidden argument*/NULL);
		if (!L_101)
		{
			goto IL_0669;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3075782012, _stringLiteral2785512304, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral747299879, /*hidden argument*/NULL);
		ButtonControllerScript_selectSound_m808880357(__this, _stringLiteral747299879, /*hidden argument*/NULL);
		ButtonControllerScript_selectButton_m619056489(__this, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral4273456759, _stringLiteral4002445229, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_0669:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_102 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral2895553709, /*hidden argument*/NULL);
		if (!L_102)
		{
			goto IL_06bb;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3075782012, _stringLiteral2895553709, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral3654983690, /*hidden argument*/NULL);
		ButtonControllerScript_selectSound_m808880357(__this, _stringLiteral3654983690, /*hidden argument*/NULL);
		ButtonControllerScript_selectButton_m619056489(__this, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral4273456759, _stringLiteral4002445229, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_06bb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_103 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral3593808015, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_070d;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3075782012, _stringLiteral3593808015, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral3163906174, /*hidden argument*/NULL);
		ButtonControllerScript_selectSound_m808880357(__this, _stringLiteral3163906174, /*hidden argument*/NULL);
		ButtonControllerScript_selectButton_m619056489(__this, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral4273456759, _stringLiteral4002445229, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_070d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_104 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral3060255191, /*hidden argument*/NULL);
		if (!L_104)
		{
			goto IL_074c;
		}
	}
	{
		int32_t L_105 = __this->get_nightThemeCost_25();
		ButtonControllerScript_checkBuyButtonToShow_m3246718299(__this, L_105, _stringLiteral3921657742, /*hidden argument*/NULL);
		ButtonControllerScript_changeTheme_m1190884257(__this, _stringLiteral2699831051, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3574414313, _stringLiteral4002445229, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_074c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_106 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral4130524952, /*hidden argument*/NULL);
		if (!L_106)
		{
			goto IL_078b;
		}
	}
	{
		int32_t L_107 = __this->get_flappyThemeCost_26();
		ButtonControllerScript_checkBuyButtonToShow_m3246718299(__this, L_107, _stringLiteral1559865961, /*hidden argument*/NULL);
		ButtonControllerScript_changeTheme_m1190884257(__this, _stringLiteral3775034288, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3574414313, _stringLiteral4002445229, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_078b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_108 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral2687637966, /*hidden argument*/NULL);
		if (!L_108)
		{
			goto IL_07ca;
		}
	}
	{
		int32_t L_109 = __this->get_desertThemeCost_27();
		ButtonControllerScript_checkBuyButtonToShow_m3246718299(__this, L_109, _stringLiteral681917307, /*hidden argument*/NULL);
		ButtonControllerScript_changeTheme_m1190884257(__this, _stringLiteral166522051, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3574414313, _stringLiteral4002445229, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_07ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_110 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral2272329368, /*hidden argument*/NULL);
		if (!L_110)
		{
			goto IL_0805;
		}
	}
	{
		int32_t L_111 = __this->get_skyflightSoundCost_28();
		ButtonControllerScript_checkBuyButtonToShow_m3246718299(__this, L_111, _stringLiteral2895553709, /*hidden argument*/NULL);
		ButtonControllerScript_selectSound_m808880357(__this, _stringLiteral1790728806, /*hidden argument*/NULL);
		ButtonControllerScript_playPreviewSound_m1032544213(__this, _stringLiteral3654983690, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_0805:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_112 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral4005421059, /*hidden argument*/NULL);
		if (!L_112)
		{
			goto IL_0835;
		}
	}
	{
		int32_t L_113 = __this->get_dankstormSoundCost_29();
		ButtonControllerScript_checkBuyButtonToShow_m3246718299(__this, L_113, _stringLiteral2785512304, /*hidden argument*/NULL);
		ButtonControllerScript_playPreviewSound_m1032544213(__this, _stringLiteral747299879, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_0835:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_114 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral2378241666, /*hidden argument*/NULL);
		if (!L_114)
		{
			goto IL_0865;
		}
	}
	{
		int32_t L_115 = __this->get_egyptSoundCost_30();
		ButtonControllerScript_checkBuyButtonToShow_m3246718299(__this, L_115, _stringLiteral3593808015, /*hidden argument*/NULL);
		ButtonControllerScript_playPreviewSound_m1032544213(__this, _stringLiteral3163906174, /*hidden argument*/NULL);
		goto IL_087a;
	}

IL_0865:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t191731427_il2cpp_TypeInfo_var);
		bool L_116 = CrossPlatformInputManager_GetButtonDown_m1689635996(NULL /*static, unused*/, _stringLiteral1467627447, /*hidden argument*/NULL);
		if (!L_116)
		{
			goto IL_087a;
		}
	}
	{
		ButtonControllerScript_performPurchase_m2064459992(__this, /*hidden argument*/NULL);
	}

IL_087a:
	{
		return;
	}
}
// System.Void ButtonControllerScript::FixedUpdate()
extern "C"  void ButtonControllerScript_FixedUpdate_m2503950570 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_FixedUpdate_m2503950570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSource_t3935305588 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_audioSource_2();
		NullCheck(L_0);
		AudioSource_t3935305588 * L_1 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		V_0 = L_1;
		String_t* L_2 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral4011269308, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, _stringLiteral4002445229, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		AudioSource_t3935305588 * L_4 = V_0;
		NullCheck(L_4);
		float L_5 = AudioSource_get_time_m2174765632(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_previewTime_47();
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral4011269308, L_7, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// System.Void ButtonControllerScript::resetThings()
extern "C"  void ButtonControllerScript_resetThings_m3684079304 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_resetThings_m3684079304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral167811139, 0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral870646944, 0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2541657273, 0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2722989498, 0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral388954149, 0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3681683422, 0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = __this->get_go_permanent_34();
		NullCheck(L_0);
		InitTextsScript_t1053055510 * L_1 = GameObject_GetComponent_TisInitTextsScript_t1053055510_m1730174322(L_0, /*hidden argument*/GameObject_GetComponent_TisInitTextsScript_t1053055510_m1730174322_RuntimeMethod_var);
		NullCheck(L_1);
		InitTextsScript_UpdateTexts_m2560498976(L_1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_go_permanent_34();
		NullCheck(L_2);
		InitButtonsScript_t1987868624 * L_3 = GameObject_GetComponent_TisInitButtonsScript_t1987868624_m198946856(L_2, /*hidden argument*/GameObject_GetComponent_TisInitButtonsScript_t1987868624_m198946856_RuntimeMethod_var);
		NullCheck(L_3);
		InitButtonsScript_InitButtons_m1101143926(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ButtonControllerScript::DelayGameStart()
extern "C"  RuntimeObject* ButtonControllerScript_DelayGameStart_m2227959488 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_DelayGameStart_m2227959488_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDelayGameStartU3Ec__Iterator0_t3856269498 * V_0 = NULL;
	{
		U3CDelayGameStartU3Ec__Iterator0_t3856269498 * L_0 = (U3CDelayGameStartU3Ec__Iterator0_t3856269498 *)il2cpp_codegen_object_new(U3CDelayGameStartU3Ec__Iterator0_t3856269498_il2cpp_TypeInfo_var);
		U3CDelayGameStartU3Ec__Iterator0__ctor_m3486972112(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDelayGameStartU3Ec__Iterator0_t3856269498 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CDelayGameStartU3Ec__Iterator0_t3856269498 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ButtonControllerScript::checkBuyButtonToShow(System.Int32,System.String)
extern "C"  void ButtonControllerScript_checkBuyButtonToShow_m3246718299 (ButtonControllerScript_t2432915718 * __this, int32_t ___cost0, String_t* ___buttonName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_checkBuyButtonToShow_m3246718299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObject_t1113636619 * L_0 = __this->get_text_buy_35();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_btn_buy_24();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		Text_t1901882714 * L_2 = __this->get_txt_cost_36();
		int32_t L_3 = ___cost0;
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2753579780, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_6);
		int32_t L_7 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		V_0 = L_7;
		int32_t L_8 = V_0;
		int32_t L_9 = ___cost0;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0067;
		}
	}
	{
		GameObject_t1113636619 * L_10 = __this->get_btn_buy_24();
		NullCheck(L_10);
		GameObject_SetActive_m796801857(L_10, (bool)1, /*hidden argument*/NULL);
		int32_t L_11 = ___cost0;
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3941174888, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ___buttonName1;
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1301175002, L_12, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Void ButtonControllerScript::performPurchase()
extern "C"  void ButtonControllerScript_performPurchase_m2064459992 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_performPurchase_m2064459992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	GameObject_t1113636619 * V_3 = NULL;
	int32_t V_4 = 0;
	{
		int32_t L_0 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3941174888, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral1301175002, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3831740680, ((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)L_4)), /*hidden argument*/NULL);
		int32_t L_5 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		V_1 = L_5;
		Text_t1901882714 * L_6 = __this->get_specialPointsText_38();
		String_t* L_7 = Int32_ToString_m141394615((&V_1), /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_7);
		String_t* L_8 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3937257545(NULL /*static, unused*/, L_8, _stringLiteral1228557250, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_10 = GameObject_Find_m2032535176(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		GameObject_t1113636619 * L_11 = V_3;
		NullCheck(L_11);
		GameObject_SetActive_m796801857(L_11, (bool)0, /*hidden argument*/NULL);
		String_t* L_12 = V_2;
		String_t* L_13 = String_Concat_m3937257545(NULL /*static, unused*/, L_12, _stringLiteral1228557250, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, L_13, _stringLiteral3875954633, /*hidden argument*/NULL);
		String_t* L_14 = V_2;
		String_t* L_15 = V_2;
		NullCheck(L_15);
		int32_t L_16 = String_get_Length_m3847582255(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_17 = String_Substring_m2848979100(L_14, ((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)5)), /*hidden argument*/NULL);
		bool L_18 = String_op_Equality_m920492651(NULL /*static, unused*/, L_17, _stringLiteral773881782, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0156;
		}
	}
	{
		String_t* L_19 = V_2;
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral404843312, L_19, /*hidden argument*/NULL);
		String_t* L_20 = V_2;
		if (!L_20)
		{
			goto IL_0141;
		}
	}
	{
		String_t* L_21 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m920492651(NULL /*static, unused*/, L_21, _stringLiteral816225280, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00f1;
		}
	}
	{
		String_t* L_23 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_op_Equality_m920492651(NULL /*static, unused*/, L_23, _stringLiteral1559865961, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_0105;
		}
	}
	{
		String_t* L_25 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_op_Equality_m920492651(NULL /*static, unused*/, L_25, _stringLiteral3921657742, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_0119;
		}
	}
	{
		String_t* L_27 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_28 = String_op_Equality_m920492651(NULL /*static, unused*/, L_27, _stringLiteral681917307, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_012d;
		}
	}
	{
		goto IL_0141;
	}

IL_00f1:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3359376429, _stringLiteral3790055497, /*hidden argument*/NULL);
		goto IL_0141;
	}

IL_0105:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3359376429, _stringLiteral3775034288, /*hidden argument*/NULL);
		goto IL_0141;
	}

IL_0119:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3359376429, _stringLiteral2699831051, /*hidden argument*/NULL);
		goto IL_0141;
	}

IL_012d:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3359376429, _stringLiteral166522051, /*hidden argument*/NULL);
		goto IL_0141;
	}

IL_0141:
	{
		String_t* L_29 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3359376429, /*hidden argument*/NULL);
		ButtonControllerScript_selectTheme_m4172231223(__this, L_29, /*hidden argument*/NULL);
		goto IL_0271;
	}

IL_0156:
	{
		String_t* L_30 = V_2;
		String_t* L_31 = V_2;
		NullCheck(L_31);
		int32_t L_32 = String_get_Length_m3847582255(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_33 = String_Substring_m2848979100(L_30, ((int32_t)il2cpp_codegen_subtract((int32_t)L_32, (int32_t)5)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_34 = String_op_Equality_m920492651(NULL /*static, unused*/, L_33, _stringLiteral2002597352, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0271;
		}
	}
	{
		String_t* L_35 = V_2;
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3075782012, L_35, /*hidden argument*/NULL);
		String_t* L_36 = V_2;
		if (!L_36)
		{
			goto IL_023d;
		}
	}
	{
		String_t* L_37 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_38 = String_op_Equality_m920492651(NULL /*static, unused*/, L_37, _stringLiteral1938886732, /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_01d9;
		}
	}
	{
		String_t* L_39 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_40 = String_op_Equality_m920492651(NULL /*static, unused*/, L_39, _stringLiteral2070893366, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_01ed;
		}
	}
	{
		String_t* L_41 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_42 = String_op_Equality_m920492651(NULL /*static, unused*/, L_41, _stringLiteral2785512304, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_0201;
		}
	}
	{
		String_t* L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_44 = String_op_Equality_m920492651(NULL /*static, unused*/, L_43, _stringLiteral2895553709, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_0215;
		}
	}
	{
		String_t* L_45 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_op_Equality_m920492651(NULL /*static, unused*/, L_45, _stringLiteral3593808015, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_0229;
		}
	}
	{
		goto IL_023d;
	}

IL_01d9:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral2074989104, /*hidden argument*/NULL);
		goto IL_023d;
	}

IL_01ed:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral1114791609, /*hidden argument*/NULL);
		goto IL_023d;
	}

IL_0201:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral747299879, /*hidden argument*/NULL);
		goto IL_023d;
	}

IL_0215:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral3654983690, /*hidden argument*/NULL);
		goto IL_023d;
	}

IL_0229:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral1785964129, _stringLiteral3163906174, /*hidden argument*/NULL);
		goto IL_023d;
	}

IL_023d:
	{
		String_t* L_47 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral1785964129, /*hidden argument*/NULL);
		ButtonControllerScript_selectSound_m808880357(__this, L_47, /*hidden argument*/NULL);
		Text_t1901882714 * L_48 = __this->get_specialPointsText_38();
		int32_t L_49 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		V_4 = L_49;
		String_t* L_50 = Int32_ToString_m141394615((&V_4), /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_48, L_50);
	}

IL_0271:
	{
		ButtonControllerScript_selectButton_m619056489(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonControllerScript::selectButton()
extern "C"  void ButtonControllerScript_selectButton_m619056489 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_selectButton_m619056489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_text_buy_35();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_btn_buy_24();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		String_t* L_2 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral404843312, /*hidden argument*/NULL);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3574414313, L_3, /*hidden argument*/NULL);
		String_t* L_4 = V_0;
		if (!L_4)
		{
			goto IL_00ed;
		}
	}
	{
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, _stringLiteral816225280, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_007d;
		}
	}
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_7, _stringLiteral3921657742, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0099;
		}
	}
	{
		String_t* L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_9, _stringLiteral1559865961, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_00b5;
		}
	}
	{
		String_t* L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m920492651(NULL /*static, unused*/, L_11, _stringLiteral681917307, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00d1;
		}
	}
	{
		goto IL_00ed;
	}

IL_007d:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral772767670, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_13 = __this->get_ckm_mainTheme_56();
		NullCheck(L_13);
		GameObject_SetActive_m796801857(L_13, (bool)1, /*hidden argument*/NULL);
		goto IL_0109;
	}

IL_0099:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral772767670, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_14 = __this->get_ckm_nightTheme_57();
		NullCheck(L_14);
		GameObject_SetActive_m796801857(L_14, (bool)1, /*hidden argument*/NULL);
		goto IL_0109;
	}

IL_00b5:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral772767670, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_15 = __this->get_ckm_flappyTheme_58();
		NullCheck(L_15);
		GameObject_SetActive_m796801857(L_15, (bool)1, /*hidden argument*/NULL);
		goto IL_0109;
	}

IL_00d1:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral772767670, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_16 = __this->get_ckm_desertTheme_59();
		NullCheck(L_16);
		GameObject_SetActive_m796801857(L_16, (bool)1, /*hidden argument*/NULL);
		goto IL_0109;
	}

IL_00ed:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral772767670, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_17 = __this->get_ckm_mainTheme_56();
		NullCheck(L_17);
		GameObject_SetActive_m796801857(L_17, (bool)1, /*hidden argument*/NULL);
		goto IL_0109;
	}

IL_0109:
	{
		String_t* L_18 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3075782012, /*hidden argument*/NULL);
		V_1 = L_18;
		String_t* L_19 = V_1;
		if (!L_19)
		{
			goto IL_01fb;
		}
	}
	{
		String_t* L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_op_Equality_m920492651(NULL /*static, unused*/, L_20, _stringLiteral1938886732, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_016f;
		}
	}
	{
		String_t* L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m920492651(NULL /*static, unused*/, L_22, _stringLiteral2070893366, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_018b;
		}
	}
	{
		String_t* L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m920492651(NULL /*static, unused*/, L_24, _stringLiteral2895553709, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_01a7;
		}
	}
	{
		String_t* L_26 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_op_Equality_m920492651(NULL /*static, unused*/, L_26, _stringLiteral2785512304, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_01c3;
		}
	}
	{
		String_t* L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Equality_m920492651(NULL /*static, unused*/, L_28, _stringLiteral3593808015, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_01df;
		}
	}
	{
		goto IL_01fb;
	}

IL_016f:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral1999255016, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_30 = __this->get_ckm_noSound_60();
		NullCheck(L_30);
		GameObject_SetActive_m796801857(L_30, (bool)1, /*hidden argument*/NULL);
		goto IL_0217;
	}

IL_018b:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral1999255016, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_31 = __this->get_ckm_mainSound_61();
		NullCheck(L_31);
		GameObject_SetActive_m796801857(L_31, (bool)1, /*hidden argument*/NULL);
		goto IL_0217;
	}

IL_01a7:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral1999255016, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_32 = __this->get_ckm_skyflightSound_62();
		NullCheck(L_32);
		GameObject_SetActive_m796801857(L_32, (bool)1, /*hidden argument*/NULL);
		goto IL_0217;
	}

IL_01c3:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral1999255016, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_33 = __this->get_ckm_dankstormSound_63();
		NullCheck(L_33);
		GameObject_SetActive_m796801857(L_33, (bool)1, /*hidden argument*/NULL);
		goto IL_0217;
	}

IL_01df:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral1999255016, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_34 = __this->get_ckm_egyptSound_64();
		NullCheck(L_34);
		GameObject_SetActive_m796801857(L_34, (bool)1, /*hidden argument*/NULL);
		goto IL_0217;
	}

IL_01fb:
	{
		ButtonControllerScript_disableCheckmarks_m343318825(__this, _stringLiteral1999255016, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_35 = __this->get_ckm_mainSound_61();
		NullCheck(L_35);
		GameObject_SetActive_m796801857(L_35, (bool)1, /*hidden argument*/NULL);
		goto IL_0217;
	}

IL_0217:
	{
		return;
	}
}
// System.Void ButtonControllerScript::disableCheckmarks(System.String)
extern "C"  void ButtonControllerScript_disableCheckmarks_m343318825 (ButtonControllerScript_t2432915718 * __this, String_t* ___category0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_disableCheckmarks_m343318825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___category0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral772767670, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0045;
		}
	}
	{
		GameObject_t1113636619 * L_2 = __this->get_ckm_mainTheme_56();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_ckm_nightTheme_57();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = __this->get_ckm_flappyTheme_58();
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = __this->get_ckm_desertTheme_59();
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)0, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_0045:
	{
		String_t* L_6 = ___category0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, _stringLiteral1999255016, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0091;
		}
	}
	{
		GameObject_t1113636619 * L_8 = __this->get_ckm_noSound_60();
		NullCheck(L_8);
		GameObject_SetActive_m796801857(L_8, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = __this->get_ckm_mainSound_61();
		NullCheck(L_9);
		GameObject_SetActive_m796801857(L_9, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_10 = __this->get_ckm_skyflightSound_62();
		NullCheck(L_10);
		GameObject_SetActive_m796801857(L_10, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_11 = __this->get_ckm_dankstormSound_63();
		NullCheck(L_11);
		GameObject_SetActive_m796801857(L_11, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_12 = __this->get_ckm_egyptSound_64();
		NullCheck(L_12);
		GameObject_SetActive_m796801857(L_12, (bool)0, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void ButtonControllerScript::selectSound(System.String)
extern "C"  void ButtonControllerScript_selectSound_m808880357 (ButtonControllerScript_t2432915718 * __this, String_t* ___sound0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_selectSound_m808880357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSource_t3935305588 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_audioSource_2();
		NullCheck(L_0);
		AudioSource_t3935305588 * L_1 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		V_0 = L_1;
		String_t* L_2 = ___sound0;
		if (!L_2)
		{
			goto IL_019c;
		}
	}
	{
		String_t* L_3 = ___sound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral2074989104, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_5 = ___sound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, _stringLiteral1114791609, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0078;
		}
	}
	{
		String_t* L_7 = ___sound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_7, _stringLiteral747299879, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_00c1;
		}
	}
	{
		String_t* L_9 = ___sound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_9, _stringLiteral3654983690, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_010a;
		}
	}
	{
		String_t* L_11 = ___sound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m920492651(NULL /*static, unused*/, L_11, _stringLiteral3163906174, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0153;
		}
	}
	{
		goto IL_019c;
	}

IL_0067:
	{
		GameObject_t1113636619 * L_13 = __this->get_audioSource_2();
		NullCheck(L_13);
		GameObject_SetActive_m796801857(L_13, (bool)0, /*hidden argument*/NULL);
		goto IL_019c;
	}

IL_0078:
	{
		GameObject_t1113636619 * L_14 = __this->get_audioSource_2();
		NullCheck(L_14);
		bool L_15 = GameObject_get_activeSelf_m1767405923(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0094;
		}
	}
	{
		GameObject_t1113636619 * L_16 = __this->get_audioSource_2();
		NullCheck(L_16);
		GameObject_SetActive_m796801857(L_16, (bool)1, /*hidden argument*/NULL);
	}

IL_0094:
	{
		AudioSource_t3935305588 * L_17 = V_0;
		NullCheck(L_17);
		AudioClip_t3680889665 * L_18 = AudioSource_get_clip_m1234340632(L_17, /*hidden argument*/NULL);
		AudioClip_t3680889665 * L_19 = __this->get_clip_mainSound_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00bc;
		}
	}
	{
		AudioSource_t3935305588 * L_21 = V_0;
		AudioClip_t3680889665 * L_22 = __this->get_clip_mainSound_3();
		NullCheck(L_21);
		AudioSource_set_clip_m31653938(L_21, L_22, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_23 = V_0;
		NullCheck(L_23);
		AudioSource_Play_m48294159(L_23, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		goto IL_019c;
	}

IL_00c1:
	{
		GameObject_t1113636619 * L_24 = __this->get_audioSource_2();
		NullCheck(L_24);
		bool L_25 = GameObject_get_activeSelf_m1767405923(L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00dd;
		}
	}
	{
		GameObject_t1113636619 * L_26 = __this->get_audioSource_2();
		NullCheck(L_26);
		GameObject_SetActive_m796801857(L_26, (bool)1, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		AudioSource_t3935305588 * L_27 = V_0;
		NullCheck(L_27);
		AudioClip_t3680889665 * L_28 = AudioSource_get_clip_m1234340632(L_27, /*hidden argument*/NULL);
		AudioClip_t3680889665 * L_29 = __this->get_clip_dankstormSound_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0105;
		}
	}
	{
		AudioSource_t3935305588 * L_31 = V_0;
		AudioClip_t3680889665 * L_32 = __this->get_clip_dankstormSound_4();
		NullCheck(L_31);
		AudioSource_set_clip_m31653938(L_31, L_32, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_33 = V_0;
		NullCheck(L_33);
		AudioSource_Play_m48294159(L_33, /*hidden argument*/NULL);
	}

IL_0105:
	{
		goto IL_019c;
	}

IL_010a:
	{
		GameObject_t1113636619 * L_34 = __this->get_audioSource_2();
		NullCheck(L_34);
		bool L_35 = GameObject_get_activeSelf_m1767405923(L_34, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_0126;
		}
	}
	{
		GameObject_t1113636619 * L_36 = __this->get_audioSource_2();
		NullCheck(L_36);
		GameObject_SetActive_m796801857(L_36, (bool)1, /*hidden argument*/NULL);
	}

IL_0126:
	{
		AudioSource_t3935305588 * L_37 = V_0;
		NullCheck(L_37);
		AudioClip_t3680889665 * L_38 = AudioSource_get_clip_m1234340632(L_37, /*hidden argument*/NULL);
		AudioClip_t3680889665 * L_39 = __this->get_clip_skyflightSound_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_40 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_014e;
		}
	}
	{
		AudioSource_t3935305588 * L_41 = V_0;
		AudioClip_t3680889665 * L_42 = __this->get_clip_skyflightSound_5();
		NullCheck(L_41);
		AudioSource_set_clip_m31653938(L_41, L_42, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_43 = V_0;
		NullCheck(L_43);
		AudioSource_Play_m48294159(L_43, /*hidden argument*/NULL);
	}

IL_014e:
	{
		goto IL_019c;
	}

IL_0153:
	{
		GameObject_t1113636619 * L_44 = __this->get_audioSource_2();
		NullCheck(L_44);
		bool L_45 = GameObject_get_activeSelf_m1767405923(L_44, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_016f;
		}
	}
	{
		GameObject_t1113636619 * L_46 = __this->get_audioSource_2();
		NullCheck(L_46);
		GameObject_SetActive_m796801857(L_46, (bool)1, /*hidden argument*/NULL);
	}

IL_016f:
	{
		AudioSource_t3935305588 * L_47 = V_0;
		NullCheck(L_47);
		AudioClip_t3680889665 * L_48 = AudioSource_get_clip_m1234340632(L_47, /*hidden argument*/NULL);
		AudioClip_t3680889665 * L_49 = __this->get_clip_egyptSound_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_50 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_0197;
		}
	}
	{
		AudioSource_t3935305588 * L_51 = V_0;
		AudioClip_t3680889665 * L_52 = __this->get_clip_egyptSound_6();
		NullCheck(L_51);
		AudioSource_set_clip_m31653938(L_51, L_52, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_53 = V_0;
		NullCheck(L_53);
		AudioSource_Play_m48294159(L_53, /*hidden argument*/NULL);
	}

IL_0197:
	{
		goto IL_019c;
	}

IL_019c:
	{
		return;
	}
}
// System.Void ButtonControllerScript::selectTheme(System.String)
extern "C"  void ButtonControllerScript_selectTheme_m4172231223 (ButtonControllerScript_t2432915718 * __this, String_t* ___theme0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_selectTheme_m4172231223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___theme0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral230658322, L_0, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral404843312, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___theme0;
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral3359376429, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___theme0;
		ButtonControllerScript_changeTheme_m1190884257(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonControllerScript::changeTheme(System.String)
extern "C"  void ButtonControllerScript_changeTheme_m1190884257 (ButtonControllerScript_t2432915718 * __this, String_t* ___theme0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_changeTheme_m1190884257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRenderer_t3235626157 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_backgroundSprite_7();
		NullCheck(L_0);
		SpriteRenderer_t3235626157 * L_1 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		V_0 = L_1;
		String_t* L_2 = ___theme0;
		if (!L_2)
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_3 = ___theme0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral3790055497, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0057;
		}
	}
	{
		String_t* L_5 = ___theme0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, _stringLiteral3775034288, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0068;
		}
	}
	{
		String_t* L_7 = ___theme0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_7, _stringLiteral2699831051, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_9 = ___theme0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_9, _stringLiteral166522051, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_008a;
		}
	}
	{
		goto IL_009b;
	}

IL_0057:
	{
		SpriteRenderer_t3235626157 * L_11 = V_0;
		Sprite_t280657092 * L_12 = __this->get_mainTheme_8();
		NullCheck(L_11);
		SpriteRenderer_set_sprite_m1286893786(L_11, L_12, /*hidden argument*/NULL);
		goto IL_009b;
	}

IL_0068:
	{
		SpriteRenderer_t3235626157 * L_13 = V_0;
		Sprite_t280657092 * L_14 = __this->get_flappyTheme_9();
		NullCheck(L_13);
		SpriteRenderer_set_sprite_m1286893786(L_13, L_14, /*hidden argument*/NULL);
		goto IL_009b;
	}

IL_0079:
	{
		SpriteRenderer_t3235626157 * L_15 = V_0;
		Sprite_t280657092 * L_16 = __this->get_nightTheme_10();
		NullCheck(L_15);
		SpriteRenderer_set_sprite_m1286893786(L_15, L_16, /*hidden argument*/NULL);
		goto IL_009b;
	}

IL_008a:
	{
		SpriteRenderer_t3235626157 * L_17 = V_0;
		Sprite_t280657092 * L_18 = __this->get_desertTheme_11();
		NullCheck(L_17);
		SpriteRenderer_set_sprite_m1286893786(L_17, L_18, /*hidden argument*/NULL);
		goto IL_009b;
	}

IL_009b:
	{
		return;
	}
}
// System.Collections.IEnumerator ButtonControllerScript::ShowAdWhenReady()
extern "C"  RuntimeObject* ButtonControllerScript_ShowAdWhenReady_m2072552498 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_ShowAdWhenReady_m2072552498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * V_0 = NULL;
	{
		U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * L_0 = (U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 *)il2cpp_codegen_object_new(U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249_il2cpp_TypeInfo_var);
		U3CShowAdWhenReadyU3Ec__Iterator1__ctor_m4043849735(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ButtonControllerScript::HandleShowResult(UnityEngine.Advertisements.ShowResult)
extern "C"  void ButtonControllerScript_HandleShowResult_m200410161 (ButtonControllerScript_t2432915718 * __this, int32_t ___result0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___result0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___result0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0037;
	}

IL_0013:
	{
		RuntimeObject* L_2 = ButtonControllerScript_HandleAnimation_m1917644310(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_2, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_0025:
	{
		RuntimeObject* L_3 = ButtonControllerScript_HandleAnimation_m1917644310(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_0037:
	{
		return;
	}
}
// System.Collections.IEnumerator ButtonControllerScript::HandleAnimation()
extern "C"  RuntimeObject* ButtonControllerScript_HandleAnimation_m1917644310 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_HandleAnimation_m1917644310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CHandleAnimationU3Ec__Iterator2_t1056433757 * V_0 = NULL;
	{
		U3CHandleAnimationU3Ec__Iterator2_t1056433757 * L_0 = (U3CHandleAnimationU3Ec__Iterator2_t1056433757 *)il2cpp_codegen_object_new(U3CHandleAnimationU3Ec__Iterator2_t1056433757_il2cpp_TypeInfo_var);
		U3CHandleAnimationU3Ec__Iterator2__ctor_m3913915067(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CHandleAnimationU3Ec__Iterator2_t1056433757 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CHandleAnimationU3Ec__Iterator2_t1056433757 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ButtonControllerScript::resetGravity()
extern "C"  void ButtonControllerScript_resetGravity_m3471744663 (ButtonControllerScript_t2432915718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_resetGravity_m3471744663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_singleObstacle_43();
		NullCheck(L_0);
		GravityScript_t2220762432 * L_1 = GameObject_GetComponent_TisGravityScript_t2220762432_m2338767488(L_0, /*hidden argument*/GameObject_GetComponent_TisGravityScript_t2220762432_m2338767488_RuntimeMethod_var);
		NullCheck(L_1);
		GravityScript_resetGravity_m3601770064(L_1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_doubleObstacle_44();
		NullCheck(L_2);
		GravityScript_t2220762432 * L_3 = GameObject_GetComponent_TisGravityScript_t2220762432_m2338767488(L_2, /*hidden argument*/GameObject_GetComponent_TisGravityScript_t2220762432_m2338767488_RuntimeMethod_var);
		NullCheck(L_3);
		GravityScript_resetGravity_m3601770064(L_3, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = __this->get_trippleObstacle_45();
		NullCheck(L_4);
		GravityScript_t2220762432 * L_5 = GameObject_GetComponent_TisGravityScript_t2220762432_m2338767488(L_4, /*hidden argument*/GameObject_GetComponent_TisGravityScript_t2220762432_m2338767488_RuntimeMethod_var);
		NullCheck(L_5);
		GravityScript_resetGravity_m3601770064(L_5, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = __this->get_quattroObstacle_46();
		NullCheck(L_6);
		GravityScript_t2220762432 * L_7 = GameObject_GetComponent_TisGravityScript_t2220762432_m2338767488(L_6, /*hidden argument*/GameObject_GetComponent_TisGravityScript_t2220762432_m2338767488_RuntimeMethod_var);
		NullCheck(L_7);
		GravityScript_resetGravity_m3601770064(L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonControllerScript::playPreviewSound(System.String)
extern "C"  void ButtonControllerScript_playPreviewSound_m1032544213 (ButtonControllerScript_t2432915718 * __this, String_t* ___previewSound0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonControllerScript_playPreviewSound_m1032544213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSource_t3935305588 * V_0 = NULL;
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral4011269308, _stringLiteral4002445229, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = __this->get_audioSource_2();
		NullCheck(L_0);
		AudioSource_t3935305588 * L_1 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		V_0 = L_1;
		GameObject_t1113636619 * L_2 = __this->get_audioSource_2();
		NullCheck(L_2);
		bool L_3 = GameObject_get_activeSelf_m1767405923(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0037;
		}
	}
	{
		GameObject_t1113636619 * L_4 = __this->get_audioSource_2();
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)1, /*hidden argument*/NULL);
	}

IL_0037:
	{
		String_t* L_5 = ___previewSound0;
		if (!L_5)
		{
			goto IL_00a5;
		}
	}
	{
		String_t* L_6 = ___previewSound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, _stringLiteral3654983690, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0072;
		}
	}
	{
		String_t* L_8 = ___previewSound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m920492651(NULL /*static, unused*/, L_8, _stringLiteral747299879, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0083;
		}
	}
	{
		String_t* L_10 = ___previewSound0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m920492651(NULL /*static, unused*/, L_10, _stringLiteral3163906174, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0094;
		}
	}
	{
		goto IL_00a5;
	}

IL_0072:
	{
		AudioSource_t3935305588 * L_12 = V_0;
		AudioClip_t3680889665 * L_13 = __this->get_clip_skyflightSound_5();
		NullCheck(L_12);
		AudioSource_set_clip_m31653938(L_12, L_13, /*hidden argument*/NULL);
		goto IL_00a5;
	}

IL_0083:
	{
		AudioSource_t3935305588 * L_14 = V_0;
		AudioClip_t3680889665 * L_15 = __this->get_clip_dankstormSound_4();
		NullCheck(L_14);
		AudioSource_set_clip_m31653938(L_14, L_15, /*hidden argument*/NULL);
		goto IL_00a5;
	}

IL_0094:
	{
		AudioSource_t3935305588 * L_16 = V_0;
		AudioClip_t3680889665 * L_17 = __this->get_clip_egyptSound_6();
		NullCheck(L_16);
		AudioSource_set_clip_m31653938(L_16, L_17, /*hidden argument*/NULL);
		goto IL_00a5;
	}

IL_00a5:
	{
		AudioSource_t3935305588 * L_18 = V_0;
		NullCheck(L_18);
		AudioSource_Play_m48294159(L_18, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonControllerScript/<DelayGameStart>c__Iterator0::.ctor()
extern "C"  void U3CDelayGameStartU3Ec__Iterator0__ctor_m3486972112 (U3CDelayGameStartU3Ec__Iterator0_t3856269498 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ButtonControllerScript/<DelayGameStart>c__Iterator0::MoveNext()
extern "C"  bool U3CDelayGameStartU3Ec__Iterator0_MoveNext_m649956165 (U3CDelayGameStartU3Ec__Iterator0_t3856269498 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayGameStartU3Ec__Iterator0_MoveNext_m649956165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_00e4;
	}

IL_0021:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (0.2f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_00e6;
	}

IL_0045:
	{
		ButtonControllerScript_t2432915718 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = L_4->get_cnv_settings_32();
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)0, /*hidden argument*/NULL);
		ButtonControllerScript_t2432915718 * L_6 = __this->get_U24this_0();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = L_6->get_cnv_menu_31();
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)0, /*hidden argument*/NULL);
		ButtonControllerScript_t2432915718 * L_8 = __this->get_U24this_0();
		NullCheck(L_8);
		GameObject_t1113636619 * L_9 = L_8->get_cnv_game_33();
		NullCheck(L_9);
		GameObject_SetActive_m796801857(L_9, (bool)1, /*hidden argument*/NULL);
		ButtonControllerScript_t2432915718 * L_10 = __this->get_U24this_0();
		NullCheck(L_10);
		String_t* L_11 = L_10->get_difficulty_49();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_11, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_13 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral870646944, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral870646944, ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1)), /*hidden argument*/NULL);
		goto IL_00dd;
	}

IL_00ad:
	{
		ButtonControllerScript_t2432915718 * L_14 = __this->get_U24this_0();
		NullCheck(L_14);
		String_t* L_15 = L_14->get_difficulty_49();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m920492651(NULL /*static, unused*/, L_15, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00dd;
		}
	}
	{
		int32_t L_17 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral388954149, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral388954149, ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1)), /*hidden argument*/NULL);
	}

IL_00dd:
	{
		__this->set_U24PC_3((-1));
	}

IL_00e4:
	{
		return (bool)0;
	}

IL_00e6:
	{
		return (bool)1;
	}
}
// System.Object ButtonControllerScript/<DelayGameStart>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CDelayGameStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m313983947 (U3CDelayGameStartU3Ec__Iterator0_t3856269498 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ButtonControllerScript/<DelayGameStart>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CDelayGameStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1470980665 (U3CDelayGameStartU3Ec__Iterator0_t3856269498 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void ButtonControllerScript/<DelayGameStart>c__Iterator0::Dispose()
extern "C"  void U3CDelayGameStartU3Ec__Iterator0_Dispose_m3960277018 (U3CDelayGameStartU3Ec__Iterator0_t3856269498 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void ButtonControllerScript/<DelayGameStart>c__Iterator0::Reset()
extern "C"  void U3CDelayGameStartU3Ec__Iterator0_Reset_m3146499568 (U3CDelayGameStartU3Ec__Iterator0_t3856269498 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayGameStartU3Ec__Iterator0_Reset_m3146499568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonControllerScript/<HandleAnimation>c__Iterator2::.ctor()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2__ctor_m3913915067 (U3CHandleAnimationU3Ec__Iterator2_t1056433757 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ButtonControllerScript/<HandleAnimation>c__Iterator2::MoveNext()
extern "C"  bool U3CHandleAnimationU3Ec__Iterator2_MoveNext_m3372310698 (U3CHandleAnimationU3Ec__Iterator2_t1056433757 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CHandleAnimationU3Ec__Iterator2_MoveNext_m3372310698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0041;
			}
		}
	}
	{
		goto IL_0067;
	}

IL_0021:
	{
		goto IL_0041;
	}

IL_0026:
	{
		__this->set_U24current_1(NULL);
		bool L_2 = __this->get_U24disposing_2();
		if (L_2)
		{
			goto IL_003c;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_003c:
	{
		goto IL_0069;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		bool L_3 = Advertisement_get_isShowing_m3507224835(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		ButtonControllerScript_t2432915718 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		Animator_t434523843 * L_5 = L_4->get_anim_39();
		NullCheck(L_5);
		Animator_Play_m1697843332(L_5, _stringLiteral1578302179, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0067:
	{
		return (bool)0;
	}

IL_0069:
	{
		return (bool)1;
	}
}
// System.Object ButtonControllerScript/<HandleAnimation>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CHandleAnimationU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4093547286 (U3CHandleAnimationU3Ec__Iterator2_t1056433757 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ButtonControllerScript/<HandleAnimation>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CHandleAnimationU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1945090563 (U3CHandleAnimationU3Ec__Iterator2_t1056433757 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void ButtonControllerScript/<HandleAnimation>c__Iterator2::Dispose()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2_Dispose_m2603768003 (U3CHandleAnimationU3Ec__Iterator2_t1056433757 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void ButtonControllerScript/<HandleAnimation>c__Iterator2::Reset()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2_Reset_m213181928 (U3CHandleAnimationU3Ec__Iterator2_t1056433757 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CHandleAnimationU3Ec__Iterator2_Reset_m213181928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::.ctor()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1__ctor_m4043849735 (U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::MoveNext()
extern "C"  bool U3CShowAdWhenReadyU3Ec__Iterator1_MoveNext_m3890286113 (U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowAdWhenReadyU3Ec__Iterator1_MoveNext_m3890286113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	ShowOptions_t990845000 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0041;
			}
		}
	}
	{
		goto IL_0097;
	}

IL_0021:
	{
		goto IL_0041;
	}

IL_0026:
	{
		__this->set_U24current_2(NULL);
		bool L_2 = __this->get_U24disposing_3();
		if (L_2)
		{
			goto IL_003c;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_003c:
	{
		goto IL_0099;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		bool L_3 = Advertisement_IsReady_m2792558112(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		ShowOptions_t990845000 * L_4 = (ShowOptions_t990845000 *)il2cpp_codegen_object_new(ShowOptions_t990845000_il2cpp_TypeInfo_var);
		ShowOptions__ctor_m2194205660(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		ShowOptions_t990845000 * L_5 = V_1;
		ButtonControllerScript_t2432915718 * L_6 = __this->get_U24this_1();
		intptr_t L_7 = (intptr_t)ButtonControllerScript_HandleShowResult_m200410161_RuntimeMethod_var;
		Action_1_t3243021218 * L_8 = (Action_1_t3243021218 *)il2cpp_codegen_object_new(Action_1_t3243021218_il2cpp_TypeInfo_var);
		Action_1__ctor_m3797332079(L_8, L_6, L_7, /*hidden argument*/Action_1__ctor_m3797332079_RuntimeMethod_var);
		NullCheck(L_5);
		ShowOptions_set_resultCallback_m3887508449(L_5, L_8, /*hidden argument*/NULL);
		ShowOptions_t990845000 * L_9 = V_1;
		__this->set_U3CoptionsU3E__0_0(L_9);
		ShowOptions_t990845000 * L_10 = __this->get_U3CoptionsU3E__0_0();
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		Advertisement_Show_m53580060(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3831740680, ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_0097:
	{
		return (bool)0;
	}

IL_0099:
	{
		return (bool)1;
	}
}
// System.Object ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CShowAdWhenReadyU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1120398288 (U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CShowAdWhenReadyU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m796175227 (U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::Dispose()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1_Dispose_m2634302449 (U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void ButtonControllerScript/<ShowAdWhenReady>c__Iterator1::Reset()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1_Reset_m2101412034 (U3CShowAdWhenReadyU3Ec__Iterator1_t1035390249 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowAdWhenReadyU3Ec__Iterator1_Reset_m2101412034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DestroyerScript::.ctor()
extern "C"  void DestroyerScript__ctor_m1065780995 (DestroyerScript_t3910417206 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyerScript::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void DestroyerScript_OnTriggerEnter_m1364252301 (DestroyerScript_t3910417206 * __this, Collider_t1773347010 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DestroyerScript_OnTriggerEnter_m1364252301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t1773347010 * L_0 = ___collider0;
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m4211327027(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, _stringLiteral823026589, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		Collider_t1773347010 * L_4 = ___collider0;
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = GameObject_get_tag_m3951609671(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, _stringLiteral823026589, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003f;
		}
	}

IL_0034:
	{
		Collider_t1773347010 * L_8 = ___collider0;
		NullCheck(L_8);
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_003f:
	{
		Collider_t1773347010 * L_10 = ___collider0;
		NullCheck(L_10);
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m4211327027(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m920492651(NULL /*static, unused*/, L_12, _stringLiteral2929758584, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0073;
		}
	}
	{
		Collider_t1773347010 * L_14 = ___collider0;
		NullCheck(L_14);
		GameObject_t1113636619 * L_15 = Component_get_gameObject_m442555142(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16 = GameObject_get_tag_m3951609671(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m920492651(NULL /*static, unused*/, L_16, _stringLiteral2929758584, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_013e;
		}
	}

IL_0073:
	{
		int32_t L_18 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3046124294, /*hidden argument*/NULL);
		__this->set_actScore_3(L_18);
		int32_t L_19 = __this->get_actScore_3();
		__this->set_actScore_3(((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1)));
		int32_t L_20 = __this->get_actScore_3();
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3046124294, L_20, /*hidden argument*/NULL);
		String_t* L_21 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3316871923, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_21, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00e4;
		}
	}
	{
		int32_t L_23 = __this->get_actScore_3();
		int32_t L_24 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral167811139, /*hidden argument*/NULL);
		if ((((int32_t)L_23) <= ((int32_t)L_24)))
		{
			goto IL_00df;
		}
	}
	{
		int32_t L_25 = __this->get_actScore_3();
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral167811139, L_25, /*hidden argument*/NULL);
	}

IL_00df:
	{
		goto IL_0122;
	}

IL_00e4:
	{
		String_t* L_26 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3316871923, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_op_Equality_m920492651(NULL /*static, unused*/, L_26, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0122;
		}
	}
	{
		int32_t L_28 = __this->get_actScore_3();
		int32_t L_29 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral2722989498, /*hidden argument*/NULL);
		if ((((int32_t)L_28) <= ((int32_t)L_29)))
		{
			goto IL_0122;
		}
	}
	{
		int32_t L_30 = __this->get_actScore_3();
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2722989498, L_30, /*hidden argument*/NULL);
	}

IL_0122:
	{
		Text_t1901882714 * L_31 = __this->get_scoreText_2();
		int32_t* L_32 = __this->get_address_of_actScore_3();
		String_t* L_33 = Int32_ToString_m141394615(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_31, L_33);
	}

IL_013e:
	{
		Collider_t1773347010 * L_34 = ___collider0;
		NullCheck(L_34);
		GameObject_t1113636619 * L_35 = Component_get_gameObject_m442555142(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_36 = Object_get_name_m4211327027(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_37 = String_op_Equality_m920492651(NULL /*static, unused*/, L_36, _stringLiteral2929758584, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_0172;
		}
	}
	{
		Collider_t1773347010 * L_38 = ___collider0;
		NullCheck(L_38);
		GameObject_t1113636619 * L_39 = Component_get_gameObject_m442555142(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		String_t* L_40 = GameObject_get_tag_m3951609671(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_41 = String_op_Equality_m920492651(NULL /*static, unused*/, L_40, _stringLiteral2929758584, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0178;
		}
	}

IL_0172:
	{
		Collider_t1773347010 * L_42 = ___collider0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
	}

IL_0178:
	{
		Collider_t1773347010 * L_43 = ___collider0;
		NullCheck(L_43);
		GameObject_t1113636619 * L_44 = Component_get_gameObject_m442555142(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		String_t* L_45 = GameObject_get_tag_m3951609671(L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_op_Equality_m920492651(NULL /*static, unused*/, L_45, _stringLiteral3452654525, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0198;
		}
	}
	{
		Collider_t1773347010 * L_47 = ___collider0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
	}

IL_0198:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GravityScript::.ctor()
extern "C"  void GravityScript__ctor_m3628567097 (GravityScript_t2220762432 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GravityScript::Start()
extern "C"  void GravityScript_Start_m1755594917 (GravityScript_t2220762432 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (0.0f), (-0.1f), (0.0f), /*hidden argument*/NULL);
		__this->set_additional_gravity_3(L_0);
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m3353183577((&L_2), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m3053443106(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GravityScript::FixedUpdate()
extern "C"  void GravityScript_FixedUpdate_m3746438327 (GravityScript_t2220762432 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GravityScript_FixedUpdate_m3746438327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3046124294, /*hidden argument*/NULL);
		__this->set_actScore_4(L_0);
		int32_t L_1 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3556801464, /*hidden argument*/NULL);
		__this->set_speed_5(L_1);
		int32_t L_2 = __this->get_speed_5();
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_3 = __this->get_speed_5();
		__this->set_moduloSpeed_7(L_3);
	}

IL_0037:
	{
		int32_t L_4 = __this->get_actScore_4();
		int32_t L_5 = __this->get_moduloSpeed_7();
		if (((int32_t)((int32_t)L_4%(int32_t)L_5)))
		{
			goto IL_0077;
		}
	}
	{
		bool L_6 = __this->get_addGravity_6();
		if (!L_6)
		{
			goto IL_0072;
		}
	}
	{
		Vector3_t3722313464  L_7 = __this->get_gravity_2();
		Vector3_t3722313464  L_8 = __this->get_additional_gravity_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		__this->set_gravity_2(L_9);
		__this->set_addGravity_6((bool)0);
	}

IL_0072:
	{
		goto IL_007e;
	}

IL_0077:
	{
		__this->set_addGravity_6((bool)1);
	}

IL_007e:
	{
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_11 = L_10;
		NullCheck(L_11);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = __this->get_gravity_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_14 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_position_m3387557959(L_11, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GravityScript::resetGravity()
extern "C"  void GravityScript_resetGravity_m3601770064 (GravityScript_t2220762432 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (0.0f), (-4.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_gravity_2(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InitButtonsScript::.ctor()
extern "C"  void InitButtonsScript__ctor_m3266491125 (InitButtonsScript_t1987868624 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InitButtonsScript::Start()
extern "C"  void InitButtonsScript_Start_m288105319 (InitButtonsScript_t1987868624 * __this, const RuntimeMethod* method)
{
	{
		InitButtonsScript_InitButtons_m1101143926(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InitButtonsScript::InitButtons()
extern "C"  void InitButtonsScript_InitButtons_m1101143926 (InitButtonsScript_t1987868624 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitButtonsScript_InitButtons_m1101143926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	Dictionary_2_t2736202052 * V_8 = NULL;
	int32_t V_9 = 0;
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3060255191, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral4130524952, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2687637966, /*hidden argument*/NULL);
		V_2 = L_2;
		String_t* L_3 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2272329368, /*hidden argument*/NULL);
		V_3 = L_3;
		String_t* L_4 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral4005421059, /*hidden argument*/NULL);
		V_4 = L_4;
		String_t* L_5 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2378241666, /*hidden argument*/NULL);
		V_5 = L_5;
		String_t* L_6 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral404843312, /*hidden argument*/NULL);
		V_6 = L_6;
		String_t* L_7 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3075782012, /*hidden argument*/NULL);
		V_7 = L_7;
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m920492651(NULL /*static, unused*/, L_8, _stringLiteral3875954633, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007d;
		}
	}
	{
		GameObject_t1113636619 * L_10 = __this->get_btn_nightThemeInactive_2();
		NullCheck(L_10);
		GameObject_SetActive_m796801857(L_10, (bool)0, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_007d:
	{
		GameObject_t1113636619 * L_11 = __this->get_btn_nightThemeInactive_2();
		NullCheck(L_11);
		GameObject_SetActive_m796801857(L_11, (bool)1, /*hidden argument*/NULL);
	}

IL_0089:
	{
		String_t* L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m920492651(NULL /*static, unused*/, L_12, _stringLiteral3875954633, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00aa;
		}
	}
	{
		GameObject_t1113636619 * L_14 = __this->get_btn_flappyThemeInactive_3();
		NullCheck(L_14);
		GameObject_SetActive_m796801857(L_14, (bool)0, /*hidden argument*/NULL);
		goto IL_00b6;
	}

IL_00aa:
	{
		GameObject_t1113636619 * L_15 = __this->get_btn_flappyThemeInactive_3();
		NullCheck(L_15);
		GameObject_SetActive_m796801857(L_15, (bool)1, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		String_t* L_16 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m920492651(NULL /*static, unused*/, L_16, _stringLiteral3875954633, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00d7;
		}
	}
	{
		GameObject_t1113636619 * L_18 = __this->get_btn_desertThemeInactive_4();
		NullCheck(L_18);
		GameObject_SetActive_m796801857(L_18, (bool)0, /*hidden argument*/NULL);
		goto IL_00e3;
	}

IL_00d7:
	{
		GameObject_t1113636619 * L_19 = __this->get_btn_desertThemeInactive_4();
		NullCheck(L_19);
		GameObject_SetActive_m796801857(L_19, (bool)1, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		String_t* L_20 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_op_Equality_m920492651(NULL /*static, unused*/, L_20, _stringLiteral3875954633, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0104;
		}
	}
	{
		GameObject_t1113636619 * L_22 = __this->get_btn_skyflightSoundInactive_5();
		NullCheck(L_22);
		GameObject_SetActive_m796801857(L_22, (bool)0, /*hidden argument*/NULL);
		goto IL_0110;
	}

IL_0104:
	{
		GameObject_t1113636619 * L_23 = __this->get_btn_skyflightSoundInactive_5();
		NullCheck(L_23);
		GameObject_SetActive_m796801857(L_23, (bool)1, /*hidden argument*/NULL);
	}

IL_0110:
	{
		String_t* L_24 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m920492651(NULL /*static, unused*/, L_24, _stringLiteral3875954633, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0132;
		}
	}
	{
		GameObject_t1113636619 * L_26 = __this->get_btn_dankstormSoundInactive_6();
		NullCheck(L_26);
		GameObject_SetActive_m796801857(L_26, (bool)0, /*hidden argument*/NULL);
		goto IL_013e;
	}

IL_0132:
	{
		GameObject_t1113636619 * L_27 = __this->get_btn_dankstormSoundInactive_6();
		NullCheck(L_27);
		GameObject_SetActive_m796801857(L_27, (bool)1, /*hidden argument*/NULL);
	}

IL_013e:
	{
		String_t* L_28 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Equality_m920492651(NULL /*static, unused*/, L_28, _stringLiteral3875954633, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0160;
		}
	}
	{
		GameObject_t1113636619 * L_30 = __this->get_btn_egyptSoundInactive_7();
		NullCheck(L_30);
		GameObject_SetActive_m796801857(L_30, (bool)0, /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_0160:
	{
		GameObject_t1113636619 * L_31 = __this->get_btn_egyptSoundInactive_7();
		NullCheck(L_31);
		GameObject_SetActive_m796801857(L_31, (bool)1, /*hidden argument*/NULL);
	}

IL_016c:
	{
		String_t* L_32 = V_6;
		if (!L_32)
		{
			goto IL_0200;
		}
	}
	{
		String_t* L_33 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_34 = String_op_Equality_m920492651(NULL /*static, unused*/, L_33, _stringLiteral816225280, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_01bc;
		}
	}
	{
		String_t* L_35 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_36 = String_op_Equality_m920492651(NULL /*static, unused*/, L_35, _stringLiteral3921657742, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_01cd;
		}
	}
	{
		String_t* L_37 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_38 = String_op_Equality_m920492651(NULL /*static, unused*/, L_37, _stringLiteral1559865961, /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_01de;
		}
	}
	{
		String_t* L_39 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_40 = String_op_Equality_m920492651(NULL /*static, unused*/, L_39, _stringLiteral681917307, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_01ef;
		}
	}
	{
		goto IL_0200;
	}

IL_01bc:
	{
		GameObject_t1113636619 * L_41 = __this->get_ckm_mainTheme_9();
		NullCheck(L_41);
		GameObject_SetActive_m796801857(L_41, (bool)1, /*hidden argument*/NULL);
		goto IL_0211;
	}

IL_01cd:
	{
		GameObject_t1113636619 * L_42 = __this->get_ckm_nightTheme_10();
		NullCheck(L_42);
		GameObject_SetActive_m796801857(L_42, (bool)1, /*hidden argument*/NULL);
		goto IL_0211;
	}

IL_01de:
	{
		GameObject_t1113636619 * L_43 = __this->get_ckm_flappyTheme_11();
		NullCheck(L_43);
		GameObject_SetActive_m796801857(L_43, (bool)1, /*hidden argument*/NULL);
		goto IL_0211;
	}

IL_01ef:
	{
		GameObject_t1113636619 * L_44 = __this->get_ckm_desertTheme_12();
		NullCheck(L_44);
		GameObject_SetActive_m796801857(L_44, (bool)1, /*hidden argument*/NULL);
		goto IL_0211;
	}

IL_0200:
	{
		GameObject_t1113636619 * L_45 = __this->get_ckm_mainTheme_9();
		NullCheck(L_45);
		GameObject_SetActive_m796801857(L_45, (bool)1, /*hidden argument*/NULL);
		goto IL_0211;
	}

IL_0211:
	{
		String_t* L_46 = V_7;
		if (!L_46)
		{
			goto IL_0387;
		}
	}
	{
		Dictionary_2_t2736202052 * L_47 = ((InitButtonsScript_t1987868624_StaticFields*)il2cpp_codegen_static_fields_for(InitButtonsScript_t1987868624_il2cpp_TypeInfo_var))->get_U3CU3Ef__switchU24map0_18();
		if (L_47)
		{
			goto IL_02a7;
		}
	}
	{
		Dictionary_2_t2736202052 * L_48 = (Dictionary_2_t2736202052 *)il2cpp_codegen_object_new(Dictionary_2_t2736202052_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2392909825(L_48, ((int32_t)9), /*hidden argument*/Dictionary_2__ctor_m2392909825_RuntimeMethod_var);
		V_8 = L_48;
		Dictionary_2_t2736202052 * L_49 = V_8;
		NullCheck(L_49);
		Dictionary_2_Add_m282647386(L_49, _stringLiteral2074989104, 0, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_50 = V_8;
		NullCheck(L_50);
		Dictionary_2_Add_m282647386(L_50, _stringLiteral1938886732, 1, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_51 = V_8;
		NullCheck(L_51);
		Dictionary_2_Add_m282647386(L_51, _stringLiteral1114791609, 2, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_52 = V_8;
		NullCheck(L_52);
		Dictionary_2_Add_m282647386(L_52, _stringLiteral2070893366, 3, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_53 = V_8;
		NullCheck(L_53);
		Dictionary_2_Add_m282647386(L_53, _stringLiteral3654983690, 4, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_54 = V_8;
		NullCheck(L_54);
		Dictionary_2_Add_m282647386(L_54, _stringLiteral2895553709, 5, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_55 = V_8;
		NullCheck(L_55);
		Dictionary_2_Add_m282647386(L_55, _stringLiteral747299879, 6, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_56 = V_8;
		NullCheck(L_56);
		Dictionary_2_Add_m282647386(L_56, _stringLiteral2785512304, 7, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_57 = V_8;
		NullCheck(L_57);
		Dictionary_2_Add_m282647386(L_57, _stringLiteral3593808015, 8, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_58 = V_8;
		((InitButtonsScript_t1987868624_StaticFields*)il2cpp_codegen_static_fields_for(InitButtonsScript_t1987868624_il2cpp_TypeInfo_var))->set_U3CU3Ef__switchU24map0_18(L_58);
	}

IL_02a7:
	{
		Dictionary_2_t2736202052 * L_59 = ((InitButtonsScript_t1987868624_StaticFields*)il2cpp_codegen_static_fields_for(InitButtonsScript_t1987868624_il2cpp_TypeInfo_var))->get_U3CU3Ef__switchU24map0_18();
		String_t* L_60 = V_7;
		NullCheck(L_59);
		bool L_61 = Dictionary_2_TryGetValue_m1013208020(L_59, L_60, (&V_9), /*hidden argument*/Dictionary_2_TryGetValue_m1013208020_RuntimeMethod_var);
		if (!L_61)
		{
			goto IL_0387;
		}
	}
	{
		int32_t L_62 = V_9;
		switch (L_62)
		{
			case 0:
			{
				goto IL_02ee;
			}
			case 1:
			{
				goto IL_02ff;
			}
			case 2:
			{
				goto IL_0310;
			}
			case 3:
			{
				goto IL_0321;
			}
			case 4:
			{
				goto IL_0332;
			}
			case 5:
			{
				goto IL_0343;
			}
			case 6:
			{
				goto IL_0354;
			}
			case 7:
			{
				goto IL_0365;
			}
			case 8:
			{
				goto IL_0376;
			}
			case 9:
			{
				goto IL_0387;
			}
		}
	}
	{
		goto IL_0387;
	}

IL_02ee:
	{
		GameObject_t1113636619 * L_63 = __this->get_ckm_noSound_13();
		NullCheck(L_63);
		GameObject_SetActive_m796801857(L_63, (bool)1, /*hidden argument*/NULL);
		goto IL_0398;
	}

IL_02ff:
	{
		GameObject_t1113636619 * L_64 = __this->get_ckm_noSound_13();
		NullCheck(L_64);
		GameObject_SetActive_m796801857(L_64, (bool)1, /*hidden argument*/NULL);
		goto IL_0398;
	}

IL_0310:
	{
		GameObject_t1113636619 * L_65 = __this->get_ckm_mainSound_14();
		NullCheck(L_65);
		GameObject_SetActive_m796801857(L_65, (bool)1, /*hidden argument*/NULL);
		goto IL_0398;
	}

IL_0321:
	{
		GameObject_t1113636619 * L_66 = __this->get_ckm_mainSound_14();
		NullCheck(L_66);
		GameObject_SetActive_m796801857(L_66, (bool)1, /*hidden argument*/NULL);
		goto IL_0398;
	}

IL_0332:
	{
		GameObject_t1113636619 * L_67 = __this->get_ckm_skyflightSound_15();
		NullCheck(L_67);
		GameObject_SetActive_m796801857(L_67, (bool)1, /*hidden argument*/NULL);
		goto IL_0398;
	}

IL_0343:
	{
		GameObject_t1113636619 * L_68 = __this->get_ckm_skyflightSound_15();
		NullCheck(L_68);
		GameObject_SetActive_m796801857(L_68, (bool)1, /*hidden argument*/NULL);
		goto IL_0398;
	}

IL_0354:
	{
		GameObject_t1113636619 * L_69 = __this->get_ckm_dankstormSound_16();
		NullCheck(L_69);
		GameObject_SetActive_m796801857(L_69, (bool)1, /*hidden argument*/NULL);
		goto IL_0398;
	}

IL_0365:
	{
		GameObject_t1113636619 * L_70 = __this->get_ckm_dankstormSound_16();
		NullCheck(L_70);
		GameObject_SetActive_m796801857(L_70, (bool)1, /*hidden argument*/NULL);
		goto IL_0398;
	}

IL_0376:
	{
		GameObject_t1113636619 * L_71 = __this->get_ckm_egyptSound_17();
		NullCheck(L_71);
		GameObject_SetActive_m796801857(L_71, (bool)1, /*hidden argument*/NULL);
		goto IL_0398;
	}

IL_0387:
	{
		GameObject_t1113636619 * L_72 = __this->get_ckm_mainSound_14();
		NullCheck(L_72);
		GameObject_SetActive_m796801857(L_72, (bool)1, /*hidden argument*/NULL);
		goto IL_0398;
	}

IL_0398:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InitTextsScript::.ctor()
extern "C"  void InitTextsScript__ctor_m3049154465 (InitTextsScript_t1053055510 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InitTextsScript::Start()
extern "C"  void InitTextsScript_Start_m1608449831 (InitTextsScript_t1053055510 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitTextsScript_Start_m1608449831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3316871923, /*hidden argument*/NULL);
		__this->set_difficulty_28(L_0);
		GameObject_t1113636619 * L_1 = __this->get_cnv_menu_2();
		NullCheck(L_1);
		bool L_2 = GameObject_get_activeSelf_m1767405923(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00e8;
		}
	}
	{
		String_t* L_3 = __this->get_difficulty_28();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_3, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_5 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral167811139, /*hidden argument*/NULL);
		__this->set_highscore_7(L_5);
		int32_t L_6 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral870646944, /*hidden argument*/NULL);
		__this->set_tries_9(L_6);
		goto IL_008f;
	}

IL_005a:
	{
		String_t* L_7 = __this->get_difficulty_28();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_7, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_008f;
		}
	}
	{
		int32_t L_9 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral2722989498, /*hidden argument*/NULL);
		__this->set_highscore_7(L_9);
		int32_t L_10 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral388954149, /*hidden argument*/NULL);
		__this->set_tries_9(L_10);
	}

IL_008f:
	{
		Text_t1901882714 * L_11 = __this->get_textHighscoreCount_6();
		int32_t* L_12 = __this->get_address_of_highscore_7();
		String_t* L_13 = Int32_ToString_m141394615(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_13);
		Text_t1901882714 * L_14 = __this->get_textPlayedCount_8();
		int32_t* L_15 = __this->get_address_of_tries_9();
		String_t* L_16 = Int32_ToString_m141394615(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, L_16);
		Text_t1901882714 * L_17 = __this->get_textLastPoints_10();
		int32_t* L_18 = __this->get_address_of_lastPoints_11();
		String_t* L_19 = Int32_ToString_m141394615(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_17, L_19);
		goto IL_01ef;
	}

IL_00e8:
	{
		GameObject_t1113636619 * L_20 = __this->get_cnv_settings_3();
		NullCheck(L_20);
		bool L_21 = GameObject_get_activeSelf_m1767405923(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0120;
		}
	}
	{
		Text_t1901882714 * L_22 = __this->get_specialPointsText_13();
		int32_t L_23 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		V_0 = L_23;
		String_t* L_24 = Int32_ToString_m141394615((&V_0), /*hidden argument*/NULL);
		NullCheck(L_22);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_22, L_24);
		goto IL_01ef;
	}

IL_0120:
	{
		GameObject_t1113636619 * L_25 = __this->get_cnv_game_4();
		NullCheck(L_25);
		bool L_26 = GameObject_get_activeSelf_m1767405923(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_01ef;
		}
	}
	{
		String_t* L_27 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3359376429, /*hidden argument*/NULL);
		V_1 = L_27;
		GameObject_t1113636619 * L_28 = __this->get_backgroundSpriteGame_16();
		NullCheck(L_28);
		SpriteRenderer_t3235626157 * L_29 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_28, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		__this->set_bgSprite_21(L_29);
		String_t* L_30 = V_1;
		if (!L_30)
		{
			goto IL_01ef;
		}
	}
	{
		String_t* L_31 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_op_Equality_m920492651(NULL /*static, unused*/, L_31, _stringLiteral3790055497, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_0197;
		}
	}
	{
		String_t* L_33 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_34 = String_op_Equality_m920492651(NULL /*static, unused*/, L_33, _stringLiteral3775034288, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_01ad;
		}
	}
	{
		String_t* L_35 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_36 = String_op_Equality_m920492651(NULL /*static, unused*/, L_35, _stringLiteral2699831051, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_01c3;
		}
	}
	{
		String_t* L_37 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_38 = String_op_Equality_m920492651(NULL /*static, unused*/, L_37, _stringLiteral166522051, /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_01d9;
		}
	}
	{
		goto IL_01ef;
	}

IL_0197:
	{
		SpriteRenderer_t3235626157 * L_39 = __this->get_bgSprite_21();
		Sprite_t280657092 * L_40 = __this->get_mainTheme_17();
		NullCheck(L_39);
		SpriteRenderer_set_sprite_m1286893786(L_39, L_40, /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_01ad:
	{
		SpriteRenderer_t3235626157 * L_41 = __this->get_bgSprite_21();
		Sprite_t280657092 * L_42 = __this->get_flappyTheme_18();
		NullCheck(L_41);
		SpriteRenderer_set_sprite_m1286893786(L_41, L_42, /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_01c3:
	{
		SpriteRenderer_t3235626157 * L_43 = __this->get_bgSprite_21();
		Sprite_t280657092 * L_44 = __this->get_nightTheme_19();
		NullCheck(L_43);
		SpriteRenderer_set_sprite_m1286893786(L_43, L_44, /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_01d9:
	{
		SpriteRenderer_t3235626157 * L_45 = __this->get_bgSprite_21();
		Sprite_t280657092 * L_46 = __this->get_desertTheme_20();
		NullCheck(L_45);
		SpriteRenderer_set_sprite_m1286893786(L_45, L_46, /*hidden argument*/NULL);
		goto IL_01ef;
	}

IL_01ef:
	{
		return;
	}
}
// System.Void InitTextsScript::UpdateTexts()
extern "C"  void InitTextsScript_UpdateTexts_m2560498976 (InitTextsScript_t1053055510 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitTextsScript_UpdateTexts_m2560498976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_cnv_menu_2();
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m1767405923(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f3;
		}
	}
	{
		String_t* L_2 = __this->get_difficulty_28();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_2, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_4 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral167811139, /*hidden argument*/NULL);
		__this->set_highscore_7(L_4);
		int32_t L_5 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral870646944, /*hidden argument*/NULL);
		__this->set_tries_9(L_5);
		int32_t L_6 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral2541657273, /*hidden argument*/NULL);
		__this->set_lastPoints_11(L_6);
		goto IL_009f;
	}

IL_005a:
	{
		String_t* L_7 = __this->get_difficulty_28();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_7, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_009f;
		}
	}
	{
		int32_t L_9 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral2722989498, /*hidden argument*/NULL);
		__this->set_highscore_7(L_9);
		int32_t L_10 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral388954149, /*hidden argument*/NULL);
		__this->set_tries_9(L_10);
		int32_t L_11 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3681683422, /*hidden argument*/NULL);
		__this->set_lastPoints_11(L_11);
	}

IL_009f:
	{
		Text_t1901882714 * L_12 = __this->get_textHighscoreCount_6();
		int32_t* L_13 = __this->get_address_of_highscore_7();
		String_t* L_14 = Int32_ToString_m141394615(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_14);
		Text_t1901882714 * L_15 = __this->get_textPlayedCount_8();
		int32_t* L_16 = __this->get_address_of_tries_9();
		String_t* L_17 = Int32_ToString_m141394615(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_15, L_17);
		Text_t1901882714 * L_18 = __this->get_textLastPoints_10();
		int32_t* L_19 = __this->get_address_of_lastPoints_11();
		String_t* L_20 = Int32_ToString_m141394615(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_20);
	}

IL_00f3:
	{
		return;
	}
}
// System.Void InitTextsScript::FixedUpdate()
extern "C"  void InitTextsScript_FixedUpdate_m1485515752 (InitTextsScript_t1053055510 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitTextsScript_FixedUpdate_m1485515752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	Sprite_t280657092 * V_3 = NULL;
	AudioSource_t3935305588 * V_4 = NULL;
	String_t* V_5 = NULL;
	TextU5BU5D_t422084607* V_6 = NULL;
	Text_t1901882714 * V_7 = NULL;
	TextU5BU5D_t422084607* V_8 = NULL;
	int32_t V_9 = 0;
	Text_t1901882714 * V_10 = NULL;
	TextU5BU5D_t422084607* V_11 = NULL;
	int32_t V_12 = 0;
	Text_t1901882714 * V_13 = NULL;
	TextU5BU5D_t422084607* V_14 = NULL;
	int32_t V_15 = 0;
	Text_t1901882714 * V_16 = NULL;
	TextU5BU5D_t422084607* V_17 = NULL;
	int32_t V_18 = 0;
	{
		GameObject_t1113636619 * L_0 = __this->get_cnv_menu_2();
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m1767405923(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t1113636619 * L_2 = __this->get_backgroundSpriteMenu_14();
		NullCheck(L_2);
		SpriteRenderer_t3235626157 * L_3 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_2, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		__this->set_bgSprite_21(L_3);
		goto IL_00b3;
	}

IL_0026:
	{
		GameObject_t1113636619 * L_4 = __this->get_cnv_settings_3();
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeSelf_m1767405923(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_006f;
		}
	}
	{
		GameObject_t1113636619 * L_6 = __this->get_backgroundSpriteSettings_15();
		NullCheck(L_6);
		SpriteRenderer_t3235626157 * L_7 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_6, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		__this->set_bgSprite_21(L_7);
		int32_t L_8 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		V_0 = L_8;
		Text_t1901882714 * L_9 = __this->get_specialPointsText_13();
		String_t* L_10 = Int32_ToString_m141394615((&V_0), /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_10);
		goto IL_00b3;
	}

IL_006f:
	{
		GameObject_t1113636619 * L_11 = __this->get_cnv_game_4();
		NullCheck(L_11);
		bool L_12 = GameObject_get_activeSelf_m1767405923(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00b3;
		}
	}
	{
		GameObject_t1113636619 * L_13 = __this->get_backgroundSpriteGame_16();
		NullCheck(L_13);
		SpriteRenderer_t3235626157 * L_14 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_13, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		__this->set_bgSprite_21(L_14);
		int32_t L_15 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		V_1 = L_15;
		Text_t1901882714 * L_16 = __this->get_specialPointsText_13();
		String_t* L_17 = Int32_ToString_m141394615((&V_1), /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_16, L_17);
	}

IL_00b3:
	{
		String_t* L_18 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3574414313, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_18, _stringLiteral4002445229, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0202;
		}
	}
	{
		SpriteRenderer_t3235626157 * L_20 = __this->get_bgSprite_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0202;
		}
	}
	{
		String_t* L_22 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3359376429, /*hidden argument*/NULL);
		V_2 = L_22;
		SpriteRenderer_t3235626157 * L_23 = __this->get_bgSprite_21();
		NullCheck(L_23);
		Sprite_t280657092 * L_24 = SpriteRenderer_get_sprite_m663386871(L_23, /*hidden argument*/NULL);
		V_3 = L_24;
		String_t* L_25 = V_2;
		if (!L_25)
		{
			goto IL_0202;
		}
	}
	{
		String_t* L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_op_Equality_m920492651(NULL /*static, unused*/, L_26, _stringLiteral3790055497, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_013e;
		}
	}
	{
		String_t* L_28 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Equality_m920492651(NULL /*static, unused*/, L_28, _stringLiteral3775034288, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_016f;
		}
	}
	{
		String_t* L_30 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_31 = String_op_Equality_m920492651(NULL /*static, unused*/, L_30, _stringLiteral2699831051, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_01a0;
		}
	}
	{
		String_t* L_32 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_33 = String_op_Equality_m920492651(NULL /*static, unused*/, L_32, _stringLiteral166522051, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_01d1;
		}
	}
	{
		goto IL_0202;
	}

IL_013e:
	{
		Sprite_t280657092 * L_34 = __this->get_mainTheme_17();
		NullCheck(L_34);
		String_t* L_35 = Object_get_name_m4211327027(L_34, /*hidden argument*/NULL);
		Sprite_t280657092 * L_36 = V_3;
		NullCheck(L_36);
		String_t* L_37 = Object_get_name_m4211327027(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_38 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_35, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_016a;
		}
	}
	{
		SpriteRenderer_t3235626157 * L_39 = __this->get_bgSprite_21();
		Sprite_t280657092 * L_40 = __this->get_mainTheme_17();
		NullCheck(L_39);
		SpriteRenderer_set_sprite_m1286893786(L_39, L_40, /*hidden argument*/NULL);
	}

IL_016a:
	{
		goto IL_0202;
	}

IL_016f:
	{
		Sprite_t280657092 * L_41 = __this->get_flappyTheme_18();
		NullCheck(L_41);
		String_t* L_42 = Object_get_name_m4211327027(L_41, /*hidden argument*/NULL);
		Sprite_t280657092 * L_43 = V_3;
		NullCheck(L_43);
		String_t* L_44 = Object_get_name_m4211327027(L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_45 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_42, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_019b;
		}
	}
	{
		SpriteRenderer_t3235626157 * L_46 = __this->get_bgSprite_21();
		Sprite_t280657092 * L_47 = __this->get_flappyTheme_18();
		NullCheck(L_46);
		SpriteRenderer_set_sprite_m1286893786(L_46, L_47, /*hidden argument*/NULL);
	}

IL_019b:
	{
		goto IL_0202;
	}

IL_01a0:
	{
		Sprite_t280657092 * L_48 = __this->get_nightTheme_19();
		NullCheck(L_48);
		String_t* L_49 = Object_get_name_m4211327027(L_48, /*hidden argument*/NULL);
		Sprite_t280657092 * L_50 = V_3;
		NullCheck(L_50);
		String_t* L_51 = Object_get_name_m4211327027(L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_49, L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01cc;
		}
	}
	{
		SpriteRenderer_t3235626157 * L_53 = __this->get_bgSprite_21();
		Sprite_t280657092 * L_54 = __this->get_nightTheme_19();
		NullCheck(L_53);
		SpriteRenderer_set_sprite_m1286893786(L_53, L_54, /*hidden argument*/NULL);
	}

IL_01cc:
	{
		goto IL_0202;
	}

IL_01d1:
	{
		Sprite_t280657092 * L_55 = __this->get_desertTheme_20();
		NullCheck(L_55);
		String_t* L_56 = Object_get_name_m4211327027(L_55, /*hidden argument*/NULL);
		Sprite_t280657092 * L_57 = V_3;
		NullCheck(L_57);
		String_t* L_58 = Object_get_name_m4211327027(L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_59 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_56, L_58, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_01fd;
		}
	}
	{
		SpriteRenderer_t3235626157 * L_60 = __this->get_bgSprite_21();
		Sprite_t280657092 * L_61 = __this->get_desertTheme_20();
		NullCheck(L_60);
		SpriteRenderer_set_sprite_m1286893786(L_60, L_61, /*hidden argument*/NULL);
	}

IL_01fd:
	{
		goto IL_0202;
	}

IL_0202:
	{
		GameObject_t1113636619 * L_62 = __this->get_cnv_settings_3();
		NullCheck(L_62);
		bool L_63 = GameObject_get_activeSelf_m1767405923(L_62, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_03cd;
		}
	}
	{
		GameObject_t1113636619 * L_64 = __this->get_audioSource_22();
		NullCheck(L_64);
		AudioSource_t3935305588 * L_65 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_64, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		V_4 = L_65;
		String_t* L_66 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral1785964129, /*hidden argument*/NULL);
		V_5 = L_66;
		String_t* L_67 = V_5;
		if (!L_67)
		{
			goto IL_03cd;
		}
	}
	{
		String_t* L_68 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_69 = String_op_Equality_m920492651(NULL /*static, unused*/, L_68, _stringLiteral2074989104, /*hidden argument*/NULL);
		if (L_69)
		{
			goto IL_028c;
		}
	}
	{
		String_t* L_70 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_71 = String_op_Equality_m920492651(NULL /*static, unused*/, L_70, _stringLiteral1114791609, /*hidden argument*/NULL);
		if (L_71)
		{
			goto IL_029d;
		}
	}
	{
		String_t* L_72 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_73 = String_op_Equality_m920492651(NULL /*static, unused*/, L_72, _stringLiteral3654983690, /*hidden argument*/NULL);
		if (L_73)
		{
			goto IL_02e9;
		}
	}
	{
		String_t* L_74 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_75 = String_op_Equality_m920492651(NULL /*static, unused*/, L_74, _stringLiteral747299879, /*hidden argument*/NULL);
		if (L_75)
		{
			goto IL_0335;
		}
	}
	{
		String_t* L_76 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_77 = String_op_Equality_m920492651(NULL /*static, unused*/, L_76, _stringLiteral3163906174, /*hidden argument*/NULL);
		if (L_77)
		{
			goto IL_0381;
		}
	}
	{
		goto IL_03cd;
	}

IL_028c:
	{
		GameObject_t1113636619 * L_78 = __this->get_audioSource_22();
		NullCheck(L_78);
		GameObject_SetActive_m796801857(L_78, (bool)0, /*hidden argument*/NULL);
		goto IL_03cd;
	}

IL_029d:
	{
		GameObject_t1113636619 * L_79 = __this->get_audioSource_22();
		NullCheck(L_79);
		bool L_80 = GameObject_get_activeSelf_m1767405923(L_79, /*hidden argument*/NULL);
		if (L_80)
		{
			goto IL_02b9;
		}
	}
	{
		GameObject_t1113636619 * L_81 = __this->get_audioSource_22();
		NullCheck(L_81);
		GameObject_SetActive_m796801857(L_81, (bool)1, /*hidden argument*/NULL);
	}

IL_02b9:
	{
		AudioSource_t3935305588 * L_82 = V_4;
		NullCheck(L_82);
		AudioClip_t3680889665 * L_83 = AudioSource_get_clip_m1234340632(L_82, /*hidden argument*/NULL);
		AudioClip_t3680889665 * L_84 = __this->get_clip_mainSound_23();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_85 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_83, L_84, /*hidden argument*/NULL);
		if (!L_85)
		{
			goto IL_02e4;
		}
	}
	{
		AudioSource_t3935305588 * L_86 = V_4;
		AudioClip_t3680889665 * L_87 = __this->get_clip_mainSound_23();
		NullCheck(L_86);
		AudioSource_set_clip_m31653938(L_86, L_87, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_88 = V_4;
		NullCheck(L_88);
		AudioSource_Play_m48294159(L_88, /*hidden argument*/NULL);
	}

IL_02e4:
	{
		goto IL_03cd;
	}

IL_02e9:
	{
		GameObject_t1113636619 * L_89 = __this->get_audioSource_22();
		NullCheck(L_89);
		bool L_90 = GameObject_get_activeSelf_m1767405923(L_89, /*hidden argument*/NULL);
		if (L_90)
		{
			goto IL_0305;
		}
	}
	{
		GameObject_t1113636619 * L_91 = __this->get_audioSource_22();
		NullCheck(L_91);
		GameObject_SetActive_m796801857(L_91, (bool)1, /*hidden argument*/NULL);
	}

IL_0305:
	{
		AudioSource_t3935305588 * L_92 = V_4;
		NullCheck(L_92);
		AudioClip_t3680889665 * L_93 = AudioSource_get_clip_m1234340632(L_92, /*hidden argument*/NULL);
		AudioClip_t3680889665 * L_94 = __this->get_clip_skyflightSound_25();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_95 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_93, L_94, /*hidden argument*/NULL);
		if (!L_95)
		{
			goto IL_0330;
		}
	}
	{
		AudioSource_t3935305588 * L_96 = V_4;
		AudioClip_t3680889665 * L_97 = __this->get_clip_skyflightSound_25();
		NullCheck(L_96);
		AudioSource_set_clip_m31653938(L_96, L_97, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_98 = V_4;
		NullCheck(L_98);
		AudioSource_Play_m48294159(L_98, /*hidden argument*/NULL);
	}

IL_0330:
	{
		goto IL_03cd;
	}

IL_0335:
	{
		GameObject_t1113636619 * L_99 = __this->get_audioSource_22();
		NullCheck(L_99);
		bool L_100 = GameObject_get_activeSelf_m1767405923(L_99, /*hidden argument*/NULL);
		if (L_100)
		{
			goto IL_0351;
		}
	}
	{
		GameObject_t1113636619 * L_101 = __this->get_audioSource_22();
		NullCheck(L_101);
		GameObject_SetActive_m796801857(L_101, (bool)1, /*hidden argument*/NULL);
	}

IL_0351:
	{
		AudioSource_t3935305588 * L_102 = V_4;
		NullCheck(L_102);
		AudioClip_t3680889665 * L_103 = AudioSource_get_clip_m1234340632(L_102, /*hidden argument*/NULL);
		AudioClip_t3680889665 * L_104 = __this->get_clip_dankstormSound_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_105 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_103, L_104, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_037c;
		}
	}
	{
		AudioSource_t3935305588 * L_106 = V_4;
		AudioClip_t3680889665 * L_107 = __this->get_clip_dankstormSound_24();
		NullCheck(L_106);
		AudioSource_set_clip_m31653938(L_106, L_107, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_108 = V_4;
		NullCheck(L_108);
		AudioSource_Play_m48294159(L_108, /*hidden argument*/NULL);
	}

IL_037c:
	{
		goto IL_03cd;
	}

IL_0381:
	{
		GameObject_t1113636619 * L_109 = __this->get_audioSource_22();
		NullCheck(L_109);
		bool L_110 = GameObject_get_activeSelf_m1767405923(L_109, /*hidden argument*/NULL);
		if (L_110)
		{
			goto IL_039d;
		}
	}
	{
		GameObject_t1113636619 * L_111 = __this->get_audioSource_22();
		NullCheck(L_111);
		GameObject_SetActive_m796801857(L_111, (bool)1, /*hidden argument*/NULL);
	}

IL_039d:
	{
		AudioSource_t3935305588 * L_112 = V_4;
		NullCheck(L_112);
		AudioClip_t3680889665 * L_113 = AudioSource_get_clip_m1234340632(L_112, /*hidden argument*/NULL);
		AudioClip_t3680889665 * L_114 = __this->get_clip_egyptSound_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_115 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_113, L_114, /*hidden argument*/NULL);
		if (!L_115)
		{
			goto IL_03c8;
		}
	}
	{
		AudioSource_t3935305588 * L_116 = V_4;
		AudioClip_t3680889665 * L_117 = __this->get_clip_egyptSound_26();
		NullCheck(L_116);
		AudioSource_set_clip_m31653938(L_116, L_117, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_118 = V_4;
		NullCheck(L_118);
		AudioSource_Play_m48294159(L_118, /*hidden argument*/NULL);
	}

IL_03c8:
	{
		goto IL_03cd;
	}

IL_03cd:
	{
		GameObject_t1113636619 * L_119 = __this->get_texts_5();
		NullCheck(L_119);
		TextU5BU5D_t422084607* L_120 = GameObject_GetComponentsInChildren_TisText_t1901882714_m1445556921(L_119, /*hidden argument*/GameObject_GetComponentsInChildren_TisText_t1901882714_m1445556921_RuntimeMethod_var);
		V_6 = L_120;
		String_t* L_121 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3359376429, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_122 = String_op_Equality_m920492651(NULL /*static, unused*/, L_121, _stringLiteral3775034288, /*hidden argument*/NULL);
		if (!L_122)
		{
			goto IL_0428;
		}
	}
	{
		TextU5BU5D_t422084607* L_123 = V_6;
		V_8 = L_123;
		V_9 = 0;
		goto IL_0418;
	}

IL_03ff:
	{
		TextU5BU5D_t422084607* L_124 = V_8;
		int32_t L_125 = V_9;
		NullCheck(L_124);
		int32_t L_126 = L_125;
		Text_t1901882714 * L_127 = (L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_126));
		V_7 = L_127;
		Text_t1901882714 * L_128 = V_7;
		Color_t2555686324  L_129 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_128);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_128, L_129);
		int32_t L_130 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_130, (int32_t)1));
	}

IL_0418:
	{
		int32_t L_131 = V_9;
		TextU5BU5D_t422084607* L_132 = V_8;
		NullCheck(L_132);
		if ((((int32_t)L_131) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_132)->max_length)))))))
		{
			goto IL_03ff;
		}
	}
	{
		goto IL_052b;
	}

IL_0428:
	{
		String_t* L_133 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3359376429, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_134 = String_op_Equality_m920492651(NULL /*static, unused*/, L_133, _stringLiteral166522051, /*hidden argument*/NULL);
		if (!L_134)
		{
			goto IL_0476;
		}
	}
	{
		TextU5BU5D_t422084607* L_135 = V_6;
		V_11 = L_135;
		V_12 = 0;
		goto IL_0466;
	}

IL_044d:
	{
		TextU5BU5D_t422084607* L_136 = V_11;
		int32_t L_137 = V_12;
		NullCheck(L_136);
		int32_t L_138 = L_137;
		Text_t1901882714 * L_139 = (L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_138));
		V_10 = L_139;
		Text_t1901882714 * L_140 = V_10;
		Color_t2555686324  L_141 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_140);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_140, L_141);
		int32_t L_142 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_142, (int32_t)1));
	}

IL_0466:
	{
		int32_t L_143 = V_12;
		TextU5BU5D_t422084607* L_144 = V_11;
		NullCheck(L_144);
		if ((((int32_t)L_143) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_144)->max_length)))))))
		{
			goto IL_044d;
		}
	}
	{
		goto IL_052b;
	}

IL_0476:
	{
		String_t* L_145 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3359376429, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_146 = String_op_Equality_m920492651(NULL /*static, unused*/, L_145, _stringLiteral2699831051, /*hidden argument*/NULL);
		if (!L_146)
		{
			goto IL_04d3;
		}
	}
	{
		TextU5BU5D_t422084607* L_147 = V_6;
		V_14 = L_147;
		V_15 = 0;
		goto IL_04c3;
	}

IL_049b:
	{
		TextU5BU5D_t422084607* L_148 = V_14;
		int32_t L_149 = V_15;
		NullCheck(L_148);
		int32_t L_150 = L_149;
		Text_t1901882714 * L_151 = (L_148)->GetAt(static_cast<il2cpp_array_size_t>(L_150));
		V_13 = L_151;
		Text_t1901882714 * L_152 = V_13;
		Color_t2555686324  L_153;
		memset(&L_153, 0, sizeof(L_153));
		Color__ctor_m286683560((&L_153), (255.0f), (252.0f), (218.0f), /*hidden argument*/NULL);
		NullCheck(L_152);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_152, L_153);
		int32_t L_154 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_154, (int32_t)1));
	}

IL_04c3:
	{
		int32_t L_155 = V_15;
		TextU5BU5D_t422084607* L_156 = V_14;
		NullCheck(L_156);
		if ((((int32_t)L_155) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_156)->max_length)))))))
		{
			goto IL_049b;
		}
	}
	{
		goto IL_052b;
	}

IL_04d3:
	{
		String_t* L_157 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3359376429, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_158 = String_op_Equality_m920492651(NULL /*static, unused*/, L_157, _stringLiteral3790055497, /*hidden argument*/NULL);
		if (!L_158)
		{
			goto IL_052b;
		}
	}
	{
		TextU5BU5D_t422084607* L_159 = V_6;
		V_17 = L_159;
		V_18 = 0;
		goto IL_0520;
	}

IL_04f8:
	{
		TextU5BU5D_t422084607* L_160 = V_17;
		int32_t L_161 = V_18;
		NullCheck(L_160);
		int32_t L_162 = L_161;
		Text_t1901882714 * L_163 = (L_160)->GetAt(static_cast<il2cpp_array_size_t>(L_162));
		V_16 = L_163;
		Text_t1901882714 * L_164 = V_16;
		Color_t2555686324  L_165;
		memset(&L_165, 0, sizeof(L_165));
		Color__ctor_m286683560((&L_165), (255.0f), (252.0f), (218.0f), /*hidden argument*/NULL);
		NullCheck(L_164);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_164, L_165);
		int32_t L_166 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add((int32_t)L_166, (int32_t)1));
	}

IL_0520:
	{
		int32_t L_167 = V_18;
		TextU5BU5D_t422084607* L_168 = V_17;
		NullCheck(L_168);
		if ((((int32_t)L_167) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_168)->max_length)))))))
		{
			goto IL_04f8;
		}
	}

IL_052b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LoseScript::.ctor()
extern "C"  void LoseScript__ctor_m3614160696 (LoseScript_t212724369 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoseScript::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void LoseScript_OnTriggerEnter_m3921108129 (LoseScript_t212724369 * __this, Collider_t1773347010 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoseScript_OnTriggerEnter_m3921108129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3328599146* V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	GameObjectU5BU5D_t3328599146* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Collider_t1773347010 * L_0 = ___collider0;
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m4211327027(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, _stringLiteral823026589, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		Collider_t1773347010 * L_4 = ___collider0;
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = GameObject_get_tag_m3951609671(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, _stringLiteral823026589, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_01b1;
		}
	}

IL_0034:
	{
		String_t* L_8 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3316871923, /*hidden argument*/NULL);
		__this->set_difficulty_6(L_8);
		String_t* L_9 = __this->get_difficulty_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_9, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_11 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral870646944, /*hidden argument*/NULL);
		__this->set_tries_5(L_11);
		goto IL_0093;
	}

IL_006e:
	{
		String_t* L_12 = __this->get_difficulty_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m920492651(NULL /*static, unused*/, L_12, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0093;
		}
	}
	{
		int32_t L_14 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral388954149, /*hidden argument*/NULL);
		__this->set_tries_5(L_14);
	}

IL_0093:
	{
		RuntimeObject* L_15 = LoseScript_DelayGame_m1373927896(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_15, /*hidden argument*/NULL);
		int32_t L_16 = __this->get_tries_5();
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_00d6;
		}
	}
	{
		int32_t L_17 = __this->get_tries_5();
		int32_t L_18 = __this->get_triesTillAds_8();
		if (((int32_t)((int32_t)L_17%(int32_t)L_18)))
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		Advertisement_Initialize_m1051450402(NULL /*static, unused*/, _stringLiteral3014706548, (bool)1, /*hidden argument*/NULL);
		RuntimeObject* L_19 = LoseScript_ShowAdWhenReady_m2535654720(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_19, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		float L_20 = __this->get_savedTimeScale_9();
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_21 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, _stringLiteral3452654525, /*hidden argument*/NULL);
		V_0 = L_21;
		GameObjectU5BU5D_t3328599146* L_22 = V_0;
		V_2 = L_22;
		V_3 = 0;
		goto IL_0118;
	}

IL_00f5:
	{
		GameObjectU5BU5D_t3328599146* L_23 = V_2;
		int32_t L_24 = V_3;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		GameObject_t1113636619 * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		V_1 = L_26;
		GameObject_t1113636619 * L_27 = V_1;
		NullCheck(L_27);
		String_t* L_28 = Object_get_name_m4211327027(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		bool L_29 = String_Contains_m1147431944(L_28, _stringLiteral3528114727, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0114;
		}
	}
	{
		GameObject_t1113636619 * L_30 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
	}

IL_0114:
	{
		int32_t L_31 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
	}

IL_0118:
	{
		int32_t L_32 = V_3;
		GameObjectU5BU5D_t3328599146* L_33 = V_2;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_33)->max_length)))))))
		{
			goto IL_00f5;
		}
	}
	{
		AudioListener_set_pause_m2425921647(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_34 = __this->get_audioSrc_7();
		NullCheck(L_34);
		bool L_35 = Behaviour_get_isActiveAndEnabled_m3143666263(L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0142;
		}
	}
	{
		AudioSource_t3935305588 * L_36 = __this->get_audioSrc_7();
		NullCheck(L_36);
		AudioSource_Play_m48294159(L_36, /*hidden argument*/NULL);
	}

IL_0142:
	{
		String_t* L_37 = __this->get_difficulty_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_38 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_37, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0170;
		}
	}
	{
		int32_t L_39 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3046124294, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2541657273, L_39, /*hidden argument*/NULL);
		goto IL_0199;
	}

IL_0170:
	{
		String_t* L_40 = __this->get_difficulty_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_41 = String_op_Equality_m920492651(NULL /*static, unused*/, L_40, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0199;
		}
	}
	{
		int32_t L_42 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3046124294, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3681683422, L_42, /*hidden argument*/NULL);
	}

IL_0199:
	{
		GameObject_t1113636619 * L_43 = __this->get_Game_2();
		NullCheck(L_43);
		GameObject_SetActive_m796801857(L_43, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_44 = __this->get_Menu_3();
		NullCheck(L_44);
		GameObject_SetActive_m796801857(L_44, (bool)1, /*hidden argument*/NULL);
	}

IL_01b1:
	{
		return;
	}
}
// System.Collections.IEnumerator LoseScript::DelayGame()
extern "C"  RuntimeObject* LoseScript_DelayGame_m1373927896 (LoseScript_t212724369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoseScript_DelayGame_m1373927896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDelayGameU3Ec__Iterator0_t4235577792 * V_0 = NULL;
	{
		U3CDelayGameU3Ec__Iterator0_t4235577792 * L_0 = (U3CDelayGameU3Ec__Iterator0_t4235577792 *)il2cpp_codegen_object_new(U3CDelayGameU3Ec__Iterator0_t4235577792_il2cpp_TypeInfo_var);
		U3CDelayGameU3Ec__Iterator0__ctor_m604480062(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDelayGameU3Ec__Iterator0_t4235577792 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CDelayGameU3Ec__Iterator0_t4235577792 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator LoseScript::ShowAdWhenReady()
extern "C"  RuntimeObject* LoseScript_ShowAdWhenReady_m2535654720 (LoseScript_t212724369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoseScript_ShowAdWhenReady_m2535654720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * V_0 = NULL;
	{
		U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * L_0 = (U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 *)il2cpp_codegen_object_new(U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618_il2cpp_TypeInfo_var);
		U3CShowAdWhenReadyU3Ec__Iterator1__ctor_m2670077801(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * L_2 = V_0;
		return L_2;
	}
}
// System.Void LoseScript::HandleShowResult(UnityEngine.Advertisements.ShowResult)
extern "C"  void LoseScript_HandleShowResult_m3154952640 (LoseScript_t212724369 * __this, int32_t ___result0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___result0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___result0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0037;
	}

IL_0013:
	{
		RuntimeObject* L_2 = LoseScript_HandleAnimation_m1724406564(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_2, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_0025:
	{
		RuntimeObject* L_3 = LoseScript_HandleAnimation_m1724406564(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_0037:
	{
		return;
	}
}
// System.Collections.IEnumerator LoseScript::HandleAnimation()
extern "C"  RuntimeObject* LoseScript_HandleAnimation_m1724406564 (LoseScript_t212724369 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoseScript_HandleAnimation_m1724406564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CHandleAnimationU3Ec__Iterator2_t2293152264 * V_0 = NULL;
	{
		U3CHandleAnimationU3Ec__Iterator2_t2293152264 * L_0 = (U3CHandleAnimationU3Ec__Iterator2_t2293152264 *)il2cpp_codegen_object_new(U3CHandleAnimationU3Ec__Iterator2_t2293152264_il2cpp_TypeInfo_var);
		U3CHandleAnimationU3Ec__Iterator2__ctor_m403717454(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CHandleAnimationU3Ec__Iterator2_t2293152264 * L_1 = V_0;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LoseScript/<DelayGame>c__Iterator0::.ctor()
extern "C"  void U3CDelayGameU3Ec__Iterator0__ctor_m604480062 (U3CDelayGameU3Ec__Iterator0_t4235577792 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LoseScript/<DelayGame>c__Iterator0::MoveNext()
extern "C"  bool U3CDelayGameU3Ec__Iterator0_MoveNext_m4249602529 (U3CDelayGameU3Ec__Iterator0_t4235577792 * __this, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_008c;
			}
		}
	}
	{
		goto IL_0093;
	}

IL_0021:
	{
		LoseScript_t212724369 * L_2 = __this->get_U24this_1();
		float L_3 = Time_get_timeScale_m701790074(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_savedTimeScale_9(L_3);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		LoseScript_t212724369 * L_4 = __this->get_U24this_1();
		NullCheck(L_4);
		AudioSource_t3935305588 * L_5 = L_4->get_audioSrc_7();
		NullCheck(L_5);
		AudioSource_Stop_m2682712816(L_5, /*hidden argument*/NULL);
		float L_6 = Time_get_realtimeSinceStartup_m3141794964(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__0_0(((float)il2cpp_codegen_add((float)L_6, (float)(0.7f))));
		goto IL_0061;
	}

IL_0061:
	{
		float L_7 = __this->get_U3CtimeU3E__0_0();
		float L_8 = Time_get_realtimeSinceStartup_m3141794964(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_7) > ((float)L_8)))
		{
			goto IL_0061;
		}
	}
	{
		__this->set_U24current_2(NULL);
		bool L_9 = __this->get_U24disposing_3();
		if (L_9)
		{
			goto IL_0087;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0087:
	{
		goto IL_0095;
	}

IL_008c:
	{
		__this->set_U24PC_4((-1));
	}

IL_0093:
	{
		return (bool)0;
	}

IL_0095:
	{
		return (bool)1;
	}
}
// System.Object LoseScript/<DelayGame>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CDelayGameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1740452154 (U3CDelayGameU3Ec__Iterator0_t4235577792 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object LoseScript/<DelayGame>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CDelayGameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1617715274 (U3CDelayGameU3Ec__Iterator0_t4235577792 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void LoseScript/<DelayGame>c__Iterator0::Dispose()
extern "C"  void U3CDelayGameU3Ec__Iterator0_Dispose_m3894538464 (U3CDelayGameU3Ec__Iterator0_t4235577792 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void LoseScript/<DelayGame>c__Iterator0::Reset()
extern "C"  void U3CDelayGameU3Ec__Iterator0_Reset_m1581945427 (U3CDelayGameU3Ec__Iterator0_t4235577792 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayGameU3Ec__Iterator0_Reset_m1581945427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LoseScript/<HandleAnimation>c__Iterator2::.ctor()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2__ctor_m403717454 (U3CHandleAnimationU3Ec__Iterator2_t2293152264 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LoseScript/<HandleAnimation>c__Iterator2::MoveNext()
extern "C"  bool U3CHandleAnimationU3Ec__Iterator2_MoveNext_m2097147944 (U3CHandleAnimationU3Ec__Iterator2_t2293152264 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CHandleAnimationU3Ec__Iterator2_MoveNext_m2097147944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0041;
			}
		}
	}
	{
		goto IL_0052;
	}

IL_0021:
	{
		goto IL_0041;
	}

IL_0026:
	{
		__this->set_U24current_0(NULL);
		bool L_2 = __this->get_U24disposing_1();
		if (L_2)
		{
			goto IL_003c;
		}
	}
	{
		__this->set_U24PC_2(1);
	}

IL_003c:
	{
		goto IL_0054;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		bool L_3 = Advertisement_get_isShowing_m3507224835(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		__this->set_U24PC_2((-1));
	}

IL_0052:
	{
		return (bool)0;
	}

IL_0054:
	{
		return (bool)1;
	}
}
// System.Object LoseScript/<HandleAnimation>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CHandleAnimationU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3057165267 (U3CHandleAnimationU3Ec__Iterator2_t2293152264 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Object LoseScript/<HandleAnimation>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CHandleAnimationU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2444304204 (U3CHandleAnimationU3Ec__Iterator2_t2293152264 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Void LoseScript/<HandleAnimation>c__Iterator2::Dispose()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2_Dispose_m814049305 (U3CHandleAnimationU3Ec__Iterator2_t2293152264 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_1((bool)1);
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void LoseScript/<HandleAnimation>c__Iterator2::Reset()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2_Reset_m2514165367 (U3CHandleAnimationU3Ec__Iterator2_t2293152264 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CHandleAnimationU3Ec__Iterator2_Reset_m2514165367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LoseScript/<ShowAdWhenReady>c__Iterator1::.ctor()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1__ctor_m2670077801 (U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LoseScript/<ShowAdWhenReady>c__Iterator1::MoveNext()
extern "C"  bool U3CShowAdWhenReadyU3Ec__Iterator1_MoveNext_m2413234193 (U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowAdWhenReadyU3Ec__Iterator1_MoveNext_m2413234193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	ShowOptions_t990845000 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0041;
			}
		}
	}
	{
		goto IL_0097;
	}

IL_0021:
	{
		goto IL_0041;
	}

IL_0026:
	{
		__this->set_U24current_2(NULL);
		bool L_2 = __this->get_U24disposing_3();
		if (L_2)
		{
			goto IL_003c;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_003c:
	{
		goto IL_0099;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		bool L_3 = Advertisement_IsReady_m2792558112(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		ShowOptions_t990845000 * L_4 = (ShowOptions_t990845000 *)il2cpp_codegen_object_new(ShowOptions_t990845000_il2cpp_TypeInfo_var);
		ShowOptions__ctor_m2194205660(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		ShowOptions_t990845000 * L_5 = V_1;
		LoseScript_t212724369 * L_6 = __this->get_U24this_1();
		intptr_t L_7 = (intptr_t)LoseScript_HandleShowResult_m3154952640_RuntimeMethod_var;
		Action_1_t3243021218 * L_8 = (Action_1_t3243021218 *)il2cpp_codegen_object_new(Action_1_t3243021218_il2cpp_TypeInfo_var);
		Action_1__ctor_m3797332079(L_8, L_6, L_7, /*hidden argument*/Action_1__ctor_m3797332079_RuntimeMethod_var);
		NullCheck(L_5);
		ShowOptions_set_resultCallback_m3887508449(L_5, L_8, /*hidden argument*/NULL);
		ShowOptions_t990845000 * L_9 = V_1;
		__this->set_U3CoptionsU3E__0_0(L_9);
		ShowOptions_t990845000 * L_10 = __this->get_U3CoptionsU3E__0_0();
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		Advertisement_Show_m53580060(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3831740680, ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_0097:
	{
		return (bool)0;
	}

IL_0099:
	{
		return (bool)1;
	}
}
// System.Object LoseScript/<ShowAdWhenReady>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CShowAdWhenReadyU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3306219477 (U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object LoseScript/<ShowAdWhenReady>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CShowAdWhenReadyU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1665085299 (U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void LoseScript/<ShowAdWhenReady>c__Iterator1::Dispose()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1_Dispose_m69804357 (U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void LoseScript/<ShowAdWhenReady>c__Iterator1::Reset()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1_Reset_m2750784832 (U3CShowAdWhenReadyU3Ec__Iterator1_t4153835618 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowAdWhenReadyU3Ec__Iterator1_Reset_m2750784832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RandomSpawnScript::.ctor()
extern "C"  void RandomSpawnScript__ctor_m3419280163 (RandomSpawnScript_t4065494415 * __this, const RuntimeMethod* method)
{
	{
		__this->set_nextSpawn_8((1.0f));
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (0.0f), (250.0f), (-230.0f), /*hidden argument*/NULL);
		__this->set_v3_position_12(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RandomSpawnScript::FixedUpdate()
extern "C"  void RandomSpawnScript_FixedUpdate_m293722346 (RandomSpawnScript_t4065494415 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RandomSpawnScript_FixedUpdate_m293722346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_nextSpawn_8();
		if ((!(((double)((double)il2cpp_codegen_add((double)(((double)((double)L_0))), (double)(0.1)))) > ((double)(((double)((double)L_1)))))))
		{
			goto IL_00ff;
		}
	}
	{
		int32_t L_2 = Random_Range_m4054026115(NULL /*static, unused*/, 1, 5, /*hidden argument*/NULL);
		__this->set_whatToSpawn_9(L_2);
		int32_t L_3 = __this->get_whatToSpawn_9();
		V_0 = L_3;
		int32_t L_4 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1)))
		{
			case 0:
			{
				goto IL_004d;
			}
			case 1:
			{
				goto IL_0069;
			}
			case 2:
			{
				goto IL_0085;
			}
			case 3:
			{
				goto IL_00a1;
			}
		}
	}
	{
		goto IL_00bd;
	}

IL_004d:
	{
		GameObject_t1113636619 * L_5 = __this->get_prefab1_2();
		Vector3_t3722313464  L_6 = __this->get_v3_position_12();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_7 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		goto IL_00bd;
	}

IL_0069:
	{
		GameObject_t1113636619 * L_8 = __this->get_prefab2_3();
		Vector3_t3722313464  L_9 = __this->get_v3_position_12();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_10 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		goto IL_00bd;
	}

IL_0085:
	{
		GameObject_t1113636619 * L_11 = __this->get_prefab3_4();
		Vector3_t3722313464  L_12 = __this->get_v3_position_12();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_13 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		goto IL_00bd;
	}

IL_00a1:
	{
		GameObject_t1113636619 * L_14 = __this->get_prefab4_5();
		Vector3_t3722313464  L_15 = __this->get_v3_position_12();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_16 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		goto IL_00bd;
	}

IL_00bd:
	{
		String_t* L_17 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral3316871923, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m920492651(NULL /*static, unused*/, L_17, _stringLiteral2448431576, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00ed;
		}
	}
	{
		float L_19 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_20 = __this->get_spawnRateHard_7();
		__this->set_nextSpawn_8(((float)il2cpp_codegen_add((float)L_19, (float)L_20)));
		goto IL_00ff;
	}

IL_00ed:
	{
		float L_21 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_22 = __this->get_spawnRate_6();
		__this->set_nextSpawn_8(((float)il2cpp_codegen_add((float)L_21, (float)L_22)));
	}

IL_00ff:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrippleTriggerLeftScript::.ctor()
extern "C"  void TrippleTriggerLeftScript__ctor_m1935445039 (TrippleTriggerLeftScript_t4183068999 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrippleTriggerLeftScript::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void TrippleTriggerLeftScript_OnTriggerEnter_m261571855 (TrippleTriggerLeftScript_t4183068999 * __this, Collider_t1773347010 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrippleTriggerLeftScript_OnTriggerEnter_m261571855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t1773347010 * L_0 = ___collider0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m2716693327(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral237192571, _stringLiteral4002445229, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrippleTriggerMiddleScript::.ctor()
extern "C"  void TrippleTriggerMiddleScript__ctor_m2351640869 (TrippleTriggerMiddleScript_t4217752517 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrippleTriggerMiddleScript::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void TrippleTriggerMiddleScript_OnTriggerEnter_m3243190582 (TrippleTriggerMiddleScript_t4217752517 * __this, Collider_t1773347010 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrippleTriggerMiddleScript_OnTriggerEnter_m3243190582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	GameObjectU5BU5D_t3328599146* V_1 = NULL;
	GameObject_t1113636619 * V_2 = NULL;
	GameObjectU5BU5D_t3328599146* V_3 = NULL;
	int32_t V_4 = 0;
	{
		Collider_t1773347010 * L_0 = ___collider0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m2716693327(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0153;
		}
	}
	{
		String_t* L_3 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral237192571, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral3875954633, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0135;
		}
	}
	{
		String_t* L_5 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral2238759663, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, _stringLiteral3875954633, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0135;
		}
	}
	{
		float L_7 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_7;
		RuntimeObject* L_8 = TrippleTriggerMiddleScript_DelayGame_m2357289153(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral870646944, /*hidden argument*/NULL);
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0098;
		}
	}
	{
		int32_t L_10 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral870646944, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_triesTillAds_5();
		if (((int32_t)((int32_t)L_10%(int32_t)L_11)))
		{
			goto IL_0098;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		Advertisement_Initialize_m1051450402(NULL /*static, unused*/, _stringLiteral3014706548, (bool)1, /*hidden argument*/NULL);
		RuntimeObject* L_12 = TrippleTriggerMiddleScript_ShowAdWhenReady_m3650831147(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_12, /*hidden argument*/NULL);
	}

IL_0098:
	{
		float L_13 = __this->get_savedTimeScale_9();
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_14 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, _stringLiteral3452654525, /*hidden argument*/NULL);
		V_1 = L_14;
		GameObjectU5BU5D_t3328599146* L_15 = V_1;
		V_3 = L_15;
		V_4 = 0;
		goto IL_00de;
	}

IL_00b8:
	{
		GameObjectU5BU5D_t3328599146* L_16 = V_3;
		int32_t L_17 = V_4;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		GameObject_t1113636619 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		V_2 = L_19;
		GameObject_t1113636619 * L_20 = V_2;
		NullCheck(L_20);
		String_t* L_21 = Object_get_name_m4211327027(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		bool L_22 = String_Contains_m1147431944(L_21, _stringLiteral3528114727, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00d8;
		}
	}
	{
		GameObject_t1113636619 * L_23 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
	}

IL_00d8:
	{
		int32_t L_24 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_00de:
	{
		int32_t L_25 = V_4;
		GameObjectU5BU5D_t3328599146* L_26 = V_3;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_26)->max_length)))))))
		{
			goto IL_00b8;
		}
	}
	{
		AudioListener_set_pause_m2425921647(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_27 = __this->get_audioSrc_4();
		NullCheck(L_27);
		bool L_28 = Behaviour_get_isActiveAndEnabled_m3143666263(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0109;
		}
	}
	{
		AudioSource_t3935305588 * L_29 = __this->get_audioSrc_4();
		NullCheck(L_29);
		AudioSource_Play_m48294159(L_29, /*hidden argument*/NULL);
	}

IL_0109:
	{
		int32_t L_30 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3046124294, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral2541657273, L_30, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_31 = __this->get_Game_2();
		NullCheck(L_31);
		GameObject_SetActive_m796801857(L_31, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_32 = __this->get_Menu_3();
		NullCheck(L_32);
		GameObject_SetActive_m796801857(L_32, (bool)1, /*hidden argument*/NULL);
	}

IL_0135:
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral237192571, _stringLiteral3875954633, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral2238759663, _stringLiteral3875954633, /*hidden argument*/NULL);
	}

IL_0153:
	{
		return;
	}
}
// System.Collections.IEnumerator TrippleTriggerMiddleScript::DelayGame()
extern "C"  RuntimeObject* TrippleTriggerMiddleScript_DelayGame_m2357289153 (TrippleTriggerMiddleScript_t4217752517 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrippleTriggerMiddleScript_DelayGame_m2357289153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDelayGameU3Ec__Iterator0_t4237126612 * V_0 = NULL;
	{
		U3CDelayGameU3Ec__Iterator0_t4237126612 * L_0 = (U3CDelayGameU3Ec__Iterator0_t4237126612 *)il2cpp_codegen_object_new(U3CDelayGameU3Ec__Iterator0_t4237126612_il2cpp_TypeInfo_var);
		U3CDelayGameU3Ec__Iterator0__ctor_m243069393(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDelayGameU3Ec__Iterator0_t4237126612 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CDelayGameU3Ec__Iterator0_t4237126612 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator TrippleTriggerMiddleScript::ShowAdWhenReady()
extern "C"  RuntimeObject* TrippleTriggerMiddleScript_ShowAdWhenReady_m3650831147 (TrippleTriggerMiddleScript_t4217752517 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrippleTriggerMiddleScript_ShowAdWhenReady_m3650831147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * V_0 = NULL;
	{
		U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * L_0 = (U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 *)il2cpp_codegen_object_new(U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877_il2cpp_TypeInfo_var);
		U3CShowAdWhenReadyU3Ec__Iterator1__ctor_m2848285895(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * L_2 = V_0;
		return L_2;
	}
}
// System.Void TrippleTriggerMiddleScript::HandleShowResult(UnityEngine.Advertisements.ShowResult)
extern "C"  void TrippleTriggerMiddleScript_HandleShowResult_m438357543 (TrippleTriggerMiddleScript_t4217752517 * __this, int32_t ___result0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___result0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___result0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0043;
	}

IL_0013:
	{
		AudioListener_set_pause_m2425921647(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		RuntimeObject* L_2 = TrippleTriggerMiddleScript_HandleAnimation_m2765064618(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_2, /*hidden argument*/NULL);
		goto IL_0043;
	}

IL_002b:
	{
		AudioListener_set_pause_m2425921647(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		RuntimeObject* L_3 = TrippleTriggerMiddleScript_HandleAnimation_m2765064618(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
		goto IL_0043;
	}

IL_0043:
	{
		return;
	}
}
// System.Collections.IEnumerator TrippleTriggerMiddleScript::HandleAnimation()
extern "C"  RuntimeObject* TrippleTriggerMiddleScript_HandleAnimation_m2765064618 (TrippleTriggerMiddleScript_t4217752517 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrippleTriggerMiddleScript_HandleAnimation_m2765064618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CHandleAnimationU3Ec__Iterator2_t2078686267 * V_0 = NULL;
	{
		U3CHandleAnimationU3Ec__Iterator2_t2078686267 * L_0 = (U3CHandleAnimationU3Ec__Iterator2_t2078686267 *)il2cpp_codegen_object_new(U3CHandleAnimationU3Ec__Iterator2_t2078686267_il2cpp_TypeInfo_var);
		U3CHandleAnimationU3Ec__Iterator2__ctor_m517399459(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CHandleAnimationU3Ec__Iterator2_t2078686267 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CHandleAnimationU3Ec__Iterator2_t2078686267 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::.ctor()
extern "C"  void U3CDelayGameU3Ec__Iterator0__ctor_m243069393 (U3CDelayGameU3Ec__Iterator0_t4237126612 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::MoveNext()
extern "C"  bool U3CDelayGameU3Ec__Iterator0_MoveNext_m3238553079 (U3CDelayGameU3Ec__Iterator0_t4237126612 * __this, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_008c;
			}
		}
	}
	{
		goto IL_0093;
	}

IL_0021:
	{
		TrippleTriggerMiddleScript_t4217752517 * L_2 = __this->get_U24this_1();
		float L_3 = Time_get_timeScale_m701790074(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_savedTimeScale_9(L_3);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		TrippleTriggerMiddleScript_t4217752517 * L_4 = __this->get_U24this_1();
		NullCheck(L_4);
		AudioSource_t3935305588 * L_5 = L_4->get_audioSrc_4();
		NullCheck(L_5);
		AudioSource_Stop_m2682712816(L_5, /*hidden argument*/NULL);
		float L_6 = Time_get_realtimeSinceStartup_m3141794964(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtimeU3E__0_0(((float)il2cpp_codegen_add((float)L_6, (float)(0.7f))));
		goto IL_0061;
	}

IL_0061:
	{
		float L_7 = __this->get_U3CtimeU3E__0_0();
		float L_8 = Time_get_realtimeSinceStartup_m3141794964(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_7) > ((float)L_8)))
		{
			goto IL_0061;
		}
	}
	{
		__this->set_U24current_2(NULL);
		bool L_9 = __this->get_U24disposing_3();
		if (L_9)
		{
			goto IL_0087;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0087:
	{
		goto IL_0095;
	}

IL_008c:
	{
		__this->set_U24PC_4((-1));
	}

IL_0093:
	{
		return (bool)0;
	}

IL_0095:
	{
		return (bool)1;
	}
}
// System.Object TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CDelayGameU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1579123096 (U3CDelayGameU3Ec__Iterator0_t4237126612 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CDelayGameU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m996866801 (U3CDelayGameU3Ec__Iterator0_t4237126612 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::Dispose()
extern "C"  void U3CDelayGameU3Ec__Iterator0_Dispose_m3537113322 (U3CDelayGameU3Ec__Iterator0_t4237126612 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void TrippleTriggerMiddleScript/<DelayGame>c__Iterator0::Reset()
extern "C"  void U3CDelayGameU3Ec__Iterator0_Reset_m2029078786 (U3CDelayGameU3Ec__Iterator0_t4237126612 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayGameU3Ec__Iterator0_Reset_m2029078786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::.ctor()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2__ctor_m517399459 (U3CHandleAnimationU3Ec__Iterator2_t2078686267 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::MoveNext()
extern "C"  bool U3CHandleAnimationU3Ec__Iterator2_MoveNext_m10569484 (U3CHandleAnimationU3Ec__Iterator2_t2078686267 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CHandleAnimationU3Ec__Iterator2_MoveNext_m10569484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0041;
			}
		}
	}
	{
		goto IL_0067;
	}

IL_0021:
	{
		goto IL_0041;
	}

IL_0026:
	{
		__this->set_U24current_1(NULL);
		bool L_2 = __this->get_U24disposing_2();
		if (L_2)
		{
			goto IL_003c;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_003c:
	{
		goto IL_0069;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		bool L_3 = Advertisement_get_isShowing_m3507224835(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		TrippleTriggerMiddleScript_t4217752517 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		Animator_t434523843 * L_5 = L_4->get_anim_8();
		NullCheck(L_5);
		Animator_Play_m1697843332(L_5, _stringLiteral1578302179, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0067:
	{
		return (bool)0;
	}

IL_0069:
	{
		return (bool)1;
	}
}
// System.Object TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CHandleAnimationU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m789160188 (U3CHandleAnimationU3Ec__Iterator2_t2078686267 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CHandleAnimationU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m424604411 (U3CHandleAnimationU3Ec__Iterator2_t2078686267 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::Dispose()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2_Dispose_m764787641 (U3CHandleAnimationU3Ec__Iterator2_t2078686267 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void TrippleTriggerMiddleScript/<HandleAnimation>c__Iterator2::Reset()
extern "C"  void U3CHandleAnimationU3Ec__Iterator2_Reset_m824429254 (U3CHandleAnimationU3Ec__Iterator2_t2078686267 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CHandleAnimationU3Ec__Iterator2_Reset_m824429254_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::.ctor()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1__ctor_m2848285895 (U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::MoveNext()
extern "C"  bool U3CShowAdWhenReadyU3Ec__Iterator1_MoveNext_m3353907810 (U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowAdWhenReadyU3Ec__Iterator1_MoveNext_m3353907810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	ShowOptions_t990845000 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0041;
			}
		}
	}
	{
		goto IL_009d;
	}

IL_0021:
	{
		goto IL_0041;
	}

IL_0026:
	{
		__this->set_U24current_2(NULL);
		bool L_2 = __this->get_U24disposing_3();
		if (L_2)
		{
			goto IL_003c;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_003c:
	{
		goto IL_009f;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		bool L_3 = Advertisement_IsReady_m2792558112(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		AudioListener_set_pause_m2425921647(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		ShowOptions_t990845000 * L_4 = (ShowOptions_t990845000 *)il2cpp_codegen_object_new(ShowOptions_t990845000_il2cpp_TypeInfo_var);
		ShowOptions__ctor_m2194205660(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		ShowOptions_t990845000 * L_5 = V_1;
		TrippleTriggerMiddleScript_t4217752517 * L_6 = __this->get_U24this_1();
		intptr_t L_7 = (intptr_t)TrippleTriggerMiddleScript_HandleShowResult_m438357543_RuntimeMethod_var;
		Action_1_t3243021218 * L_8 = (Action_1_t3243021218 *)il2cpp_codegen_object_new(Action_1_t3243021218_il2cpp_TypeInfo_var);
		Action_1__ctor_m3797332079(L_8, L_6, L_7, /*hidden argument*/Action_1__ctor_m3797332079_RuntimeMethod_var);
		NullCheck(L_5);
		ShowOptions_set_resultCallback_m3887508449(L_5, L_8, /*hidden argument*/NULL);
		ShowOptions_t990845000 * L_9 = V_1;
		__this->set_U3CoptionsU3E__0_0(L_9);
		ShowOptions_t990845000 * L_10 = __this->get_U3CoptionsU3E__0_0();
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t842671397_il2cpp_TypeInfo_var);
		Advertisement_Show_m53580060(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral3831740680, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, _stringLiteral3831740680, ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_009d:
	{
		return (bool)0;
	}

IL_009f:
	{
		return (bool)1;
	}
}
// System.Object TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CShowAdWhenReadyU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1702813485 (U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CShowAdWhenReadyU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m820526258 (U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::Dispose()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1_Dispose_m791182743 (U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void TrippleTriggerMiddleScript/<ShowAdWhenReady>c__Iterator1::Reset()
extern "C"  void U3CShowAdWhenReadyU3Ec__Iterator1_Reset_m3724331834 (U3CShowAdWhenReadyU3Ec__Iterator1_t1651327877 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowAdWhenReadyU3Ec__Iterator1_Reset_m3724331834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrippleTriggerRightScript::.ctor()
extern "C"  void TrippleTriggerRightScript__ctor_m3454749363 (TrippleTriggerRightScript_t853323302 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrippleTriggerRightScript::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void TrippleTriggerRightScript_OnTriggerEnter_m4089145672 (TrippleTriggerRightScript_t853323302 * __this, Collider_t1773347010 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrippleTriggerRightScript_OnTriggerEnter_m4089145672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t1773347010 * L_0 = ___collider0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m2716693327(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, _stringLiteral2238759663, _stringLiteral4002445229, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
